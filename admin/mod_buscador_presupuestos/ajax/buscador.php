<?php
require '../../../config.php';
$tipo_documento = "0";
if (isset($_POST['tipo_documento'])):
    $tipo_documento = $_POST['tipo_documento'];
endif;
$busqueda = "";
if (isset($_POST['busqueda'])):
    $busqueda = $_POST['busqueda'];
endif;
$presupuesto = new Presupuesto();
if (strlen($busqueda) >= 2):
    $array_ped_pres = $presupuesto->search_admin_pre_ped($tipo_documento, $busqueda);
else:
    $array_ped_pres = $presupuesto->search_admin_pre_ped_all($tipo_documento);
endif;
foreach ($array_ped_pres as $item):
    ?>
    <?php if ($tipo_documento == "0"): ?>
        <table class="table table-striped">
            <tr>
                <td><?= $item['num_pre'] ?></td>
                <td><?= $item['id_cliente'] ?></td>
                <td><?= $item['fecha'] ?></td>
                <td>
                    <a target="_blank" href="<?= path_web_mods ?>mod_pdf/index.php?pres=<?= $item['id'] ?>" <span class="btn-sm btn_pdf_pedido btn btn-danger fa fa-file-pdf-o">&nbsp;Ver</span></a>
                    <?php if (empty($item['ref_pres'])): ?>
                        <button class="<?= $bg_color ?> hidden btn btn-xs icon_exchange_presupuestos bg-border-success" id_pres="<?= $item['id'] ?>"><?= lang_text_historial_confirmar ?></button>
                    <?php else: ?>
                        <?php $ref_pre = $presupuesto->obtener_num_pre($item['ref_pres']); ?>
                        Confirmado. <?= hideNumPedAdminREF($ref_pre, $item['cod_proc'], $item['cod_clie']) ?>
                    <?php endif; ?>
                </td>
            </tr>
        </table>
    <?php else: ?>
        <table class="table table-striped">
            <tr>
                <td><?= $item['num_pre'] ?></td>
                <td><?= $item['id_cliente'] ?></td>
                <td>
                    <?php
                    if ($item['status'] == '0'): echo "Anulado";
                    endif;
                    ?>
                </td>
                <td><?= $item['fecha'] ?></td>
                <td>
                    <a target="_blank" href="<?= path_web_mods ?>mod_pdf/index.php?pres=<?= $item['id'] ?>" <span class="btn-sm btn_pdf_pedido btn btn-danger fa fa-file-pdf-o">&nbsp;Ver</span></a>
                </td>
            </tr>
        </table>
    <?php
    endif;
endforeach;
