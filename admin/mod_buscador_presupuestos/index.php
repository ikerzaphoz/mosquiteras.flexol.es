<div class="row col-md-12 col-xs-12">
    <form class="search_admin col-md-12 col-xs-12 w100">
        <div class="col-md-8 col-xs-8">
            <input type="text" name="busqueda" class="font-size1em btn-sm input_admin_search_ppto w100 border-radius4" placeholder="Buscar (nº ppto / pedido, cod. cliente, fecha)">
        </div>
        <input type="hidden" value="<?=$tipo_documento?>" name="tipo_documento">
        <div class="col-md-4 col-xs-4">
            <button class="font-size1em btn btn-sm bg-border-info btn-default empty_search_admin"><i class="fa fa-eraser"></i>&nbsp;Limpiar</button>
        </div>
    </form>
</div>