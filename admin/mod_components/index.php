<?php

$familia = new Familia();
$array_components = $familia->getMaxMeasure('1791');

?>

<div class="container">
    <h4>Editar Medidas - Componentes</h4>
    <table class="table table-striped">
        <thead>
        <th>#</th>
        <th>Descripción</th>
        <th>Máximo medidas</th>
        <th>Opciones</th>
        </thead>
        <tbody class="contentTable">

        <?php

        foreach ($array_components as $item):

            ?>
            <tr>
                <form class="form_product">
                    <td><?=$item['arfami']?>-<?=$item['arsubf']?>-<?=$item['ararti']?></td>
                    <td>
                        <input readonly type="text" class="form-control" value="<?=$item['ardesc']?>">
                    </td>
                    <td>
                        <input name="measure" type="text" class="edit_max_measure_component" value="<?=$item['max_measure']?>">
                    </td>
                    <td>
                        <span class="btn btn-success edit_save_measure_component fa fa-save">
                    </td>
                    <input type="hidden" value="<?=$item['arfami']?>" name="arfami">
                    <input type="hidden" value="<?=$item['arsubf']?>" name="arsubf">
                    <input type="hidden" value="<?=$item['ararti']?>" name="ararti">
                </form>
            </tr>
            <?php

        endforeach;

        ?>
</tbody>
</table>
</div>