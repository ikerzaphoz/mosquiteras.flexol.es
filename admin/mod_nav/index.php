<div class="container">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="<?php if ($page == 'index'): echo ' active';
endif; ?> navbar-brand" href="<?= path_admin ?>index.php"><img style='width: 140px; height:35px; display: inline-block' alt="logo" src="<?= path_image ?>logo_mosquiflex.png"></a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <?php if($_SESSION['usu_admin'] != "clie"):?>
                    <li class="dropdown <?php if ($page == 'productos'): echo ' active';
endif; ?>">
                        <div class="dropdown-toggle btn_menu" data-toggle="dropdown">Productos</div>
                        <ul class="dropdown-menu">
                            <li><a href="<?= path_admin ?>productos.php?path=producto">Activar / Desactivar familias</a></li>
                            <li><a href="<?= path_admin ?>productos.php?path=producto_status">Activar / Desactivar productos</a></li>
                            <li><a href="<?= path_admin ?>productos.php?path=color">Colores HTML</a></li>
                        </ul>
                    </li>
                    <li class="dropdown <?php if ($page == 'componentes'): echo ' active';
endif; ?>">
                        <div class="dropdown-toggle btn_menu" data-toggle="dropdown">Componentes</div>
                        <ul class="dropdown-menu">
                            <li><a href="<?= path_admin ?>componentes.php?path=medidas">Medidas máximas</a></li>
                        </ul>
                    </li>
                    <li class="dropdown hidden <?php if ($page == 'web'): echo ' active';
endif; ?>">
                        <div class="dropdown-toggle btn_menu" data-toggle="dropdown">Web</div>
                        <ul class="dropdown-menu">
                            <li><a href="<?= path_admin ?>web.php?path=idiomas">Idiomas</a></li>
                        </ul>
                    </li>
                    <li class="dropdown <?php if ($page == 'scripts'): echo ' active';
endif; ?>">
                        <div class="dropdown-toggle btn_menu" data-toggle="dropdown">Base de datos</div>
                        <ul class="dropdown-menu">
                            <li><a href="<?= path_admin ?>scripts.php?path=scripts">Importar excels</a></li>
                        </ul>
                    </li>
                    <li class="dropdown <?php if ($page == 'presupuestos'): echo ' active';
endif; ?>">
                        <div class="dropdown-toggle btn_menu" data-toggle="dropdown">Ventas</div>
                        <ul class="dropdown-menu">
                            <li><a href="<?= path_admin ?>presupuestos.php?path=presupuestos">Presupuestos</a></li>
                            <li><a href="<?= path_admin ?>presupuestos.php?path=pedidos">Pedidos</a></li>
                        </ul>
                    </li>
                    <?php endif; ?>
                    <?php if($_SESSION['usu_admin'] == "clie"):?>
                        <li class="dropdown <?php if ($page == 'presupuestos'): echo ' active';
endif; ?>">
                        <div class="dropdown-toggle btn_menu btn btn-default border-success" data-toggle="dropdown">Presupuestos / pedidos</div>
                        <ul class="dropdown-menu">
                            <li><a href="<?= path_admin ?>presupuestos.php?path=presupuestos">Presupuestos</a></li>
                            <li><a href="<?= path_admin ?>presupuestos.php?path=pedidos">Pedidos</a></li>
                            
                            <li><a href="<?= path_admin ?>presupuestos.php?path=pedidos_valorados">Pedidos pagos anticipados</a></li>
                            <li><a href="<?= path_admin ?>presupuestos.php?path=solicitud">Solicitudes de muestras y expositores</a></li>
                        </ul>
                    </li>
                    <?php endif; ?>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?= path_admin ?>mod_nav/log_out.php">Cerrar sesión</a></li>
                </ul>
            </div>
        </div>
    </nav>