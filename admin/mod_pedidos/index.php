<?php
$presupuesto = new Presupuesto();
$array_presupuesto = $presupuesto->obtener_todos_pedidos("1");
$tipo_documento = "1";
?>

<div class="container">
    <h4>Ver Pedidos</h4>
<?php include 'mod_buscador_presupuestos/index.php' ?>
    <table class="table table-striped">
        <thead>
        <th>Pedidos</th>
        <th>Cliente</th>
        <th>Estado</th>
        <th>Fecha</th>
        <th>Opciones</th>
        </thead>
        <tbody class="response_ajax">
<?php foreach ($array_presupuesto as $item): ?>
                <tr>
                    <td><?= $item['num_pre'] ?></td>
                    <td><?= $item['id_cliente'] ?></td>
                    <td>
    <?php if ($item['status'] == '0'): echo "Anulado";
    endif; ?>
                    </td>
                    <td><?= $item['fecha'] ?></td>
                    <td>
                        <a target="_blank" href="<?= path_web_mods ?>mod_pdf/index.php?pres=<?= $item['id'] ?>" <span class="btn-sm btn_pdf_pedido btn btn-danger fa fa-file-pdf-o">&nbsp;Ver</span></a>
                        <button class="hidden btn-sm btn btn-default">Mostrar albaranes</button>
                    </td>
                </tr>
    <?php /*
      $array_albaranes = $presupuesto->get_albaranes($item['id']);
      foreach($array_albaranes as $albaran):?>
      <tr>
      <td><?=round2decimals($albaran['albpre'])?>&euro;</td>
      </tr>
      <?php endforeach; */ ?>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>