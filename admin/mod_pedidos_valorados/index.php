<?php

$presupuesto = new Presupuesto();
$array_pedido_valorado = $presupuesto->obtener_todos_pedidos_valorados("1");
$tipo_documento = "3";

?>

<div class="container">
    <h4>Ver Pedidos - Pago anticipado</h4>
    <?php include 'mod_buscador_presupuestos/index.php'?>
    <table class="table table-striped">
        <thead>
        <th>Presupuesto</th>
        <th>Cliente</th>
        <th>Fecha</th>
        <th>Opciones</th>
        </thead>
        <tbody class="response_ajax">
            <?php
                foreach($array_pedido_valorado as $item):?>
                    <tr>
                        <td><?=$item['num_pre']?></td>
                        <td><?=$item['id_cliente']?></td>
                        <td><?=$item['fecha']?></td>
                        <td>
                            <a target="_blank" href="<?=path_web_mods?>mod_pdf/index.php?pres_cond=<?=$item['id']?>" <span class="btn-sm btn_pdf_pedido btn btn-danger fa fa-file-pdf-o">&nbsp;Ver</span></a>
                            <button class="hidden btn-sm btn btn-default">Mostrar albaranes</button>
                            <?php if(empty($item['ref_pres'])):?>
                            <button class="<?= $bg_color ?> btn btn-xs icon_exchange_presupuestos bg-border-success" id_pres="<?= $item['id'] ?>"><?=lang_text_historial_confirmar?></button>
                            <?php else:?>
                            Confirmado. Pedido <?=$item['ref_pres'];?>
                            <?php endif;?>
                        </td>
                    </tr>
                    <?php
                    /*
                    $array_albaranes = $presupuesto->get_albaranes($item['id']);
                    foreach($array_albaranes as $albaran):?>
                        <tr>
                            <td><?=round2decimals($albaran['albpre'])?>&euro;</td>
                        </tr>
                    <?php endforeach; */?>
                <?php endforeach;?>
        </tbody>
    </table>
</div>