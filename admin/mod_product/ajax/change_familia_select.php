<?php
require '../../../config.php';
$familia = new Familia();
$array_familias = $familia->select_familia();

$arfami = "";
if (isset($_POST['arfami'])): $arfami = $_POST['arfami'];
endif;

$array_productos = $familia->get_status_articulos($arfami);
?>

<h4>Editar Productos</h4>
Seleccionar familia: 
<select class="select_familia_status">
    <option selected value="<?= $arfami ?>"><?= $arfami ?></option>
    <?php foreach ($array_familias as $item_familia): ?>
        <?php
        if ($arfami != $item_familia['arfami']):
            ?>
            <option value="<?= $item_familia['arfami'] ?>"><?= $item_familia['arfami'] ?></option>
            <?php
        endif;

    endforeach;
    ?>
</select>
<table class="table table-striped">
    <thead>
    <th>Arfami</th>
    <th>Arsubf</th>
    <th>Ararti</th>
    <th>Descripción</th>
    <th>Producto acabado</th>
    <th>Componentes</th>
    <th>Venta industrial</th>
</thead>
<tbody class="contentTable">

    <?php
    foreach ($array_productos as $item):
        ?>
        <tr>
    <form class="form_product_status">
        <td><?= $item['arfami'] ?></td>
        <td><?= $item['arsubf'] ?></td>
        <td><?= $item['ararti'] ?></td>
        <td><?= $item['ardesc'] ?></td>
        <td>
            <input name="status_2" type="checkbox" class="checkbox-inline edit_status_2" <?php
            if ($item['status_2'] == "1"): echo "checked";
            endif;
            ?>>
        </td>
        <td>
            <input name="status_1" type="checkbox" class="checkbox-inline edit_status_1" <?php
            if ($item['status_1'] == "1"): echo "checked";
            endif;
            ?>>
        </td>
        <td>
            <input name="status_3" type="checkbox" class="checkbox-inline edit_status_3" <?php
            if ($item['status_3'] == "1"): echo "checked";
            endif;
            ?>>
        </td>
        <td>
            <span class="btn btn-success edit_save_status fa fa-save">
        </td>
        <input type="hidden" value="<?= $item['arfami'] ?>" class="arfami" name="arfami">
        <input type="hidden" value="<?= $item['arsubf'] ?>" class="arsubf" name="arsubf">
        <input type="hidden" value="<?= $item['ararti'] ?>" class="ararti" name="ararti">
    </form>
    </tr>
    <?php
endforeach;
?>
</tbody>
</table>