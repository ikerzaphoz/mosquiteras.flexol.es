<?php

$familia = new Familia();
$array_familia = $familia->getFamilia('fafami', "status");

?>
<div class="container">
    <h4>Editar Productos</h4>
    <table class="table table-striped">
        <thead>
        <th>#</th>
        <th>Descripción</th>
        <th>Estado</th>
        <th>Opciones</th>
        </thead>
        <tbody class="contentTable">

        <?php

        foreach ($array_familia as $item):

            ?>
            <tr>
                <form class="form_product">
                    <td><?=$item['fafami']?></td>
                    <td>
                        <input name="fadesc" type="text" class="form-control edit_desc_product" value="<?=$item['fadesc']?>">
                    </td>
                    <td>
                        <input name="status" type="checkbox" class="checkbox-inline edit_status_product" <?php if($item['status']=="1"): echo "checked";endif; ?>>
                    </td>
                    <td>
                        <span class="btn btn-success edit_save_product fa fa-save">
                    </td>
                    <input type="hidden" value="<?=$item['fafami']?>" name="fafami">
                </form>
            </tr>
            <?php

        endforeach;

        ?>
        </tbody>
    </table>
</div>