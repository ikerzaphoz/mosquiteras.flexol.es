<?php

require '../../config.php';
require path_class . 'phpexcel/PHPExcel.php';
$empresa = "MT";
if(isset($_GET['empresa']) && !empty($_GET['empresa'])):
    $empresa = $_GET['empresa'];
endif;
$archivo = path_excels.$empresa."/clientes.xlsx";

$bd = new Db();

$inputFileType = PHPExcel_IOFactory::identify($archivo);
$objReader = PHPExcel_IOFactory::createReader($inputFileType);
$objPHPExcel = $objReader->load($archivo);
$sheet = $objPHPExcel->getSheet(0);
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();

$total_insert = 0;
$total = 0;
$sql = "INSERT INTO clientes VALUES ";
for ($row = 2; $row <= $highestRow; $row++) {

    $total++;
    $campo1 = replaceCharacteresSql($sheet->getCell("A" . $row)->getValue());
    $campo2 = replaceCharacteresSql($sheet->getCell("B" . $row)->getValue());
    $campo3 = replaceCharacteresSql($sheet->getCell("C" . $row)->getValue());
    $campo4 = replaceCharacteresSql($sheet->getCell("D" . $row)->getValue());
    $campo5 = replaceCharacteresSql($sheet->getCell("E" . $row)->getValue());
    $campo6 = replaceCharacteresSql($sheet->getCell("F" . $row)->getValue());
    $campo7 = replaceCharacteresSql($sheet->getCell("G" . $row)->getValue());
    $campo8 = replaceCharacteresSql($sheet->getCell("H" . $row)->getValue());
    $campo9 = replaceCharacteresSql($sheet->getCell("I" . $row)->getValue());
    $campo10 = replaceCharacteresSql($sheet->getCell("J" . $row)->getValue());
    $campo11 = replaceCharacteresSql($sheet->getCell("K" . $row)->getValue());
    $campo12 = replaceCharacteresSql($sheet->getCell("L" . $row)->getValue());
    $campo13 = replaceCharacteresSql($sheet->getCell("M" . $row)->getValue());
    $campo14 = replaceCharacteresSql($sheet->getCell("N" . $row)->getValue());
    $campo15 = replaceCharacteresSql($sheet->getCell("O" . $row)->getValue());
    $campo16 = replaceCharacteresSql($sheet->getCell("P" . $row)->getValue());
    $campo17 = replaceCharacteresSql($sheet->getCell("Q" . $row)->getValue());
    $campo18 = replaceCharacteresSql($sheet->getCell("R" . $row)->getValue());
    $campo19 = replaceCharacteresSql($sheet->getCell("S" . $row)->getValue());
    $campo20 = replaceCharacteresSql($sheet->getCell("T" . $row)->getValue());
    $campo21 = replaceCharacteresSql($sheet->getCell("U" . $row)->getValue());
    $campo22 = replaceCharacteresSql($sheet->getCell("V" . $row)->getValue());
    $campo23 = replaceCharacteresSql($sheet->getCell("W" . $row)->getValue());
    $campo24 = replaceCharacteresSql($sheet->getCell("X" . $row)->getValue());
    $campo25 = replaceCharacteresSql($sheet->getCell("Y" . $row)->getValue());
    $campo26 = replaceCharacteresSql($sheet->getCell("Z" . $row)->getValue());

    $campo27 = replaceCharacteresSql($sheet->getCell("AA" . $row)->getValue());
    $campo28 = replaceCharacteresSql($sheet->getCell("AB" . $row)->getValue());
    $campo29 = replaceCharacteresSql($sheet->getCell("AC" . $row)->getValue());
    $campo30 = replaceCharacteresSql($sheet->getCell("AD" . $row)->getValue());
    $campo31 = replaceCharacteresSql($sheet->getCell("AE" . $row)->getValue());
    $campo32 = replaceCharacteresSql($sheet->getCell("AF" . $row)->getValue());
    $campo33 = replaceCharacteresSql($sheet->getCell("AG" . $row)->getValue());
    $campo34 = replaceCharacteresSql($sheet->getCell("AH" . $row)->getValue());
    $campo35 = replaceCharacteresSql($sheet->getCell("AI" . $row)->getValue());
    $campo36 = replaceCharacteresSql($sheet->getCell("AJ" . $row)->getValue());
    $campo37 = replaceCharacteresSql($sheet->getCell("AK" . $row)->getValue());
    $campo38 = replaceCharacteresSql($sheet->getCell("AL" . $row)->getValue());
    $campo39 = replaceCharacteresSql($sheet->getCell("AM" . $row)->getValue());
    $campo40 = replaceCharacteresSql($sheet->getCell("AN" . $row)->getValue());
    $campo41 = replaceCharacteresSql($sheet->getCell("AO" . $row)->getValue());
    $campo42 = replaceCharacteresSql($sheet->getCell("AP" . $row)->getValue());
    $campo43 = replaceCharacteresSql($sheet->getCell("AQ" . $row)->getValue());
    $campo44 = replaceCharacteresSql($sheet->getCell("AR" . $row)->getValue());
    $campo45 = replaceCharacteresSql($sheet->getCell("AS" . $row)->getValue());

    $campo46 = replaceCharacteresSql($sheet->getCell("AT" . $row)->getValue());
    $campo47 = replaceCharacteresSql($sheet->getCell("AU" . $row)->getValue());
    $campo48 = replaceCharacteresSql($sheet->getCell("AV" . $row)->getValue());
    $campo49 = replaceCharacteresSql($sheet->getCell("AW" . $row)->getValue());
    $campo50 = replaceCharacteresSql($sheet->getCell("AX" . $row)->getValue());
    $campo51 = replaceCharacteresSql($sheet->getCell("AY" . $row)->getValue());
    $campo52 = replaceCharacteresSql($sheet->getCell("AZ" . $row)->getValue());
    $campo53 = replaceCharacteresSql($sheet->getCell("BA" . $row)->getValue());
    $campo54 = replaceCharacteresSql($sheet->getCell("BB" . $row)->getValue());
    $campo55 = replaceCharacteresSql($sheet->getCell("BC" . $row)->getValue());
    $campo56 = replaceCharacteresSql($sheet->getCell("BD" . $row)->getValue());
    $campo57 = replaceCharacteresSql($sheet->getCell("BE" . $row)->getValue());
    $campo58 = replaceCharacteresSql($sheet->getCell("BF" . $row)->getValue());
    $campo59 = replaceCharacteresSql($sheet->getCell("BG" . $row)->getValue());
    $campo60 = replaceCharacteresSql($sheet->getCell("BH" . $row)->getValue());
    $campo61 = replaceCharacteresSql($sheet->getCell("BI" . $row)->getValue());
    $campo62 = replaceCharacteresSql($sheet->getCell("BJ" . $row)->getValue());
    $campo63 = replaceCharacteresSql($sheet->getCell("BK" . $row)->getValue());
    $campo64 = replaceCharacteresSql($sheet->getCell("BL" . $row)->getValue());
    $campo65 = replaceCharacteresSql($sheet->getCell("BM" . $row)->getValue());

    //Después del campo 65 hay 4 registros que corresponden a contraseña, email, campo para recuperar contraseña y is_cliente_final (Por defecto vacíos todos)


    $sql.= " ('$campo1', '$campo2', '$campo3', '$campo4', '$campo5', '$campo6', '$campo7', '$campo8', '$campo9', '$campo10', '$campo11', '$campo12', '$campo13','$campo14', '$campo15', '$campo16', '$campo17', '$campo18', '$campo19', '$campo20', '$campo21', '$campo22','$campo23', '$campo24',
'$campo25', '$campo26', '$campo27', '$campo28', '$campo29', '$campo30', '$campo31','$campo32', '$campo33',
'$campo34', '$campo35', '$campo36', '$campo37', '$campo38', '$campo39', '$campo40','$campo41', '$campo42',
'$campo43', '$campo44', '$campo45', '$campo46', '$campo47', '$campo48', '$campo49','$campo50', '$campo51',
'$campo52', '$campo53', '$campo54', '$campo55', '$campo56', '$campo57', '$campo58','$campo59', '$campo60',
'$campo61', '$campo62', '$campo63', '$campo64', '$campo65', '', '', '', '', '".$empresa."'),";

}

$sql_aux = substr($sql, 0, -1);
$total_insert =  $bd->ejecutarReturnAffected($sql_aux);
echo $total_insert . " FILAS NUEVAS DE " . $total . " FILAS ENCONTRADAS EN EL EXCEL clientes DE LA EMPRESA ".$empresa;