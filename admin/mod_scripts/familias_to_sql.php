<?php

require '../../config.php';
require path_class.'phpexcel/PHPExcel.php';
$empresa = "MT";
if(isset($_GET['empresa']) && !empty($_GET['empresa'])):
    $empresa = $_GET['empresa'];
endif;
$archivo = path_excels.$empresa."/familias.xlsx";

$bd = new Db();

$inputFileType = PHPExcel_IOFactory::identify($archivo);
$objReader = PHPExcel_IOFactory::createReader($inputFileType);
$objPHPExcel = $objReader->load($archivo);
$sheet = $objPHPExcel->getSheet(0);
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();

$total_insert = 0;
$total = 0;
$sql = "INSERT INTO familias VALUES ";
for ($row = 2; $row <= $highestRow; $row++){

    $total++;
    $campo1 = replaceCharacteresSql($sheet->getCell("A".$row)->getValue());
    $campo2 = replaceCharacteresSql($sheet->getCell("B".$row)->getValue());
    $campo3 = replaceCharacteresSql($sheet->getCell("C".$row)->getValue());

    //Existe un campo status (1, por defecto), campo min_ran_anc (Minimo de ancho por familia), campo min_ran_alt (Minimo de alto por familia)

    $sql .= " ('$campo1', '$campo2', '$campo3', '1', '', '', '".$empresa."'),";

}

$sql_aux = substr($sql, 0, -1);
$total_insert =  $bd->ejecutarReturnAffected($sql_aux);
echo $total_insert . " FILAS NUEVAS DE " . $total . " FILAS ENCONTRADAS EN EL EXCEL familias de la empresa ".$empresa;