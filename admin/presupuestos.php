<?php

include '../config.php';
check_login_admin();

$page = "presupuestos";
$path = "";
if(isset($_GET['path'])): $path = $_GET['path']; endif;
include 'mod_head/index.php';
include 'mod_nav/index.php';
if($path == "presupuestos" OR empty($path)):
    include 'mod_presupuestos/index.php';
elseif($path == "pedidos"):
    include 'mod_pedidos/index.php';
elseif($path == "pedidos_valorados"):
    include 'mod_pedidos_valorados/index.php';
elseif($path == "solicitud"):
    include 'mod_solicitud/index.php';
endif;
include 'mod_footer/index.php';
