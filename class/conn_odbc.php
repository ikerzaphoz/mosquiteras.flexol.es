<?php

/*
 * Conexión al odbc de la librería interc
 */

Class Conn_odbc {

    private $odbc_name;
    private $user;
    private $pass;

    public function __construct() {
        $this->odbc_name = "INTERC";
        $this->user = "userweb";
        $this->pass = "userweb";
    }

    public function connect() {
        try {
            $conexion = odbc_connect($this->odbc_name, $this->user, $this->pass);
        } catch (Exception $error) {
            echo "Error en la conexión:" . $error->getMessage();
        }
        return $conexion;
    }

    public function close($connect) {
        try {
            odbc_close($connect);
        } catch (Exception $error) {
            echo "Error en la conexión:" . $error->getMessage();
        }
    }

}
