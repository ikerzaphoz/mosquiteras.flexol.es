<?php

Class Lang{

    private $id;
    private $var_name;
    private $lang_es;
    private $lang_pt;

    public function __construct($id = NULL, $var_name = NULL, $lang_es = NULL, $lang_pt = NULL)
    {
        $this->id = $id;
        $this->var_name = $var_name;
        $this->lang_es = $lang_es;
        $this->lang_pt = $lang_pt;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getVarName()
    {
        return $this->var_name;
    }

    public function setVarName($var_name)
    {
        $this->var_name = $var_name;
    }

    public function getLangEs()
    {
        return $this->lang_es;
    }

    public function setLangEs($lang_es)
    {
        $this->lang_es = $lang_es;
    }

    public function getLangPt()
    {
        return $this->lang_pt;
    }

    public function setLangPt($lang_pt)
    {
        $this->lang_pt = $lang_pt;
    }

    public function get_array_lang($lang = NULL){
        $bd = new Db();
        if(isset($lang)):
            $sql= "SELECT id, var_name, lang_".$lang;
        else:
            $sql= "SELECT *";
        endif;
        $sql .= " FROM lang";
        $array = $bd->obtener_consultas($sql, 'id');
        return $array;
    }

    public function update_var_name($id, $lang_es, $lang_pt){
        $bd = new Db();
        $sql = "UPDATE lang SET lang_es='$lang_es', lang_pt='$lang_pt' WHERE id = '$id'";
        return $bd->ejecutarReturnAffected($sql);
    }

}