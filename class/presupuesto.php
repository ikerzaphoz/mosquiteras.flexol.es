<?php

Class Presupuesto {

    private $id;
    private $num_pre;
    private $id_cli;
    private $fecha;
    private $total;

    public function __construct($id = NULL, $num_pre = NULL, $id_cli = NULL, $fecha = NULL, $total = NULL) {
        $this->id = $id;
        $this->num_pre = $num_pre;
        $this->id_cli = $id_cli;
        $this->fecha = $fecha;
        $this->total = $total;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getNumPre() {
        return $this->num_pre;
    }

    public function setNumPre($num_pre) {
        $this->num_pre = $num_pre;
    }

    public function getIdCli() {
        return $this->id_cli;
    }

    public function setIdCli($id_cli) {
        $this->id_cli = $id_cli;
    }

    public function getFecha() {
        return $this->fecha;
    }

    public function setFecha($fecha) {
        $this->fecha = $fecha;
    }

    public function getTotal() {
        return $this->total;
    }

    public function setTotal($total) {
        $this->total = $total;
    }

    public function count($id_cliente = NULL) {
        $bd = new Db();
        $sql = "SELECT count(id) as total FROM presupuestos WHERE (is_pedido = 0 OR is_pedido = 2) AND empresa = 'EN'";
        if (isset($id_cliente)):
            $sql .= " AND id_cliente = '$id_cliente'";
        endif;
        $result = $bd->obtener_consultas($sql);
        return $result[0]['total'];
    }

    public function count_valorado($id_cliente = NULL) {
        $bd = new Db();
        $sql = "SELECT count(id) as total FROM presupuestos WHERE (is_pedido = 0 OR is_pedido = 2) AND empresa = 'EN'";
        if (isset($id_cliente)):
            $sql .= " AND id_cliente = '$id_cliente'";
        endif;
        $result = $bd->obtener_consultas($sql);
        return $result[0]['total'];
    }

    public function count_solicitud($id_cliente = NULL) {
        $bd = new Db();
        $sql = "SELECT count(id) as total FROM presupuestos WHERE is_pedido = 3 AND empresa = 'EN'";
        if (isset($id_cliente)):
            $sql .= " AND id_cliente = '$id_cliente'";
        endif;
        $result = $bd->obtener_consultas($sql);
        return $result[0]['total'];
    }

    public function count_ped($id_cliente = NULL) {
        $bd = new Db();
        $sql = "SELECT count(id) as total FROM presupuestos WHERE is_pedido = 1 AND empresa = 'EN'";
        if (isset($id_cliente)):
            $sql .= " AND id_cliente = '$id_cliente'";
        endif;
        $result = $bd->obtener_consultas($sql);
        return $result[0]['total'];
    }

    public function save_presupuesto($num_pre = NULL, $id_cli = NULL, $fecha = NULL, $total = NULL, $referencia = NULL, $is_pedido = NULL, $agencia = NULL, $cedtop_condiciones = NULL, $cedtoe_condiciones = NULL, $cod_proc = NULL, $cod_clie = NULL) {
        $bd = new Db();
        $sql = "INSERT INTO presupuestos (num_pre, id_cliente, fecha, total, referencia, is_pedido, agencia, cedtop_condiciones, cedtoe_condiciones, empresa, cod_proc, cod_clie) VALUES ('$num_pre', '$id_cli', '$fecha', '$total', '$referencia', '$is_pedido', '$agencia', '$cedtop_condiciones', '$cedtoe_condiciones', 'EN', '$cod_proc', '$cod_clie')";
        $bd->ejecutarReturnAffected($sql);
        return $bd->lastID();
    }

    public function setDireccionEnvio($id_pre, $id_dire) {
        $bd = new Db();
        $sql = "UPDATE presupuestos SET dire_envio = '$id_dire' WHERE id = '$id_pre' AND empresa = 'EN'";
        $result = $bd->ejecutar($sql);
        return $result;
    }

    public function setAgencia($id_pre, $agencia) {
        $bd = new Db();
        $sql = "UPDATE presupuestos SET agencia = '$agencia' WHERE id = '$id_pre' AND empresa = 'EN'";
        $result = $bd->ejecutar($sql);
        return $result;
    }

    public function save_albaran($num_pre = NULL, $albfami = NULL, $albsub = NULL, $albco = NULL, $albunit = NULL, $albanc = NULL, $albalt = NULL, $albpre = NULL, $albobs = NULL, $is_almacen = NULL, $albarti = NULL, $adjunto = NULL, $alb_condiciones = NULL, $is_dto_alb = NULL, $art_desc = NULL, $col_desc = NULL, $linea_producto = NULL) {
        $bd = new Db();
        $sql = "INSERT INTO albaranes (num_pre, albfami, albsub, albco, albunit, albanc, albalt, albpre, albobs, is_almacen, albarti, adjunto, alb_condiciones, is_dto_alb, art_desc, col_desc, linea_producto, empresa) VALUES ('$num_pre', '$albfami', '$albsub', '$albco', '$albunit', '$albanc', '$albalt', '$albpre', '$albobs', '$is_almacen', '$albarti', '$adjunto', '$alb_condiciones', '$is_dto_alb', '$art_desc', '$col_desc', '$linea_producto', 'EN')";
        $result = $bd->ejecutarReturnAffected($sql);
        return $result;
    }

    public function obtener_todos_presupuestos($is_admin = NULL) {
        $bd = new Db();
        $sql = "SELECT * FROM presupuestos WHERE is_pedido = 0 AND empresa = 'EN'";
        if ($is_admin == "1") {
            $sql .= " AND id_cliente != '9999999' AND id_cliente != '' AND status = '1'";
        }
        $sql .= " AND web IS NULL ORDER BY fecha desc";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function obtener_todos_pedidos_valorados($is_admin = NULL) {
        $bd = new Db();
        $sql = "SELECT * FROM presupuestos WHERE is_pedido = 2 AND empresa = 'EN'";
        if ($is_admin == "1") {
            $sql .= " AND id_cliente != '9999999' AND id_cliente != '' AND status = '1'";
        }
        $sql .= " AND web IS NULL ORDER BY fecha desc";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function obtener_todos_pedidos($is_admin = NULL) {
        $bd = new Db();
        $sql = "SELECT * FROM presupuestos WHERE is_pedido = 1 AND empresa = 'EN'";
        if ($is_admin == "1") {
            $sql .= " AND id_cliente != '9999999' AND id_cliente != ''";
        }
        $sql .= " AND web IS NULL ORDER BY fecha desc";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function obtener_solicitudes($is_admin = NULL) {
        $bd = new Db();
        $sql = "SELECT * FROM presupuestos WHERE is_pedido = 3 AND empresa = 'EN'";
        if ($is_admin == "1") {
            $sql .= " AND id_cliente != '9999999' AND status = '1'";
        }
        $sql .= " AND web IS NULL ORDER BY fecha desc";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function getAll($id_cliente = NULL, $is_pedido = NULL, $limit = NULL, $offset = NULL) {
        $bd = new Db();
        $sql = "SELECT * FROM presupuestos";
        if (isset($id_cliente)) {
            $sql .= " WHERE id_cliente = '$id_cliente'";
        }
        $sql .= " AND is_pedido = '$is_pedido' ";
        $sql .= " AND status = 1 AND empresa = 'EN' AND web IS NULL";
        $sql .= " ORDER BY fecha DESC";
        if (isset($limit)):
            $sql .= " LIMIT " . $limit;
        endif;
        if (isset($offset)):
            $sql .= ", " . $offset;
        endif;
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function get_presupuesto($id_pres = NULL) {
        $bd = new Db();
        $sql = "SELECT * FROM presupuestos WHERE id = '$id_pres' AND empresa = 'EN'";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function obtener_numero_presupuesto_por_referencia($id_pres = NULL) {
        $bd = new Db();
        $sql = "SELECT num_pre FROM presupuestos WHERE id = '$id_pres' AND empresa = 'EN'";
        $result = $bd->obtener_consultas($sql);
        return $result[0]['num_pre'];
    }

    public function get_albaranes($id_pres = NULL) {
        $bd = new Db();
        $sql = "SELECT * FROM albaranes WHERE num_pre = '$id_pres' AND albunit > 0 AND empresa = 'EN'";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function is_almacen($id_pres = NULL) {
        $bd = new Db();
        $sql = "SELECT is_almacen FROM albaranes WHERE num_pre = '$id_pres' AND albunit > 0 AND empresa = 'EN'";
        $result = $bd->obtener_consultas($sql);
        return $result[0]['is_almacen'];
    }

    public function count_albaranes($id_pres = NULL) {
        $bd = new Db();
        $sql = "SELECT count(*) as total FROM albaranes WHERE num_pre = '$id_pres' AND empresa = 'EN'";
        $result = $bd->obtener_consultas($sql);
        return $result[0]['total'];
    }

    public function updateObs($id_pres, $obs) {
        $bd = new Db();
        $sql = "UPDATE presupuestos SET observaciones = '$obs' WHERE id = '$id_pres' AND empresa = 'EN'";
        $result = $bd->ejecutarReturnAffected($sql);
        return $result;
    }

    public function search_presupuestos($id_cliente, $search) {
        $bd = new Db();
        $sql = "SELECT * FROM presupuestos WHERE id_cliente = '$id_cliente' AND upper(num_pre) LIKE upper('%$search%') OR fecha LIKE '%$search%' OR upper(observaciones) LIKE upper('%$search%') AND empresa = 'EN' AND web IS NULL";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function getObservaciones($id_pres) {
        $bd = new Db();
        $sql = "SELECT observaciones FROM presupuestos WHERE id = '$id_pres' AND empresa = 'EN'";
        $result = $bd->obtener_consultas($sql);
        return $result[0]['observaciones'];
    }

    //ceprov, cecodi, cefami, cesubf, cearti, cecolo, cedtoa, cepres, ceminf, cedtop, cedtoe, is_almacen
    public function getCondicionesEspeciales($ceprov = NULL, $cecodi = NULL, $cefami = NULL, $cesubf = NULL, $cearti = NULL, $cecolo = NULL, $is_almacen = NULL, $fecha_actual = NULL) {
        $bd = new Db();
        $array_condiciones = array();
        //Obtenemos conficiones especiales del cliente
        $sql = "SELECT * FROM condiciones_especiales WHERE ceprov = '$ceprov' AND cecodi = '$cecodi' AND empresa = 'EN'";
        if (isset($is_almacen)):
            $sql .= " AND is_almacen = '$is_almacen'";
        endif;
        //1. Comprobamos con los datos de familia, subfamilia, articulo, color, fechaDesde, fechaHasta
        if (!empty($cefami) && !empty($cesubf) && !empty($cearti) && !empty($cecolo) && !empty($fecha_actual)):
            $sql1 = $sql . " AND cefami = '$cefami' AND cesubf = '$cesubf' AND cearti = '$cearti' AND cecolo = '$cecolo' AND cedesd <= '$fecha_actual' AND cehast >= '$fecha_actual'";
            $array_condiciones1 = $bd->obtener_consultas($sql1);
            if (!empty($array_condiciones1)):
                $array_condiciones[] = $array_condiciones1;

            endif;
        endif;
        //2. Comprobamos con los datos de familia, subfamilia, articulo y color
        if (!empty($cefami) && !empty($cesubf) && !empty($cearti) && !empty($cecolo)):
            $sql2 = $sql . " AND cefami = '$cefami' AND cesubf = '$cesubf' AND cearti = '$cearti' AND cecolo = '$cecolo'";
            $array_condiciones2 = $bd->obtener_consultas($sql2);
            if (!empty($array_condiciones2)):
                $array_condiciones[] = $array_condiciones2;
            endif;
        endif;
        //3. Comprobamos con los datos familia, subfamilia, articulo, fechaDesde, fechaHasta
        if (!empty($cefami) && !empty($cesubf) && !empty($cearti) && !empty($fecha_actual)):
            $sql3 = $sql . " AND cefami = '$cefami' AND cesubf = '$cesubf' AND cearti = '$cearti' AND cedesd <= '$fecha_actual' AND cehast >= '$fecha_actual'";
            $array_condiciones3 = $bd->obtener_consultas($sql3);
            if (!empty($array_condiciones3)):
                $array_condiciones[] = $array_condiciones3;
            endif;
        endif;
        //4. Comprobamos con los datos de familia, subfamilia, articulo.
        if (!empty($cefami) && !empty($cesubf) && !empty($cearti)):
            $sql4 = $sql . " AND cefami = '$cefami' AND cesubf = '$cesubf' AND cearti = '$cearti'";
            $array_condiciones4 = $bd->obtener_consultas($sql4);
            if (!empty($array_condiciones4)):
                $array_condiciones[] = $array_condiciones4;
            endif;
        endif;
        //5. Comprobamos con los datos de familia, subfamilia, fechaDesde, fechaHasta
        if (!empty($cefami) && !empty($cesubf) && !empty($fecha_actual)):
            $sql5 = $sql . " AND cefami = '$cefami' AND cesubf = '$cesubf' AND cedesd <= '$fecha_actual' AND cehast >= '$fecha_actual'";
            $array_condiciones5 = $bd->obtener_consultas($sql5);
            if (!empty($array_condiciones5)):
                $array_condiciones[] = $array_condiciones5;
            endif;
        endif;
        //6. Comprobamos con los datos de familia, subfamilia
        if (!empty($cefami) && !empty($cesubf)):
            $sql6 = $sql . " AND cefami = '$cefami' AND cesubf = '$cesubf'";
            $array_condiciones6 = $bd->obtener_consultas($sql6);
            if (!empty($array_condiciones6)):
                $array_condiciones[] = $array_condiciones6;
            endif;
        endif;
        //7. Comprobamos con los datos de familia, fechaDesde, fechaHasta
        if (!empty($cefami) && !empty($fecha_actual)):
            $sql7 = $sql . " AND cefami = '$cefami' AND cedesd <= '$fecha_actual' AND cehast >= '$fecha_actual'";
            $array_condiciones7 = $bd->obtener_consultas($sql7);
            if (!empty($array_condiciones7)):
                $array_condiciones[] = $array_condiciones7;
            endif;
        endif;
        //8. Comprobamos con los datos de familia
        if (!empty($cefami)):
            $sql8 = $sql . " AND cefami = '$cefami'";
            $array_condiciones8 = $bd->obtener_consultas($sql8);
            if (!empty($array_condiciones8)):
                $array_condiciones[] = $array_condiciones8;
            endif;
        endif;
        //9. Cliente sin condiciones, sin los datos de familia, subfamilia, articulo, color, fechaDesde, fechaHasta
        if (!empty($ceprov) && !empty($cecodi) && empty($cefami) && empty($cesubf) && empty($cearti) && empty($cecolo) && empty($fecha_actual)):
            $sql9 = $sql . " AND cefami = '' AND cesubf = '' AND cearti = '' AND cecolo = '' AND cedesd <= '' AND cehast >= ''";
            $array_condiciones9 = $bd->obtener_consultas($sql9);
            if (!empty($array_condiciones9)):
                $array_condiciones[] = $array_condiciones9;
            endif;
        endif;

        //10. Cliente sin condiciones, sin los datos de familia, subfamilia, articulo, color
        if (!empty($ceprov) && !empty($cecodi) && empty($cefami) && empty($cesubf) && empty($cearti) && empty($cecolo) && !empty($fecha_actual)):
            $sql10 = $sql . " AND cefami = '' AND cesubf = '' AND cearti = '' AND cecolo = '' AND cedesd <= '$fecha_actual' AND cehast >= '$fecha_actual'";
            $array_condiciones10 = $bd->obtener_consultas($sql10);
            if (!empty($array_condiciones10)):
                $array_condiciones[] = $array_condiciones10;
            endif;
        endif;

        return $array_condiciones;
    }

    public function getCondicionesEspeciales_old($id_prov = NULL, $id_codi = NULL, $id_fami = NULL, $is_almacen = NULL) {
        $bd = new Db();
        $sql = "SELECT * FROM condiciones_especiales WHERE ceprov = '$id_prov' AND cecodi = '$id_codi' AND empresa = 'EN'";
        if (isset($id_fami)):
            $sql .= " AND cefami = '$id_fami'";
        endif;
        if (isset($is_almacen)):
            $sql .= "AND is_almacen = '$is_almacen'";
        endif;
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function getIva($tipo = NULL) {
        $bd = new Db();
        $sql = "SELECT * FROM tipos_iva WHERE tipo = '$tipo' AND empresa = 'MT'";
        $result = $bd->obtener_consultas($sql);
        return $result[0];
    }

    public function getFormaPago($id = NULL) {
        $bd = new Db();
        $sql = "SELECT * FROM formas_de_pago WHERE fpfpag = '$id' AND empresa = 'EN'";
        $result = $bd->obtener_consultas($sql);
        return $result[0];
    }

    public function getAplazamiento($id = NULL) {
        $bd = new Db();
        $sql = "SELECT * FROM aplazamiento WHERE apapla = '$id' AND empresa = 'EN'";
        $result = $bd->obtener_consultas($sql);
        return $result[0];
    }

    public function deletePresupuesto($id = NULL) {
        $bd = new Db();
        $sql = "UPDATE presupuestos SET status = '0' WHERE id = '$id' AND empresa = 'EN'";
        $result = $bd->ejecutarReturnAffected($sql);
        return $result;
    }

    //Función para crear el pedido desde el presupuesto y mantener ambos
    public function setPreToPed($id = NULL) {
        $bd = new Db();
        $return = 0;
        $total_alb = 0;
        $cont_insert = 0;
        $sql_pres = "SELECT * FROM presupuestos WHERE id = '$id' AND empresa = 'EN'";
        $array_pres = $bd->obtener_consultas($sql_pres);
        $cod_proc = str_pad($array_pres[0]['cod_proc'], "2", "0", STR_PAD_LEFT);
        $cod_clie = str_pad($array_pres[0]['cod_clie'], "5", "0", STR_PAD_LEFT);
        $cod_cliente = $cod_proc . $cod_clie;
        $count_ped = $this->count_ped() + 1;
        $count_ped = str_pad($count_ped, "8", "0", STR_PAD_LEFT);
        $num_ped = "Ped. EN. CL " . $cod_cliente . "-" . $count_ped;
        $fecha = date('Y/m/d H:i:s');
        $total = $array_pres[0]['total'];
        $dire_envio = $array_pres[0]['dire_envio'];
        $dire_fac = $array_pres[0]['dire_fac'];
        $obs = $array_pres[0]['observaciones'];
        $ref = $array_pres[0]['referencia'];
        $agencia = $array_pres[0]['agencia'];
        $id_ref = hideNumPre($array_pres[0]['num_pre']);
        $id_ref = hidePpto($id_ref);
        $cedtop_condiciones = $array_pres[0]['cedtop_condiciones'];
        $cedtoe_condiciones = $array_pres[0]['cedtoe_condiciones'];
        $num_pre_pers = $array_pres[0]['num_pre_pers'];
        $insert_new_ped = "INSERT INTO presupuestos (num_pre, id_cliente, fecha, total, dire_envio, dire_fac, observaciones, referencia, is_pedido, ref_pres, status, agencia, empresa, cedtop_condiciones, cedtoe_condiciones, cod_proc, cod_clie, num_pre_pers)
VALUES
('" . $num_ped . "','" . $cod_cliente . "', '" . $fecha . "', '" . $total . "', '" . $dire_envio . "', '" . $dire_fac . "', '" . $obs . "', '" . $ref . "', '1', '" . $id_ref . "', 1, '" . $agencia . "', 'EN', '" . $cedtop_condiciones . "', '" . $cedtoe_condiciones . "', '" . $cod_proc . "', '" . $cod_clie . "', '" . $num_pre_pers . "')";
        $insert_ped = $bd->ejecutarReturnAffected($insert_new_ped);
        $id_new_ped = $bd->lastID();
        if ($insert_ped == 1):
            $sql_update = "UPDATE presupuestos SET ref_pres = '$id_new_ped' WHERE id = '$id' AND empresa = 'EN'";
            $bd->ejecutar($sql_update);
            $total_alb = $this->count_albaranes($id);
            $sql_alb = "SELECT * FROM albaranes WHERE num_pre = $id AND empresa = 'EN'";
            $array_alb = $bd->obtener_consultas($sql_alb);
            foreach ($array_alb as $albaran):
                $albfami = $albaran['albfami'];
                $albsub = $albaran['albsub'];
                $albco = $albaran['albco'];
                $albunit = $albaran['albunit'];
                $albanc = $albaran['albanc'];
                $albalt = $albaran['albalt'];
                $albpre = $albaran['albpre'];
                $albobs = $albaran['albobs'];
                $albAlm = $albaran['is_almacen'];
                $albarti = $albaran['albarti'];
                $albadjunto = $albaran['adjunto'];
                $albordenlinea = $albaran['orden_linea'];
                $albcondiciones = $albaran['alb_condiciones'];
                $albisdto = $albaran['is_dto_alb'];
                $alblineapro = $albaran['linea_producto'];
                $albartdesc = $albaran['art_desc'];
                $albcoldesc = $albaran['col_desc'];
                $insert_alb = "INSERT INTO albaranes (num_pre, albfami, albsub, albco, albunit, albanc, albalt, albpre, albobs, is_almacen , albarti, adjunto, empresa, linea_producto, col_desc, art_desc, orden_linea, alb_condiciones, is_dto_alb)
VALUES
('" . $id_new_ped . "', '" . $albfami . "', '" . $albsub . "', '" . $albco . "', '" . $albunit . "', '" . $albanc . "', '" . $albalt . "', '" . $albpre . "', '" . $albobs . "', '" . $albAlm . "', '" . $albarti . "', '" . $albadjunto . "', 'EN', '" . $alblineapro . "', '" . $albcoldesc . "', '" . $albartdesc . "', '" . $albordenlinea . "', '" . $albcondiciones . "', '" . $albisdto . "')";
                $sql_insert = $bd->ejecutarReturnAffected($insert_alb);
                if ($sql_insert == 1):
                    $cont_insert++;
                endif;
            endforeach;
        endif;
        if ($total_alb == $cont_insert):
            $return = 1;
        endif;
        return $return;
    }

    //Función para crear el pedido desde el presupuesto y mantener ambos
    public function setPreToPedAdmin($id = NULL) {
        $bd = new Db();
        $return = 0;
        $total_alb = 0;
        $cont_insert = 0;
        $sql_pres = "SELECT * FROM presupuestos WHERE id = '$id' AND empresa = 'EN'";
        $array_pres = $bd->obtener_consultas($sql_pres);
        $cod_proc = str_pad($array_pres[0]['cod_proc'], "2", "0", STR_PAD_LEFT);
        $cod_clie = str_pad($array_pres[0]['cod_clie'], "5", "0", STR_PAD_LEFT);
        $cod_cliente = $cod_proc . $cod_clie;
        $count_ped = $this->count_ped() + 1;
        $id_new_ped_aux = $count_ped;
        $count_ped = str_pad($count_ped, "8", "0", STR_PAD_LEFT);
        $num_ped = "Ped. EN. CL " . $cod_cliente . "-" . $count_ped;
        $fecha = date('Y/m/d H:i:s');
        $total = $array_pres[0]['total'];
        $dire_envio = $array_pres[0]['dire_envio'];
        $dire_fac = $array_pres[0]['dire_fac'];
        $obs = $array_pres[0]['observaciones'];
        $ref = $array_pres[0]['referencia'];
        $agencia = $array_pres[0]['agencia'];
        $id_ref = hideNumPreAdmin($array_pres[0]['num_pre'], $cod_proc, $cod_clie);
        $id_ref = hidePptoAdmin($id_ref, $cod_proc, $cod_clie);
        $cedtop_condiciones = $array_pres[0]['cedtop_condiciones'];
        $cedtoe_condiciones = $array_pres[0]['cedtoe_condiciones'];
        $num_pre_pers = $array_pres[0]['num_pre_pers'];
        $insert_new_ped = "INSERT INTO presupuestos (num_pre, id_cliente, fecha, total, dire_envio, dire_fac, observaciones, referencia, is_pedido, ref_pres, status, agencia, empresa, cedtop_condiciones, cedtoe_condiciones, cod_proc, cod_clie, num_pre_pers)
VALUES
('" . $num_ped . "','" . $cod_cliente . "', '" . $fecha . "', '" . $total . "', '" . $dire_envio . "', '" . $dire_fac . "', '" . $obs . "', '" . $ref . "', '1', '" . $id_ref . "', 1, '" . $agencia . "', 'EN', '" . $cedtop_condiciones . "', '" . $cedtoe_condiciones . "', '" . $cod_proc . "', '" . $cod_clie . "', '" . $num_pre_pers . "')";
        $insert_ped = $bd->ejecutarReturnAffected($insert_new_ped);
        $id_new_ped = $bd->lastID();
        if ($insert_ped == 1):
            $sql_update = "UPDATE presupuestos SET ref_pres = '$id_new_ped_aux' WHERE id = '$id' AND empresa = 'EN'";
            $bd->ejecutar($sql_update);
            $total_alb = $this->count_albaranes($id);
            $sql_alb = "SELECT * FROM albaranes WHERE num_pre = $id AND empresa = 'EN'";
            $array_alb = $bd->obtener_consultas($sql_alb);
            foreach ($array_alb as $albaran):
                $albfami = $albaran['albfami'];
                $albsub = $albaran['albsub'];
                $albco = $albaran['albco'];
                $albunit = $albaran['albunit'];
                $albanc = $albaran['albanc'];
                $albalt = $albaran['albalt'];
                $albpre = $albaran['albpre'];
                $albobs = $albaran['albobs'];
                $albAlm = $albaran['is_almacen'];
                $albarti = $albaran['albarti'];
                $albadjunto = $albaran['adjunto'];
                $albordenlinea = $albaran['orden_linea'];
                $albcondiciones = $albaran['alb_condiciones'];
                $albisdto = $albaran['is_dto_alb'];
                $alblineapro = $albaran['linea_producto'];
                $albartdesc = $albaran['art_desc'];
                $albcoldesc = $albaran['col_desc'];
                $insert_alb = "INSERT INTO albaranes (num_pre, albfami, albsub, albco, albunit, albanc, albalt, albpre, albobs, is_almacen , albarti, adjunto, empresa, linea_producto, col_desc, art_desc, orden_linea, alb_condiciones, is_dto_alb)
VALUES
('" . $id_new_ped . "', '" . $albfami . "', '" . $albsub . "', '" . $albco . "', '" . $albunit . "', '" . $albanc . "', '" . $albalt . "', '" . $albpre . "', '" . $albobs . "', '" . $albAlm . "', '" . $albarti . "', '" . $albadjunto . "', 'EN', '" . $alblineapro . "', '" . $albcoldesc . "', '" . $albartdesc . "', '" . $albordenlinea . "', '" . $albcondiciones . "', '" . $albisdto . "')";
                $sql_insert = $bd->ejecutarReturnAffected($insert_alb);
                if ($sql_insert == 1):
                    $cont_insert++;
                endif;
            endforeach;
        endif;
        if ($total_alb == $cont_insert):
            $return = 1;
        endif;
        return $return;
    }

    function obtener_pedido_presupuesto($id = NULL) {
        $bd = new Db();
        $sql = "SELECT * FROM presupuesto WHERE ref_pres = '$id' AND is_pedido = '1'";
        $result = $bd->obtener_consultas($sql);
        return $result[0];
    }

    function obtener_presupuesto_pedido($id = NULL) {
        $bd = new Db();
        $sql = "SELECT * FROM presupuesto WHERE ref_pres = '$id' AND is_pedido = '0'";
        $result = $bd->obtener_consultas($sql);
        return $result[0];
    }

    function actualizar_precio_linea_pedido($id_linea, $precio_actual) {
        $bd = new Db();
        $sql = "UPDATE ALBARANES SET albpre = '" . $precio_actual . "' WHERE id = '" . $id_linea . "' AND empresa = 'EN'";
        $result = $bd->ejecutar($sql);
        return $result;
    }

    function actualizar_fecha_pedido($id_pres, $precio_total) {
        $bd = new Db();
        $fecha = date('Y/m/d H:i:s');
        $sql = "UPDATE PRESUPUESTOS SET fecha = '" . $fecha . "', total = '" . $precio_total . "' WHERE id = '" . $id_pres . "' AND empresa = 'EN'";
        $result = $bd->ejecutar($sql);
        return $result;
    }

    function insertar_concepto($num_pre = NULL, $texto_concepto = NULL, $tipo_concepto = NULL, $aumento_concepto = NULL, $valor_concepto = NULL) {
        $bd = new Db();
        $sql = "INSERT INTO conceptos (num_pre, texto_concepto, tipo_concepto, aumento_concepto, valor_concepto, empresa) VALUES('" . $num_pre . "', '" . $texto_concepto . "', '" . $tipo_concepto . "', '" . $aumento_concepto . "', '" . $valor_concepto . "', 'EN')";
        $result = $bd->ejecutar($sql);
        return $result;
    }

    function eliminar_concepto($num_pre = NULL, $id_linea = NULL) {
        $bd = new Db();
        $sql = "DELETE FROM conceptos WHERE num_pre = '$num_pre' AND id = '$id_linea' AND empresa = 'EN'";
        $result = $bd->ejecutar($sql);
        return $result;
    }

    function get_conceptos_pedido($num_pre = NULL) {
        $bd = new Db();
        $sql = "SELECT * FROM conceptos WHERE num_pre = '" . $num_pre . "' AND empresa = 'EN'";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    function count_concepto($num_pre = NULL) {
        $bd = new Db();
        $sql = "SELECT count(*) as total FROM conceptos WHERE num_pre = '" . $num_pre . "' AND empresa = 'EN'";
        $result = $bd->obtener_consultas($sql);
        return $result[0]['total'];
    }

    function get_adjunto_incremento($num_pre = NULL) {
        $bd = new Db();
        $sql = "SELECT adjunto FROM albaranes WHERE num_pre = '" . $num_pre . "' AND empresa = 'EN'";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    function set_num_personalizado($texto_personalizado = NULL, $num_pre = NULL) {
        $bd = new Db();
        $sql = "UPDATE presupuestos set num_pre_pers = '" . $texto_personalizado . "' WHERE id = '" . $num_pre . "' AND empresa = 'EN'";
        $result = $bd->ejecutar($sql);
        return $result;
    }

    function set_cabecera_personalizada($num_pre = NULL, $texto_cabecera = NULL, $num_pre_pers = NULL) {
        $bd = new Db();
        $sql = "INSERT INTO cabeceras_presupuestos VALUES ('" . $num_pre . "', '" . $texto_cabecera . "', '" . $num_pre_pers . "', 'EN')";
        $result = $bd->ejecutar($sql);
        return $result;
    }

    function get_cabecera_personalizada($id_pre) {
        $bd = new Db();
        $sql = "SELECT * FROM cabeceras_presupuestos WHERE id = '" . $id_pre . "' AND empresa = 'EN'";
        $result = $bd->obtener_consultas($sql);
        return $result[0];
    }

    function comprobar_cliente_presupuesto($id = NULL, $cod_prov = NULL, $cod_clie = NULL) {
        $bd = new Db();
        $sql = "SELECT count(*) as total FROM presupuestos WHERE id = '" . $id . "' AND cod_proc = '" . $cod_prov . "' AND cod_clie = '" . $cod_clie . "'";
        $result = $bd->obtener_consultas($sql);
        return $result[0]['total'];
    }

    function search_admin_pre_ped($tipo_documento = NULL, $busqueda = NULL) {
        $bd = new Db();
        //INSTR('mycol', 'abc') > 0
        //$sql = "SELECT * FROM presupuestos WHERE is_pedido = '$tipo_documento' AND (num_pre LIKE '%$busqueda%' OR id_cliente LIKE '%$busqueda%' OR cod_proc LIKE '%$busqueda%' OR cod_clie LIKE '%$busqueda%') AND empresa = 'EN' ORDER BY fecha desc";
        $sql = "SELECT * FROM presupuestos WHERE is_pedido = '$tipo_documento' AND (UPPER(num_pre) LIKE UPPER('%$busqueda%') OR UPPER(id_cliente) LIKE UPPER('%$busqueda%') OR UPPER(cod_proc) LIKE UPPER('%$busqueda%') OR UPPER(cod_clie) LIKE UPPER('%$busqueda%') OR INSTR(UPPER(num_pre), UPPER('$busqueda')) > 0) AND empresa = 'EN' AND id_cliente != '9999999' AND status = '1' ORDER BY fecha desc";
        if($tipo_documento == "1"):
            $sql = "SELECT * FROM presupuestos WHERE is_pedido = '$tipo_documento' AND (UPPER(num_pre) LIKE UPPER('%$busqueda%') OR UPPER(id_cliente) LIKE UPPER('%$busqueda%') OR UPPER(cod_proc) LIKE UPPER('%$busqueda%') OR UPPER(cod_clie) LIKE UPPER('%$busqueda%') OR INSTR(UPPER(num_pre), UPPER('$busqueda')) > 0) AND empresa = 'EN' AND id_cliente != '9999999' ORDER BY fecha desc";
        endif;
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    function search_admin_pre_ped_all($tipo_documento = NULL) {
        $bd = new Db();
        $sql = "SELECT * FROM presupuestos WHERE is_pedido = '$tipo_documento' AND empresa = 'EN' AND id_cliente != '9999999' AND status = '1' ORDER BY fecha desc";
        if($tipo_documento == "1"):
            $sql = "SELECT * FROM presupuestos WHERE is_pedido = '$tipo_documento' AND empresa = 'EN' AND id_cliente != '9999999' ORDER BY fecha desc";
        endif;
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    function obtener_is_pedido($id = NULL) {
        $bd = new Db();
        $sql = "SELECT is_pedido FROM presupuestos WHERE id = '$id'";
        $result = $bd->obtener_consultas($sql);
        return $result[0]['is_pedido'];
    }

    function obtener_num_pre($id = NULL) {
        $bd = new Db();
        $sql = "SELECT num_pre FROM presupuestos WHERE id = '$id'";
        $result = $bd->obtener_consultas($sql);
        return $result[0]['num_pre'];
    }

}
