<?php

if(!isset($_SESSION))
{
    session_start();
}

if(empty($_SESSION['lang'])):$_SESSION['lang'] = "es"; endif;

if(!empty($_GET['lang']) AND $_GET['lang'] == "es"):
    $_SESSION['lang'] = $_GET['lang'];
endif;

if(!empty($_GET['lang']) AND $_GET['lang'] == "pt"):
    $_SESSION['lang'] = $_GET['lang'];
endif;

include root.'lang/lang_'.$_SESSION['lang'].'.php';

?>