#!/bin/bash
<?php
require '../../../config.php';

ini_set("display_errors", 1);

function hideNumPre_aux($text, $clproc, $clcodi) {
    $text = str_replace('Ppto. EN. CL ', '', $text);
    $text = str_replace('Ped. EN. CL ', '', $text);
    $text = str_replace($clproc . $clcodi . '-', '', $text);
    $text = ltrim($text, '0');
    $text = substr($text, -7);
    return $text;
}

function round_number_to_par_aux($number, $min_value = NULL) {
    if (substr($number, 3, 1) <= 5):
        $number = $number + '0.05';
    endif;
    $number = round($number, 1, PHP_ROUND_HALF_EVEN);
    $number = str_replace(array(",", "."), '', $number);
    if ($number == "1"):
        $number = "10";
    elseif ($number == "2"):
        $number = "20";
    else:
        if ($number % 2 != 0):
            $number++;
        endif;
    endif;
    if ($number < $min_value): $number = $min_value;
    endif;
    return $number;
}

$link = mysqli_connect("217.18.166.150", "sql_mos17", "t5u8KoB4", "mosquiflex") or die('No se pudo conectar: ' . mysqli_error());

$query_familia = mysqli_query($link, "SELECT fafami, fadesc, status, min_ran_anc, min_ran_alt, empresa FROM familias WHERE empresa = 'MT'");
$array_familia = array();
while ($familia = $query_familia->fetch_assoc()):
    $array_familia[$familia['fafami']] = $familia;
endwhile;

$id_pedido = "";
$wbfche = date("Ymd");
$wbhora = date("His");
$numero_albaran = 0;
$total_albaranes = 1;
$fecha = date('d_m_Y');


$query_presupuestos = mysqli_query($link, "SELECT *, DATE_FORMAT(fecha,'%d_%m_%Y') AS fecha2  FROM presupuestos WHERE empresa = 'EN' AND id = '" . $_GET['id_pres'] . "' ORDER BY fecha2");

$hora_actual = date("Y-m-d H:i:s");
while ($presupuesto = $query_presupuestos->fetch_assoc()):

    $mensaje_error_monitor = "";

    $id = $presupuesto['id'];

    if (!empty($id)):


        $insertar_pedidos = 0;
        $insertar_detalle = 0;
        $insertar_dire = 0;

        $query_albaranes = mysqli_query($link, "SELECT * FROM albaranes WHERE num_pre = '$id' AND empresa = 'EN'");
        $query_albaranes_aux = mysqli_query($link, "SELECT * FROM albaranes WHERE num_pre = '$id' AND empresa = 'EN'");

        $fecha = new DateTime($presupuesto['fecha']);
        $fecha = $fecha->format('Ymd');

        $clproc = $presupuesto['cod_proc'];
        $clcodi = $presupuesto['cod_clie'];


        if ($presupuesto['dire_envio'] == 'fiscal' || $presupuesto['dire_envio'] == '0'):
            $abdirv = str_pad(" ", 1);
            $abdire = "000";
            $grabar_dire = "0";
            $query_clientes_aux = mysqli_query($link, "SELECT *  FROM clientes WHERE clproc = '$clproc' AND clcodi = '$clcodi' AND empresa = '" . empresa . "'");
            $result_dire_clientes_aux = $query_clientes_aux->fetch_assoc();
            if (!empty($result_dire_clientes_aux['cldire'])):
                $abdire = str_pad($result_dire_clientes_aux['cldire'], 3, " ", STR_PAD_LEFT);
            endif;
        else:
            $abdirv = str_pad("V", 1);
            $abdire = str_pad($presupuesto['dire_envio'], 3, " ", STR_PAD_LEFT);
            $grabar_dire = "1";
        endif;

        $abaget = "   ";
        if ($presupuesto['agencia'] == "seleccionar_dire"):
            $query_agencia_clientes = mysqli_query($link, "SELECT claget, empresa FROM clientes WHERE clproc = '$clproc' AND clcodi = '$clcodi' AND empresa = '" . empresa . "'");
            $result_agencia_clientes = $query_agencia_clientes->fetch_assoc();
            if (!empty($result_agencia_clientes['claget'])):
                $abaget = str_pad(strtoupper($result_agencia_clientes['claget']), 3, " ", STR_PAD_LEFT);
            endif;
        elseif ($presupuesto['agencia'] == "fiscal"):
            $abaget = "";
        else:
            if (!is_numeric($presupuesto['agencia'])):
                $abaget = strtoupper($presupuesto['agencia']);
            endif;
        endif;

        $abaget = str_pad($abaget, 3, " ", STR_PAD_LEFT);

        if (empty($presupuesto['referencia'])):
            $abspeed = "          ";
        else:
            $abspeed = str_pad(substr($presupuesto['referencia'], 0, 10), 10, " ", STR_PAD_RIGHT);
        endif;
        $abfcsa = str_pad("00000000", 8);

        $abimpr = str_pad("000000000", 9);

        $aux_codi = ltrim($clcodi, "0");

        $id_pres = hideNumPre_aux($presupuesto['num_pre'], $clproc, $aux_codi);

        if (strlen($presupuesto['id_cliente']) == "2"):
            $id_pres = substr($id_pres, 3);
        endif;

        $id_pres = str_pad($id_pres, 7, "0", STR_PAD_LEFT);

        $abtecn = str_pad("221", 3);
        $abalma = str_pad("20", 2);
        $abtipa = str_pad("1", 1);
        $abtipp = str_pad("Q", 1);
        $abfcha = str_pad($fecha, 8);
        //$array_tabla = $odbc->ejecutar_array($conexion, "SELECT * FROM GCWB100");
        $wbnro = count($array_tabla) + 1;
        $wbnro = str_pad($wbnro, 7, "0", STR_PAD_LEFT);
        $wempr = "MT";

        $consulta = "INSERT INTO GCWB100 (WBNRO, WBEMPR, WBALMA, WBTIPA, WBTIPP, WBFCHA, WBPROC, WBCODI, WBDIRV, WBDIRE, WBAGET, WBSPED, WBFCSA, WBIMPR, WBID, WBTECN, WBNPED, WBFCHE, WBHORA) VALUES ('" . $wbnro . "', '" . empresa . "', '" . $abalma . "', '" . $abtipa . "', '" . $abtipp . "', '" . $abfcha . "', '" . $clproc . "', '" . $clcodi . "', '" . $abdirv . "', '" . $abdire . "', '" . $abaget . "', '" . $abspeed . "', '" . $abfcsa . "', '" . $abimpr . "', '" . $id_pres . "', '" . $abtecn . "', '1', '" . $wbfche . "', '" . $wbhora . "')";

        //echo "\n";
        //echo $hora_actual . " -> " . $consulta . "<br>";
        //$insertar_pedidos = $odbc->numero_filas_afectadas($odbc->ejecutar_new($conexion, $consulta));

        $aenord = "0";
        $wenped = 0;
        $aenrr = 0;
        $cont = 0;
        $cont_aux = 0;
        $wenped_aux = 1;
        $array_info_albaran = array();
        $array_info_albaran_aux = array();
        $array_info_albaran_lacado_aux = array();

        $array_numero_albaran = array();
        $array_codigo_producto = array();

        $contador_productos = 0;

        while ($albaran_aux = $query_albaranes_aux->fetch_assoc()):
            $aefami = $albaran_aux['albfami'];
            if (in_array($aefami, array('2731', '2732', '2733', '2734', '2735', '2736'), true) AND empty($albaran_aux['albobs']) || $aefami == "2736"):
                $contador_productos++;
                $buscado = $albaran_aux['albfami'] . "-" . $albaran_aux['albsub'] . "-" . $albaran_aux['albco'];
                $array_codigo_producto[$contador_productos] = $buscado;
                $array_numero_albaran[$contador_productos]['linea_producto'] = $albaran_aux['linea_producto'];
                if ($contador_productos >= 1):
                    $posicion_buscado = array_search($buscado, $array_codigo_producto);
                    if ($posicion_buscado !== $contador_productos):
                        $wenped = $numero_albaran;
                    else:
                        $numero_albaran++;
                        $wenped = $numero_albaran;
                    endif;
                    $array_numero_albaran[$albaran_aux['linea_producto']]['albaran'] = $wenped;
                endif;
            endif;

        endwhile;
        while ($albaran = $query_albaranes->fetch_assoc()):
            if (isset($array_numero_albaran[$albaran['linea_producto']]['albaran'])):
                $wenped = $array_numero_albaran[$albaran['linea_producto']]['albaran'];
            endif;
            $aefami = $albaran['albfami'];
            if (in_array($aefami, array('2731', '2732', '2733', '2734', '2735', '2736'), true) AND empty($albaran['albobs']) || $aefami == "2736"):
                if ($albaran['albobs'] != "lacado"):
                    $array_info_albaran_lacado_aux[$cont]['color'] = $albaran['albco'];
                endif;
            endif;

            $lacado = "0";
            $color_lacado = "";
            mysqli_query($link, "SELECT * FROM colores_ral WHERE coralti = '" . $albaran['albco'] . "' AND is_in_colors = '0' AND empresa = 'MT' AND familia_ral = '" . $aefami . "'");
            $lacado = mysqli_affected_rows($link);
            $color_lacado = $albaran['albco'];

            if (!empty($albaran['albfami']) || $albaran['albobs'] == "Linea3"):

                $aenrr++;
                $aenrr = str_pad($aenrr, 3, "0", STR_PAD_LEFT);
                $aenord = $aenrr * 10;
                $aenord = str_pad($aenord, 4, "0", STR_PAD_LEFT);
                $aetipl = "1";
                if ($albaran['albobs'] == "lacado" || $albaran['albobs'] == "Linea3"):
                    $aearti = str_pad($albaran['albarti'], "6", "0", STR_PAD_LEFT);
                endif;

                if ($albaran['albfami'] == "1999"):
                    $aearti = str_pad($albaran['albarti'], "6", "0", STR_PAD_LEFT);
                endif;

                $aefami = $albaran['albfami'];
                $aesubf = str_pad($albaran['albsub'], 2, "0", STR_PAD_LEFT);
                if (empty($lacado)):
                    $aecolo = str_pad($albaran['albco'], 6, "0", STR_PAD_LEFT);
                else:
                    $aecolo = "999999";
                endif;
                $aerep = str_pad("   ", 3);

                if (in_array($aefami, array('2731', '2734'), true) && strpos($albaran['albobs'], "is_increment") !== false):
                    $aefami = "2799";
                    $aesubf = "90";
                    $aecolo = str_pad("000100", 6);
                    $aearti = str_replace("is_increment279990", "", $albaran['albobs']);
                endif;

                if ($albaran['albobs'] == "lacado"):
                    $encontrado_lacado = "0";
                    foreach ($array_info_albaran_lacado_aux as $index => $lacado_albaran):
                        $color_buscado = str_pad($aecolo, "6", "0", STR_PAD_LEFT);
                        $busca_en = str_pad($lacado_albaran['color'], "6", "0", STR_PAD_LEFT);
                        if ($color_buscado == $busca_en && $encontrado_lacado == "0"):
                            $wenped = $index + 1;
                            $encontrado_lacado = "1";
                        endif;
                    endforeach;
                endif;

                if (in_array($aefami, array('2731', '2732', '2733', '2734', '2735', '2736'), true) AND empty($albaran['albobs']) || $aefami == "2736"):

                    $rango = "";

                    $width_product_anc = number_format($albaran['albanc'], 3, '.', '');
                    $height_product_alt = number_format($albaran['albalt'], 3, '.', '');

                    if ($aefami == 2731):
                        $input_width_product_rango = calc_precio_enrollable_ventana($width_product_anc, $array_familia[$aefami]['min_ran_anc']);
                        $input_height_product_rango = calc_precio_enrollable_ventana($height_product_alt, $array_familia[$aefami]['min_ran_alt']);
                    endif;

                    if ($aefami == 2732):
                        $input_width_product_rango = calc_precio_ancho_enrollable_puerta($width_product_anc, $array_familia[$aefami]['min_ran_anc']);
                        $input_height_product_rango = calc_precio_alto_enrollable_puerta($height_product_alt, $array_familia[$aefami]['min_ran_alt']);
                    endif;

                    if ($aefami == 2733):
                        $aesubf_aux = 0;
                        if ($aesubf >= "11"):
                            $aesubf_aux = "1";
                        endif;
                        $input_width_product_rango = calc_precio_ancho_plisada($width_product_anc, $array_familia[$aefami]['min_ran_anc'], $aesubf_aux);
                        $input_height_product_rango = calc_precio_alto_plisada($height_product_alt, $array_familia[$id_product]['min_ran_alt']);
                    endif;

                    if ($aefami == 2734):
                        $input_width_product_rango = calc_precio_ancho_fija($width_product_anc, $array_familia[$aefami]['min_ran_anc']);
                        $input_height_product_rango = calc_precio_alto_fija($height_product_alt, $array_familia[$id_product]['min_ran_alt']);
                    endif;

                    if ($aefami == 2735):
                        if ($albaran['albsub'] < '11'):
                            $input_width_product_rango = calc_precio_ancho_marco_corredera($width_product_anc, "12");
                            $input_height_product_rango = calc_precio_alto_marco_corredera($height_product_alt, "10");
                        else:
                            $input_width_product_rango = calc_precio_ancho_hoja_corredera($width_product_anc, "6");
                            $input_height_product_rango = calc_precio_alto_hoja_corredera($height_product_alt, "10");
                        endif;
                    endif;

                    if ($aefami == 2736):
                        $input_width_product_rango = calc_precio_ancho_abatible($width_product_anc, $array_familia[$aefami]['min_ran_anc'], $aesubf);
                        $input_height_product_rango = calc_precio_alto_abatible($height_product_alt, $array_familia[$aefami]['min_ran_alt']);
                    endif;
                    $rango = $input_width_product_rango . $input_height_product_rango;
                    $aearti = str_pad($rango, "6", "0", STR_PAD_LEFT);
                    $aerep = str_pad("   ", 3);
                endif;

                if (in_array($aefami, array('1791', '1792', '1793', '1794', '1795', '1796'), true) && strpos($albaran['albobs'], "is_component") !== false):
                    $aearti = $aecolo;
                    $wenped = "1";
                endif;

                $aecapr = str_pad($albaran['albunit'], 5, "0", STR_PAD_LEFT);
                $aeanc = $albaran['albanc'];
                $aealt = $albaran['albalt'];
                $aeanc = str_pad($aeanc, 6, "0", STR_PAD_LEFT);
                $aealt = str_pad($aealt, 6, "0", STR_PAD_LEFT);
                $aeman = str_pad(" ", 2);
                $aecc = str_pad("00000", 5);
                $aealma = $albaran['is_almacen'];
                if ($aealma == "0"):
                    $aealma = "20";
                else:
                    $aealma = "10";
                    $update_wgalma = "UPDATE GCWB100 SET WBTIPP = 'M' WHERE WBFCHE = '" . $wbfche . "' AND WBHORA = '" . $wbhora . "' AND WBEMPR = '" . empresa . "'";

                //echo "\n";
                //echo $hora_actual . " -> " . $update_wgalma . "<br>";
                //$odbc->ejecutar($conexion, $update_wgalma);
                endif;

                $update_wgalma = "UPDATE GCWB100 SET WBALMA = '" . $aealma . "' WHERE WBFCHE = '" . $wbfche . "' AND WBHORA = '" . $wbhora . "' AND WBEMPR = '" . empresa . "'";

                //echo "\n";
                //echo $hora_actual . " -> " . $update_wgalma . "<br>";
                //$odbc->ejecutar($conexion, $update_wgalma);

                $update_wgalma = "UPDATE GCWB100 SET WBNPED = '" . $total_albaranes . "' WHERE WBFCHE = '" . $wbfche . "' AND WBHORA = '" . $wbhora . "' AND WBEMPR = '" . empresa . "'";

                $aetmt = str_pad("0", 6);
                $aever = "0";
                $aeinc = "0";

                $aedesc = "Descripcion articulo";

                if (in_array($aefami, array('2731', '2732', '2733', '2734', '2735', '2736'), true)):
                    $query_desc = mysqli_query($link, "SELECT fadesc, empresa FROM familias WHERE fafami = '$aefami' AND empresa = 'MT'");

                    $desc = $query_desc->fetch_assoc();

                    $aedesc = str_pad($desc['fadesc'], 30, " ", STR_PAD_RIGHT);
                else:
                    $query_desc = mysqli_query($link, "SELECT ardesc FROM articulos WHERE arfami = '" . $aefami . "' AND arsubf = '" . $aesubf . "' AND ararti = '" . $aearti . "' AND empresa = 'MT'");

                    $desc = $query_desc->fetch_assoc();

                    $aedesc = str_pad($desc['ardesc'], 30, " ", STR_PAD_RIGHT);
                endif;

                //$array_tabla = $odbc->ejecutar_array($conexion, "SELECT * FROM GCWE100");
                $wbnro = count($array_tabla) + 1;
                $wbnro = str_pad($wbnro, 7, "0", STR_PAD_LEFT);
                $wempr = "MT";

                if ($aefami == '2731'):
                    $aerep = str_pad("ME1", 3);
                endif;

                if ($aefami == '2736'):
                    $aerep = $albaran['albobs'];
                endif;

                if ($albaran['albobs'] == "Linea3"):
                    $aecapr = 0;
                    $aedesc = str_pad($desc['ardesc'], 30, "-", STR_PAD_RIGHT);
                endif;

                if ($albaran['albobs'] == "lacado"):
                    $aecapr = 1;
                    $aecolo = '100';
                endif;

                if (empresa == "EN" && $aefami == "2799" && $aesubf == "90" && $aearti == "100002"):
                    $aefami = "0000";
                    $aesubf = "00";
                    $aearti = "000000";
                    $aecolo = "000000";
                    $aetipl = "3";
                    $aetipl = "3";
                    $aedesc = "KIT INSTALACION FRONTAL";
                endif;

                if (empresa == "EN" && $aefami == "2799" && $aesubf == "90" && $aearti == "100001"):
                    $aefami = "0000";
                    $aesubf = "00";
                    $aearti = "000000";
                    $aecolo = "000000";
                    $aetipl = "3";
                    $aetipl = "3";
                    $aedesc = "GUIA DOBLE FELPUDO ESTANDAR";
                endif;

                $aerep = strtoupper($aerep);

                $consulta = "INSERT INTO GCWE100 (WENRO, WEEMPR, WEALMA, WENRR, WENORD, WETIPL, WEFAMI, WESUBF, WECOLO, WEARTI, WEDESC, WECAPR, WEANCH, WEALTO, WEMAND, WEREPL, WECC, WETOTM, WEVERT, WEINCL, WEPROC, WECODI, WESPED, WEID, WETECN, WEFCHE, WEHORA, WENPED) VALUES ('" . $wbnro . "', '" . empresa . "', '" . $aealma . "', '" . $aenrr . "', '" . $aenord . "', '" . $aetipl . "', '" . $aefami . "', '" . $aesubf . "', '" . $aecolo . "', '" . $aearti . "', '" . $aedesc . "', '" . $aecapr . "', '" . $aeanc . "', '" . $aealt . "', '" . $aeman . "', '" . $aerep . "', '" . $aecc . "', '" . $aetmt . "', '" . $aever . "', '" . $aeinc . "', '" . $clproc . "', '" . $clcodi . "', '" . $abspeed . "', '" . $id_pres . "', '" . $abtecn . "', '" . $wbfche . "', '" . $wbhora . "', '" . $wenped . "')";
                
                echo "\n";
                echo $hora_actual . " -> " . $consulta . "<br>";

                $total_albaranes = $wenped;

                $update_wenped = "UPDATE GCWB100 SET WBNPED = '" . $wenped . "' WHERE WBFCHE = '" . $wbfche . "' AND WBHORA = '" . $wbhora . "' AND WBEMPR = '" . empresa . "'";

                $consulta = utf8_decode($consulta);

                if ($lacado == "1"):
                    $aedesc = "RAL COLOR " . $color_lacado;
                    $aux_wbnro = $wbnro + 0000001;
                    $consulta = "INSERT INTO GCWE100 (WENRO, WEEMPR, WEALMA, WENRR, WENORD, WETIPL, WEFAMI, WESUBF, WECOLO, WEARTI, WEDESC, WECAPR, WEANCH, WEALTO, WEMAND, WEREPL, WECC, WETOTM, WEVERT, WEINCL, WEPROC, WECODI, WESPED, WEID, WETECN, WEFCHE, WEHORA, WENPED) VALUES ('" . $aux_wbnro . "', '" . empresa . "', '" . $aealma . "', '" . $aenrr . "', '" . $aenord . "', '3', '" . $aefami . "', '" . $aesubf . "', '" . $aecolo . "', '" . $aearti . "', '" . $aedesc . "', '0', '" . $aeanc . "', '" . $aealt . "', '" . $aeman . "', '" . $aerep . "', '" . $aecc . "', '" . $aetmt . "', '" . $aever . "', '" . $aeinc . "', '" . $clproc . "', '" . $clcodi . "', '" . $abspeed . "', '" . $id_pres . "', '" . $abtecn . "', '" . $wbfche . "', '" . $wbhora . "', '" . $wenped . "')";
                    
                    echo "\n";
                    echo $hora_actual . " -> " . $consulta . "<br>";

                endif;

            endif;
        endwhile;



    endif;

endwhile;
