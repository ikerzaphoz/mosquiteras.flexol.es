<?php

require '../config.php';
require path_class.'phpexcel/PHPExcel.php';
$archivo = path_excels."/colores.xlsx";

$bd = new Db();

$inputFileType = PHPExcel_IOFactory::identify($archivo);
$objReader = PHPExcel_IOFactory::createReader($inputFileType);
$objPHPExcel = $objReader->load($archivo);
$sheet = $objPHPExcel->getSheet(0);
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();

$check_update = 0;
$total = 0;
for ($row = 2; $row <= $highestRow; $row++){

    $total++;
    $campo1 = $sheet->getCell("A".$row)->getValue();
    $campo2 = $sheet->getCell("B".$row)->getValue();
    $campo3 = $sheet->getCell("C".$row)->getValue();
    $campo4 = $sheet->getCell("D".$row)->getValue();
    $campo5 = $sheet->getCell("E".$row)->getValue();
    $campo6 = $sheet->getCell("F".$row)->getValue();
    $campo7 = $sheet->getCell("G".$row)->getValue();
    $campo8 = $sheet->getCell("H".$row)->getValue();

    $sql = "INSERT INTO colores VALUES ('$campo1', '$campo2', '$campo3', '$campo4', '$campo5', '$campo6', '$campo7', '$campo8')";
    if($bd->ejecutarReturnAffected($sql) == 1):
        $check_update++;
    endif;

}

echo $check_update . " FILAS NUEVAS DE " . $total . " FILAS ENCONTRADAS EN EL EXCEL";
?>