<?php

require '../config.php';
require path_class.'phpexcel/PHPExcel.php';
$archivo = path_excels."/familias.xlsx";

$bd = new Db();

$inputFileType = PHPExcel_IOFactory::identify($archivo);
$objReader = PHPExcel_IOFactory::createReader($inputFileType);
$objPHPExcel = $objReader->load($archivo);
$sheet = $objPHPExcel->getSheet(0);
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();

$total_insert = 0;
$total = 0;
$sql = "INSERT INTO familias (fafami, fadesc, famaxf) VALUES";
for ($row = 2; $row <= $highestRow; $row++){

    $total++;
    $campo1 = $sheet->getCell("A".$row)->getValue();
    $campo2 = $sheet->getCell("B".$row)->getValue();
    $campo3 = $sheet->getCell("C".$row)->getValue();

    $sql.= "('$campo1', '$campo2', '$campo3'),";

}

$sql = substr($sql, 0, -1);
$total_insert =  $bd->ejecutarReturnAffected($sql);
echo $total_insert . " FILAS NUEVAS DE " . $total . " FILAS ENCONTRADAS EN EL EXCEL";
?>