<?php

require '../config.php';
require path_class.'phpexcel/PHPExcel.php';
$archivo = path_excels."/poblaciones.xlsx";

$bd = new Db();

$inputFileType = PHPExcel_IOFactory::identify($archivo);
$objReader = PHPExcel_IOFactory::createReader($inputFileType);
$objPHPExcel = $objReader->load($archivo);
$sheet = $objPHPExcel->getSheet(0);
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();

$check_update = 0;
$total = 0;
for ($row = 2; $row <= $highestRow; $row++){

    $total++;
    $campo1 = $sheet->getCell("A".$row)->getValue();
    $campo2 = $sheet->getCell("B".$row)->getValue();
    $campo3 = $sheet->getCell("C".$row)->getValue();
    $campo4 = $sheet->getCell("D".$row)->getValue();
    $campo5 = $sheet->getCell("E".$row)->getValue();
    $campo5 = $bd->replace_char_sql($campo5);

    $sql = "INSERT INTO poblaciones VALUES ('$campo1', '$campo2', '$campo3', '$campo4', '$campo5')";
    if($bd->ejecutarReturnAffected($sql) == 1):
        $check_update++;
    else:
        echo $sql."<br>";
    endif;

}

echo $check_update . " FILAS NUEVAS DE " . $total . " FILAS ENCONTRADAS EN EL EXCEL";
?>