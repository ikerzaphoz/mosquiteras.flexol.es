<?php

include '../config.php';

locationToIndex();
check_login();

$page = "cart";
include root.'web/mods/mod_head/index.php';
//Menu
if (dispositivo == "desktop"):
    include root . 'web/mods/mod_nav/index.php';
else:
    include root . 'web/mods/mod_nav/index_mobile.php';
endif;
include root.'web/mods/mod_cart/index.php';
include root.'web/mods/mod_footer/index.php';
include root . 'web/mods/mod_modal/modal_help.php';