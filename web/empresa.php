<?php

include '../config.php';
$page = "empresa";
check_login();

$provincia = new Provincias();
$array_provincias = $provincia->getListadoNombres();
include root.'web/mods/mod_head/index.php';
//Menu
if (dispositivo == "desktop"):
    include root . 'web/mods/mod_nav/index.php';
else:
    include root . 'web/mods/mod_nav/index_mobile.php';
endif;
include root.'web/mods/mod_company/index.php';
include root.'web/mods/mod_footer/index.php';
include root.'web/mods/mod_modal/modal_ini_log.php';
include root . 'web/mods/mod_modal/modal_help.php';