<?php

require '../../../../config.php';

if(!isset($_SESSION))
{
    session_start();
}

if(isset($_SESSION['products'])):
    unset($_SESSION['products']);
endif;

header("Location:".path_web."productos.php?all=all");