<?php

if ($type_session_product == "component"):
    foreach ($array_unidades_componentes as $index => $componente_unidad):

        if (isset($array_data_component[$index])):
            $pos = 0;

            foreach (json_decode($array_data_component[$index], true) as $unidades_medidas):

                if ($unidades_medidas['arsubf'] == $array_subf_componentes[$index] AND $unidades_medidas['ararti'] == $array_ararti_componentes[$index]):

                    $pos++;
                    if (isset($unidades_medidas['ancho']) AND isset($unidades_medidas['alto'])):

                        $array_product = ['id_product' => $array_id_componentes[$index], 'color_product' => $unidades_medidas['ararti'], 'group_color' => '', 'subfamily_product' => $unidades_medidas['arsubf'], 'unit_product' => $unidades_medidas['unidades'], 'width_product' => $unidades_medidas['ancho'], 'height_product' => $unidades_medidas['alto'], 'observaciones_product' => '', 'type_product' => $type_session_product, 'array_increment' => '', 'product_price' => $array_precio_componentes[$index], 'is_almacen' => '0', 'arprpr' => $unidades_medidas['arprpr'], 'tipo_medida_componente' => '1'];


                        $_SESSION['products']['component'][$index] = $array_product;

                    elseif ((isset($unidades_medidas['metros']))):

                        $array_product = ['id_product' => $array_id_componentes[$index], 'color_product' => $array_components_color_componentes[$index], 'group_color' => '', 'subfamily_product' => $array_subf_componentes[$index], 'unit_product' => $array_unidades_componentes[$index], 'width_product' => '', 'height_product' => '', 'unit_meter' => $unidades_medidas['metros'], 'observaciones_product' => 'is_component_meter', 'array_component' => ''];

                        $_SESSION['products']['component'][$index] = $array_product;
                    endif;
                endif;

            endforeach;
        endif;

    endforeach;

elseif ($type_session_product == "component_industrial"):

    if (isset($array_unidades_componentes)):

        foreach ($array_unidades_componentes as $index => $componente_unidad):

            $precio = "";
            //Agrupado por color
            if (isset($array_components_color_componentes[$index]) AND $array_components_color_componentes[$index] != "-"):
                $ararti = $array_ararti_componentes[$index];
                if (strlen($ararti) != 6) {
                    $ararti = $array_ararti_first_componentes[$index] . $array_components_color_componentes[$index];
                }
                $referencia = $array_id_componentes[$index] . "-" . str_pad($array_subf_componentes[$index], 2, "0", STR_PAD_LEFT) . "-" . $ararti;
                $array_info_component = $familia->getInfoComponent($array_id_componentes[$index], $array_subf_componentes[$index], $ararti, $array_arprpr_componentes[$index]);
                $precio_component = round2decimals($array_info_component['arpval']);

                $descripcion = $array_info_component['ardesc'];
            else:
                $is_color = "hidden";
                $referencia = $array_id_componentes[$index] . "-" . str_pad($array_subf_componentes[$index], 2, "0", STR_PAD_LEFT) . "-" . $array_ararti_componentes[$index];
                $ararti = $array_ararti_componentes[$index];
                if (strlen($ararti) != 6) {
                    $ararti = $array_ararti_first_componentes[$index] . $array_components_color_componentes[$index];
                }
                $array_info_component = $familia->getInfoComponent($array_id_componentes[$index], $array_subf_componentes[$index], $array_ararti_componentes[$index], $array_arprpr_componentes[$index]);
                $precio_component = round2decimals($array_info_component['arpval']);
                $descripcion = $array_info_component['ardesc'];
            endif;

            $precio = round2decimals($precio_component * $array_unidades_componentes[$index]);

            $array_product = ['id_product' => $array_id_componentes[$index], 'color_product' => '-', 'subfamily_product' => $array_subf_componentes[$index], 'unit_product' => $array_unidades_componentes[$index], 'width_product' => '', 'height_product' => '', 'product_price' => $precio, 'observaciones_product' => 'is_component', 'type_product' => 'component', 'is_almacen' => '1', 'arprpr' => $array_arprpr_componentes[$index]];

            $_SESSION['products']['component'][$index] = $array_product;

        endforeach;

    endif;


endif;