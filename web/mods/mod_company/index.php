<?php
if (!isset($_SESSION)) {
    session_start();
}

$padding_top = "";
$nombre = "";
$email = "";
$prov = "";
$readonly = "";
$telefono = lang_company_telefono;
if (isset($_SESSION['ini_log']) == 1):

    $nombre = $_SESSION['name_log'];
    $email = $_SESSION['email_log'];
    $prov = $_SESSION['clprov'];
    $telefono = $_SESSION['tlf_log'];
    $readonly = "readonly";
    $padding_top = "pdt25";

endif;
?>
<div id="content_company">
    <div class="col-xs-12 col-md-12">
        <div id="content_estamos" class="col-xs-12 col-md-6">
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3178.304461207778!2d-3.725709084699006!3d37.19299727986958!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd71ff74d15f92a3%3A0xc977632f46ce6498!2sMosquiteras+T%C3%A9cnicas%2C+S.L.!5e0!3m2!1ses!2sus!4v1497430031014"
                width="600" height="515" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <div id="div_contacto" class="<?= $padding_top ?> col-xs-12 col-md-6">
            <div class="info_estamos text-center">
                <h4><?= lang_company_llamenos ?></h4>
                <span><?= lang_company_telefono ?>:</span> (+34) 958 566 955
                <hr>
            </div>
            <div class="form_contacto text-center">
                <h4><?= lang_company_consultenos ?></h4>
                <div class="text-left">
                    <?php if (empty($_SESSION['ini_log'])): ?>
                        <?= lang_company_cliente_registrado ?>:
                        <button
                            class="btn_modal_ini_contacto btn-default btn btn-sm"><?= lang_nav_iniciar_sesion ?></button>
                        <br>
                    <?php endif; ?>
                    <?= lang_company_cliente_no_registrado ?>:
                </div>
                <form class="email_consulta" method="post">
                    <div class="row text-center">
                        <div class="col-xs-6 col-md-6">
                            <input name="nombre" class="form-control" <?= $readonly ?> type="text"
                                   placeholder="<?= lang_company_input_nombre ?>"
                                   value="<?= $nombre ?>">
                        </div>
                        <div class="col-xs-6 col-md-6">
                            <input name="email" class="form-control" <?= $readonly ?> type="email" placeholder="Email"
                                   value="<?= $email ?>">
                        </div>
                    </div>
                    <div class="row text-center mt-3">
                        <div class="col-xs-6 col-md-6">
                            <input name="telefono" class="form-control" <?= $readonly ?> type="text"
                                   placeholder="<?= $telefono ?>">
                        </div>
                        <div class="col-xs-6 col-md-6">
                            <input name="contacto" class="form-control" type="text"
                                   placeholder="<?= lang_company_persona_contacto ?>">
                        </div>
                    </div>
                    <div class="row text-center mt-3">
                        <div class="col-xs-12 col-md-12">
                            <input name="horario" class="form-control" type="text"
                                   placeholder="<?= lang_company_horario ?>">
                        </div>
                    </div>
                    <div class="row text-center mt-3">
                        <div class="col-xs-6 col-md-6">
                            <select name="provincia" class="form-control">
                                <?php
                                selectProvincias_id($array_provincias, $prov);
                                ?>
                            </select>
                        </div>
                        <div class="col-xs-6 col-md-6">
                            <select name="tipo_consulta" name="consulta" class="form-control">
                                <option value="tecnica"><?= lang_company_consulta_tecnica ?></option>
                                <option value="comercial"><?= lang_company_consulta_comercial ?></option>
                                <option value="incidencias"><?= lang_company_consulta_incidencias ?></option>
                                <option value="otras"><?= lang_company_consulta_otras ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="row text-center">
                        <textarea name="consulta" class="text-left form-control noresize"
                                  autofocus="autofocus"></textarea>
                    </div>
                    <div class="row text-center">
                        <button type="submit" class="btn btn-sm btn-default"><?= lang_company_enviar ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
