<?php

require '../../../../config.php';

$cliente = new Customers();

$id_dire_cli = "";
if(isset($_POST['id_dire_cli'])) $id_dire_cli = $_POST['id_dire_cli'];

$affected = $cliente->deleteDire($_SESSION['clproc'], $_SESSION['clcodi'], $id_dire_cli);

$array_error = array('error' => '-2', 'message' => 'Error desconocido');

if($affected == 1):
    $array_error = array('error' => '0', 'message' => 'Dirección eliminada correctamente');
else:
    $array_error = array('error' => '-1', 'message' => 'Error al eliminar dirección');
endif;

echo json_encode($array_error);