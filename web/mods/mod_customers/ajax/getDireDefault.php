<?php

require '../../../../config.php';

$customer = new Customers();
$dire = $customer->get_direccion_envio($_POST['clproc'], $_POST['clcodi'])[0];
$pais = $customer->getPais($dire['clpais']);
$provincia = $customer->getProvincia($dire['clpais'], $dire['clprov']);
$poblacion = $customer->getPoblacion($dire['clpais'], $dire['clprov'], $dire['clpobl']);

?>

<span class="dire_clnomb"><?=$_SESSION['name_log']?></span><br>
<span class="dire_cldir1"><?=$dire['cldir1']?></span>&nbsp;
<span class="dire_cldir2"><?=$dire['cldir2']?></span><br>
<span class="dire_poblacion"><?=$poblacion?></span><br>
<span class="dire_provincia"><?=$provincia?></span><br>
<span class="dire_pais"><?=$pais?></span><br>
<span class="dire_tlf1"><?=$dire['cltlf1']?></span><br>
<span class="dire_tlf2"><?=$dire['cltlf2']?></span>
