<?php

require '../../../../config.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$presupuesto = new Presupuesto();

$id_pres = "";
if (isset($_POST['id_pres']))
    $id_pres = $_POST['id_pres'];

$affected = $presupuesto->setPreToPed($id_pres);

if ($affected == 1):
    $array_error = array('error' => '0', 'message' => 'Modificado correctamente');
    if ($_SESSION['pago_anticipado'] == "1"):

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function

        $mail = new PHPMailer();                              // Passing `true` enables exceptions
        try {
            //Server settings
            $mail->SMTPDebug = 1;                                 // Enable verbose debug output
            //$mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'mail.mosquiflex.es';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'mosquiterastecnicas@mosquiflex.es';                 // SMTP username
            $mail->Password = 'mMosS..TteE..17';                           // SMTP password
            $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 465;                                    // TCP port to connect to
            // Activo condificacción utf-8
            $mail->CharSet = 'UTF-8';
            //Recipients
            $mail->setFrom('ventas@mosquiflex.es', 'Mailer');
            $mail->addAddress('ikerzaphoz@gmail.com', 'Iker');     // Add a recipient
            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'Here is the subject';
            $mail->Body = 'This is the HTML message body <b>in bold!</b>';
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            //$mail->send();
            //echo 'Message has been sent';
        } catch (Exception $e) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }
    endif;
else:
    $array_error = array('error' => '-1', 'message' => 'Error al modificar');
endif;

echo json_encode($array_error);
