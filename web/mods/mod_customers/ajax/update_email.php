<?php

require '../../../../config.php';

$array_response = Array("error" => '1', "message" => "Error al actualizar email");

$email = "";
if (isset($_POST['set_email']))
    $email = $_POST['set_email'];
$check_email = "";
if (isset($_POST['set_email_check']))
    $check_email = $_POST['set_email_check'];
$clprov = "";
if (isset($_POST['clprov_cliente']))
    $clprov = $_POST['clprov_cliente'];
$clcodi = "";
if (isset($_POST['clcodi_cliente']))
    $clcodi = $_POST['clcodi_cliente'];

if (!empty($email) && !empty($check_email) && !empty($clprov) && !empty($clcodi) && $email == $check_email):

    $cliente = new Customers();

    $existe_email = "";
    $existe_email = $cliente->comprobar_existe_email($email);

    if(!empty($existe_email)):
        $array_error = array('error' => '-1', 'message' => 'Email ya registrado. Revise los datos');
        echo json_encode($array_error);
        exit;
    endif;

    $affected = $cliente->setEmailByCodigo($email, $clprov, $clcodi);
    if ($affected == "1"):
        $array_response = Array("error" => '0', "message" => "Email actualizado correctamente");
        $_SESSION['email_log'] = $email;
    endif;

endif;

echo json_encode($array_response, true);
