<?php

require '../../../../config.php';

$array_response = Array("error" => '1', "message" => "Error al actualizar pie documento PVP");

$clproc = "";
if (isset($_SESSION['clproc']))
    $clproc = $_SESSION['clproc'];
$clcodi = "";
if (isset($_SESSION['clcodi']))
    $clcodi = $_SESSION['clcodi'];
$texto_cliente = "";
if (isset($_POST['pie_personalizado']))
    $texto_cliente = replaceCharacteresSql($_POST['pie_personalizado']);

if (!empty($clproc) && !empty($clcodi)):

    $cliente = new Customers();

    $exist_personalizado = $cliente->exist_pie_presupuesto($_SESSION['clproc'], $_SESSION['clcodi']);
    if(empty($exist_personalizado)):
        $affected = $cliente->insert_pie_presupuesto($_SESSION['clproc'], $_SESSION['clcodi'], $texto_cliente);
        if($affected == "1"):
            $array_response = Array("error" => '0', "message" => "Pie añadido correctamente");
        endif;
    else:
        $affected = $cliente->update_pie_presupuesto($_SESSION['clproc'], $_SESSION['clcodi'], $texto_cliente);
        if($affected == "1"):
            $array_response = Array("error" => '0', "message" => "Pie actualizado correctamente");
        endif;
    endif;

endif;

echo json_encode($array_response, true);
exit;
