<?php

require '../../../../config.php';
ini_set("display_errors", 1);

$fecha_hoy = "logos";
if (!is_dir(path_uploads_files . $fecha_hoy)) {
    mkdir(path_uploads_files . $fecha_hoy, 0755, true);
}

$nombre_fichero = strtolower($_SESSION['clproc'] . $_SESSION['clcodi']);
$nombre_fichero = str_replace(" ", "", $nombre_fichero);
$validextensions = array("jpeg", "jpg", "png");

$titulo_respuesta = "Logo presupuesto";

if(empty($_FILES["archivo1"]['size'])):
    $array_error = array('error' => '1', 'message' => 'Ningún archivo seleccionado', 'title' => $titulo_respuesta);
    echo json_encode($array_error);
    exit;
endif;

$array_error = array('error' => '1', 'message' => 'Error al subir archivo', 'title' => $titulo_respuesta);

$files = glob(path_uploads_files . "/" . $fecha_hoy . "/" . $nombre_fichero . ".*");
foreach ($files as $file) {
    unlink($file);
}

foreach ($_FILES["archivo1"] as $key => $error) {
    if ($error == UPLOAD_ERR_OK) {
        $array_nombre_extension = explode('.', $_FILES["archivo1"]["name"]);
        $extension = array_pop($array_nombre_extension);
        if (in_array($extension, $validextensions)):
            if ($_FILES["archivo1"]["size"] < 1048576): //Aprox 1MB
                move_uploaded_file($_FILES["archivo1"]["tmp_name"], path_uploads_files . "/" . $fecha_hoy . "/" . $nombre_fichero . "." . $extension);
                $array_error = array('error' => '0', 'message' => 'Archivo subido correctamente', 'title' => $titulo_respuesta, 'file_title_hidden' => $nombre_fichero . "_." . $extension, 'file_title' => $_FILES["archivo1"]["name"]);
            else:
                $array_error = array('error' => '1', 'message' => 'Error en el tamaño de archivo. Tamaño máximo de 1MB', 'title' => $titulo_respuesta);
            endif;
        else:
            $array_error = array('error' => '1', 'message' => 'Error en la extensión del archivo. Formatos soportados: jpeg, jpg o png', 'title' => $titulo_respuesta);
        endif;
    }
}

echo json_encode($array_error);
exit;
