<?php
$datos_cliente = $cliente->get_cliente($_SESSION['clproc'], $_SESSION['clcodi'])[0];
$dire_cliente = $cliente->get_direcciones_envio($_SESSION['clproc'], $_SESSION['clcodi']);

if (dispositivo == "mobile"):

    include root . 'web/mods/mod_customers/ficha_mobile.php';

else:
    ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center"></h3>
                <div class="tabbable">
                    <ul class="nav nav-stacked col-md-3">
                        <li class="active btn-default border-radius4 hidden"><a href="#misdatos"
                                                                         data-toggle="tab"><?= lang_text_mis_datos ?></a>
                        </li>
                        <li class="hidden"><a href="#misdirecciones" data-toggle="tab"><?= lang_text_mis_direcciones ?></a></li>
                        <li class="mt-15 btn-default border-radius4 bg-border-success"><a target="_blank" href="http://mosquiteras.flexol.es/web/mods/mod_pdf/index.php?pres=0"><?=lang_ficha_button_plantilla?></a></li>
                        <li class="mt-15 active btn-default border-radius4 bg-border-success"><a href="#personalizar_logo" data-toggle="tab"><?=lang_ficha_button_logo?></a></li>
                        <li class="btn-default border-radius4 bg-border-success"><a href="#personalizar_pie" data-toggle="tab"><?=lang_ficha_button_mi_pie?></a></li>
                    </ul>
                    <div class="tab-content col-md-9">
                        <div class="tab-pane active hidden" id="misdatos">
                            <h4 class="text-center"><?= lang_text_datos_fiscales ?></h4>
                            <form class="ver_datos_cliente">
                                <div class="form-group col-md-3">
                                    <label for="clnomb"><?= lang_text_nombre ?>:</label>
                                    <input class="form-control" readonly type="text" name="clnomb" value="<?= $datos_cliente['clnomb'] ?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="cldir1"><?= lang_text_direccion ?> 1:</label>
                                    <input class="form-control" autocomplete="nope" readonly type="text" name="cldir1" value="<?= $datos_cliente['cldir1'] ?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="cldir2"><?= lang_text_direccion ?> 2:</label>
                                    <input class="form-control" readonly type="text" autocomplete="nope" name="cldir2" value="<?= $datos_cliente['cldir2'] ?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="clpais"><?= lang_text_pais ?>:</label>
                                    <input class="form-control" readonly type="text" autocomplete="nope" name="clpais"
                                           value="<?= $cliente->getPais($datos_cliente['clpais']) ?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="clprov"><?= lang_text_provincia ?>:</label>
                                    <input class="form-control" readonly type="text" autocomplete="nope" name="clprov"
                                           value="<?= $cliente->getProvincia($datos_cliente['clpais'], $datos_cliente['clprov']) ?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="clpobl"><?= lang_text_poblacion ?>:</label>
                                    <input class="form-control" readonly type="text" name="clpobl"
                                           value="<?= $cliente->getPoblacion($datos_cliente['clpais'], $datos_cliente['clprov'], $datos_cliente['clpobl']) ?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="cldnic"><?= lant_text_dni_cif ?>:</label>
                                    <input class="form-control" readonly type="text" name="cldnic" value="<?= $datos_cliente['cldnic'] ?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="cltlf1"><?= lang_company_telefono ?> 1:</label>
                                    <input class="form-control" readonly type="text" autocomplete="nope" name="cltlf1" value="<?= $datos_cliente['cltlf1'] ?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="cltlf2"><?= lang_company_telefono ?> 2:</label>
                                    <input class="form-control" readonly type="text" name="cltlf2" autocomplete="nope" value="<?= $datos_cliente['cltlf2'] ?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="clfax"><?= lang_text_fax ?>:</label>
                                    <input class="form-control" readonly type="text" autocomplete="nope" name="clfax" value="<?= $datos_cliente['clfax'] ?>">
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane hidden" id="misdirecciones">
                            <ul class="nav nav-stacked col-md-3">
                                <?php
                                foreach ($dire_cliente as $index => $item):

                                    $active = "";
                                    if ($index == 0): $active = "active";
                                    endif;
                                    ?>
                                    <li class="<?= $active ?> btn-default border-radius4"><a href="#<?= $index ?>"
                                                                                             data-toggle="tab"><?= $item['nombre'] ?></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                            <div class="tab-content col-md-9">
                                <?php
                                foreach ($dire_cliente as $index => $item):
                                    $active = "";
                                    if ($index == 0): $active = "active";
                                    endif;
                                    ?>
                                    <div class="tab-pane <?= $active ?>" id_dire="<?= $item['id'] ?>"
                                         id="<?= $index ?>">
                                        <div class="form-group col-md-4">
                                            <label for="destinatario"><?= lang_text_destinatario ?>:</label>
                                            <input class="form-control" readonly type="text" name="destinatario" value="<?= $item['destinatario'] ?>">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="cldir1"><?= lang_text_direccion ?> 1:</label>
                                            <input class="form-control" readonly type="text" autocomplete="nope" name="cldir1" value="<?= $item['cldir1'] ?>">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="cldir2"><?= lang_text_direccion ?> 2:</label>
                                            <input class="form-control" readonly type="text" autocomplete="nope" name="cldir2" value="<?= $item['cldir2'] ?>">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="clpais"><?= lang_text_pais ?>:</label>
                                            <input class="form-control" readonly type="text" autocomplete="nope" name="clpais"
                                                   value="<?= $cliente->getPais($item['clpais']) ?>">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="clprov"><?= lang_text_provincia ?>:</label>
                                            <input class="form-control" readonly type="text" autocomplete="nope" name="clprov"
                                                   value="<?= $cliente->getProvincia($item['clpais'], $item['clprov']) ?>">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="clpobl"><?= lang_text_poblacion ?>:</label>
                                            <input class="form-control" readonly type="text" name="clpobl"
                                                   value="<?= $cliente->getPoblacion($item['clpais'], $item['clprov'], $item['clpobl']) ?>">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="cltlf1"><?= lang_company_telefono ?> 1:</label>
                                            <input class="form-control" readonly type="text" autocomplete="nope" name="cltlf1" value="<?= $item['cltlf1'] ?>">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="cltlf2"><?= lang_company_telefono ?> 2:</label>
                                            <input class="form-control" readonly type="text" autocomplete="nope" name="cltlf2" value="<?= $item['cltlf2'] ?>">
                                        </div>
                                    </div>
                                    <?php endforeach; ?>
                                <div class="row text-center col-md-12">
    <?php if (!empty($dire_cliente)): ?>
                                        <button
                                            class="btn-sm btn btn-danger btn_delete_dire_cliente"><?= lang_text_eliminar_direccion ?></button>
    <?php endif; ?>
                                    <button data-toggle="modal" data-target="#modal_add_dire_envio"
                                            title="Añadir dirección" class="btn_add_dire_cliente btn-sm btn btn-info"
                                            aria-hidden="true"><?= lang_text_add_direccion ?></button>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane active" id="personalizar_logo">
                            <?php include 'personalizar/ficha_logo.php'; ?>
                        </div>
                        <div class="tab-pane" id="personalizar_pie">
                            <?php include 'personalizar/ficha_pie.php'; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php endif; ?>