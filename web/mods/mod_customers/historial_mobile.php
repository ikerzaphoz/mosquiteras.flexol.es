<div class="container container_historial">

    <h4 class="text-center"><?= lang_text_historial ?></h4>
    <div class="tabbable">
        <ul class="nav nav-tabs col-md-12 col-xs-12 content_buttons_historial">
            <li class="<?php if (empty($tab) OR $tab == "presupuesto"): echo "active"; endif; ?> nav-item">
                <a href="#presupuestos" data-toggle="tab"><?= lang_text_historial_mis_presupuestos ?></a>
            </li>
            <li class="<?php if ($tab == "pedidos"): echo "active"; endif; ?> nav-item">
                <a href="#pedidos" data-toggle="tab"><?= lang_text_historial_mis_pedidos ?></a>
            </li>
        </ul>
        <div class="tab-content col-md-12 col-xs-12 nopadding">
            <div class="tab-pane <?php if (empty($tab) OR $tab == "presupuesto"): echo "active"; endif; ?>"
                 id="presupuestos">
                <div class="content_table_historial row col-md-12 col-xs-12">
                    <div class="row col-md-12 col-xs-12 text-center first_line_table">
                        <div class="col-md-1 col-xs-1">#</div>
                        <div class="col-md-1 col-xs-1"><?= lang_text_historial_abreviatura_referencia ?></div>
                        <div class="col-md-3 col-xs-3"><?= lang_text_historial_fecha ?></div>
                        <div class="col-md-3 col-xs-3"><?= lang_text_historial_validez ?></div>
                        <div class="col-md-2 col-xs-2"><?= lang_text_historial_abreviatura_documentos ?></div>
                        <div class="col-md-2 col-xs-2"><?= lang_text_historial_abreviatura_opciones ?></div>
                    </div>
                    <?php foreach ($array_presupuestos as $index => $item):

                        $is_par = "";
                        $bg_color = "bg-color-white";
                        if ($index % 2 == 0) {
                            $is_par = "bg-color-line";
                        }

                        $referencia = "-";

                        if (!empty($item['referencia'])):
                            $referencia = $item['referencia'];
                        endif;

                        $fecha = $item['fecha'];
                        $fecha = date_create($fecha);
                        $fecha = date_format($fecha, "d/m/Y");

                        $fecha_validez = "";
                        $fecha_validez = strtotime('+1 month', strtotime($item['fecha']));
                        $fecha_validez_aux = $fecha_validez;
                        $fecha_validez = date('d/m/Y', $fecha_validez);

                        $fecha_actual = strtotime(date("d-m-Y H:i:00", time()));

                        $is_valido = "1";

                        if ($fecha_actual > $fecha_validez_aux):
                            $is_valido = "0";
                        endif;

                        ?>
                        <div class="col-xs-12 row col-md-12 text-center file_historial <?= $is_par ?>">
                            <div
                                class="col-md-1 col-xs-1 nopadding"><?= str_replace("Ppto. ", "", hideNumPre($item['num_pre'])) ?></div>
                            <div class="col-md-1 col-xs-1 nopadding"><?= substr($referencia, 0, 10) ?></div>
                            <div class="col-md-3 col-xs-3 nopadding"><?= $fecha ?></div>
                            <div class="col-md-3 col-xs-3 nopadding"><?= $fecha_validez ?></div>
                            <div class="col-md-2 col-xs-2 nopadding">
                                <div class="btn-group">
                                    <button type="button"
                                            class="btn btn-default dropdown-toggle button_group_historial_mobile"
                                            data-toggle="dropdown">
                                        <?= lang_text_historial_abreviatura_documentos ?>
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown_button_group_historial_mobile">
                                        <li>
                                            <a target="_blank"
                                               href="<?= path_web_mods ?>mod_pdf/index.php?pres=<?= $item['id'] ?>"><span
                                                    class="<?= $bg_color ?> btn-xs btn_pdf_pedido btn bg-border-warning"><?= lang_text_historial_pvp ?></span></a>
                                        </li>
                                        <li>
                                            <a target="_blank"
                                               href="<?= path_web_mods ?>mod_pdf/index.php?pres_cond=<?= $item['id'] ?>"><span
                                                    class="<?= $bg_color ?> btn-xs bg-border-info btn_pdf_pedido btn"><?= lang_text_historial_condiciones ?></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-2 nopadding">
                                <div class="btn-group pull-right">
                                    <button type="button"
                                            class="btn btn-default dropdown-toggle button_group_historial_mobile"
                                            data-toggle="dropdown">
                                        <?= lang_text_historial_abreviatura_opciones ?>
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown_button_group_historial_mobile">
                                        <?php if ($is_valido == 1): ?>
                                            <?php if (!isset($item['ref_pres'])): ?>
                                                <button
                                                    class="<?= $bg_color ?> btn btn-xs icon_exchange_presupuestos bg-border-success"
                                                    id_pres="<?= $item['id'] ?>"><?= lang_text_historial_confirmar ?>
                                                </button>
                                                <button
                                                    class="<?= $bg_color ?> btn btn-xs icon_trash_presupuestos bg-border-danger"
                                                    id_pres="<?= $item['id'] ?>"><?= lang_text_historial_eliminar ?>
                                                </button>
                                            <?php else: ?>
                                                <?php
                                                $num_ped = $presupuesto->obtener_numero_presupuesto_por_referencia($item['ref_pres']);
                                                echo "Pedido: " . hidePed(hideNumPed($num_ped));
                                                ?>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <span class="fa icon_repeat_presupuestos fa-repeat"
                                                  id_pres="<?= $item['id'] ?>"
                                                  aria-hidden="true"></span>
                                        <?php endif; ?>
                                        <?php if (!isset($item['observaciones'])): ?>
                                            <button data-toggle="modal" data-target="#modal_edit_notas"
                                                    title="Mis notas"
                                                    class="<?= $bg_color ?> btn_ver_notas btn-xs btn bg-border-info"
                                                    aria-hidden="true"><i
                                                    class="fa fa-eye"></i>&nbsp;<?= lang_text_historial_ver ?>
                                            </button>
                                        <?php else: ?>
                                            <button data-toggle="modal" data-target="#modal_edit_notas"
                                                    title="Mis notas"
                                                    class="<?= $bg_color ?> btn_ver_notas btn-xs btn bg-border-info"
                                                    aria-hidden="true"><i
                                                    class="fa fa-eye"></i>&nbsp;<?= lang_text_historial_ver ?>
                                            </button>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </div>
                            <input type="hidden" class="id_pres" value="<?= $item['id'] ?>">
                            <input type="hidden" class="id_cliente" value="<?= $id_cliente ?>">
                        </div>
                    <?php endforeach; ?>
                </div>
                <?php if ($total_page_pres > 1): ?>
                    <nav aria-label="Page navigation example" class="pagination_historial">
                        <ul class="pagination" type_doc="0">
                            <?php if ($page != 1): ?>
                                <li class="page_pres_ped pagination_historial_li"><a class="page-link"
                                                                                     page="<?= $page - 1 ?>"
                                                                                     href="#"><?= lang_text_abreviatura_anterior ?></a>
                                </li>
                            <?php endif; ?>
                            <?php for ($i = 1; $i <= $total_page_pres; $i++): ?>
                                <?php if ($page == $i): ?>
                                    <li class="pagination_historial_li_active"><a><?= $i ?></a></li>
                                <?php else: ?>
                                    <li class="page_pres_ped pagination_historial_li"><a class="page-link"
                                                                                         page="<?= $i ?>"
                                                                                         href="#"><?= $i ?></a></li>
                                <?php endif; ?>
                            <?php endfor ?>
                            <?php if ($page != $total_page_pres): ?>
                                <li class="page_pres_ped pagination_historial_li"><a class="page-link"
                                                                                     page="<?= $page + 1 ?>"
                                                                                     href="#"><?= lang_text_abreviatura_siguiente ?></a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </nav>
                <?php endif; ?>
                <input type="hidden" class="limit_page" value="<?= $limit ?>">
            </div>
            <div class="tab-pane <?php if ($tab == "pedidos"): echo "active"; endif; ?>" id="pedidos">
                <div class="content_table_historial row col-md-12 col-xs-12">
                    <div class="row col-md-12 col-xs-12 text-center first_line_table">
                        <div class="col-md-1 col-xs-1">#</div>
                        <div class="col-md-2 col-xs-2"><?= lang_text_historial_abreviatura_referencia ?>.</div>
                        <div class="col-md-2 col-xs-2">Ref. Ppto</div>
                        <div class="col-md-3 col-xs-3"><?= lang_text_historial_fecha ?></div>
                        <div class="col-md-2 col-xs-2"><?= lang_text_historial_abreviatura_documentos ?></div>
                        <div class="col-md-2 col-xs-2"><?= lang_text_historial_notas ?></div>
                    </div>
                    <?php foreach ($array_pedidos as $index => $item): ?>
                        <?php

                        $is_par = "";
                        $bg_color = "bg-color-white";
                        if ($index % 2 == 0) {
                            $is_par = "bg-color-line";
                        }

                        $referencia = "-";

                        if (!empty($item['referencia'])):
                            $referencia = $item['referencia'];
                        endif;

                        $fecha = $item['fecha'];
                        $fecha = date_create($fecha);
                        $fecha = date_format($fecha, "d/m/Y");

                        $ref_presupuesto = "-";
                        if (!empty($item['ref_pres'])):
                            $ref_presupuesto = $item['ref_pres'];
                        endif;

                        ?>

                        <div class="col-xs-12 row col-md-12 text-center file_historial <?= $is_par ?>">
                            <div
                                class="col-md-1 col-xs-1 nopadding"><?= str_replace("Ped. ", "", hideNumPed($item['num_pre'])); ?></div>
                            <div class="col-md-2 col-xs-2 nopadding"><?= substr($referencia, 0, 10) ?></div>
                            <div class="col-md-2 col-xs-2 nopadding"><?= $ref_presupuesto ?></div>
                            <div class="col-md-3 col-xs-3 nopadding"><?= $fecha ?></div>
                            <div class="col-md-2 col-xs-2 nopadding">
                                <div class="btn-group">
                                    <button type="button"
                                            class="btn btn-default dropdown-toggle button_group_historial_mobile"
                                            data-toggle="dropdown">
                                        <?= lang_text_historial_abreviatura_documentos ?>
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown_button_group_historial_mobile">
                                        <li>
                                            <a target="_blank"
                                               href="<?= path_web_mods ?>mod_pdf/index.php?pres=<?= $item['id'] ?>">
                                    <span
                                        class="<?= $bg_color ?> btn-xs btn_pdf_pedido btn bg-border-warning"><?= lang_text_historial_pvp ?></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a target="_blank"
                                               href="<?= path_web_mods ?>mod_pdf/index.php?pres_cond=<?= $item['id'] ?>">
                                    <span
                                        class="<?= $bg_color ?> btn-xs bg-border-info btn_pdf_pedido btn"><?= lang_text_historial_condiciones ?></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-2 nopadding">
                                <?php if (!isset($item['observaciones'])): ?>
                                    <button data-toggle="modal" data-target="#modal_edit_notas" title="Mis notas"
                                            class="<?= $bg_color ?> btn_ver_notas btn-xs btn bg-border-success"
                                            aria-hidden="true"><?= lang_text_historial_add ?>
                                    </button>
                                <?php else: ?>
                                    <button data-toggle="modal" data-target="#modal_edit_notas" title="Mis notas"
                                            class="<?= $bg_color ?> btn_ver_notas btn-xs btn bg-border-info"
                                            aria-hidden="true"><i
                                            class="fa fa-eye"></i>&nbsp;<?= lang_text_historial_ver ?>
                                    </button>
                                <?php endif; ?>
                            </div>
                            <input type="hidden" class="id_pres" value="<?= $item['id'] ?>">
                            <input type="hidden" class="id_cliente" value="<?= $id_cliente ?>">
                        </div>
                    <?php endforeach; ?>
                </div>
                <?php if ($total_page_ped > 1): ?>
                    <nav aria-label="Page navigation example" class="pagination_historial">
                        <ul class="pagination" type_doc="1">
                            <?php if ($page != 1): ?>
                                <li class="page_pres_ped pagination_historial_li"><a class="page-link"
                                                                                     page="<?= $page - 1 ?>"
                                                                                     href="#"><?= lang_text_abreviatura_anterior ?></a>
                                </li>
                            <?php endif; ?>
                            <?php for ($i = 1; $i <= $total_page_ped; $i++): ?>
                                <?php if ($page == $i): ?>
                                    <li class="pagination_historial_li_active"><a><?= $i ?></a></li>
                                <?php else: ?>
                                    <li class="page_pres_ped pagination_historial_li"><a class="page-link"
                                                                                         page="<?= $i ?>"
                                                                                         href="#"><?= $i ?></a></li>
                                <?php endif; ?>
                            <?php endfor ?>
                            <?php if ($page != $total_page_ped): ?>
                                <li class="page_pres_ped pagination_historial_li"><a class="page-link"
                                                                                     page="<?= $page + 1 ?>"
                                                                                     href="#"><?= lang_text_abreviatura_siguiente ?></a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </nav>
                <?php endif; ?>
                <input type="hidden" class="limit_page_ped" value="<?= $limit ?>">
            </div>
        </div>
    </div>
</div>