<?php

if (!isset($_SESSION)):
    session_start();
endif;

?>

<?php

if (!isset($_SESSION['ini_log'])):

    ?>
    <div id="content_clientes">
        <div id="content_login" class="row">
            <?php if (isset($is_flotante)): ?>
            <form id="form_login_flotante" class="text-center">
                <?php else: ?>
                <form id="form_login" class="form_login text-center" method="post"
                      action="<?= path_web_mods ?>mod_customers/action/ini_log.php">
                    <?php endif;
                    ?>

                    <div class="hidden div_error_login text-center col-md-12 col-xs-12 alert-danger alert">
                        <strong><span class="fa fa-exclamation"></span></strong> Error al introducir los datos
                    </div>

            <span class="title_form_login">
                <span><?= lang_text_acceso_cliente ?></span>
                <i class="glyphicon glyphicon-user"></i>
            </span>
                    <input name="user_login" data-toggle="tooltip" data-placement="top"
                           placeholder="<?= lang_text_email_nombre_usuario ?>" title="Email / Nombre usuario"
                           type="text" id="user_login" class="form-control user_login">
                    <input name="pass_login" data-toggle="tooltip" data-placemente="top"
                           placeholder="<?= lang_text_contraseña ?>" title="Contraseña" type="password" id="pass_login"
                           class="form-control pass_login">
                    <?php

                    if (isset($is_flotante)): ?>
                        <input type="submit" class="btn-sm btn-ini-log-flotante btn bg-border-warning center-block"
                               value="<?= lang_nav_iniciar_sesion ?>">
                    <?php else: ?>
                        <input type="submit" id="submit_login" class="btn-sm btn bg-border-warning center-block"
                               value="<?= lang_nav_iniciar_sesion ?>">
                    <?php endif; ?>
                    <input type="hidden" class="option_login" name="id_option_modal" value="">

                    <div class="div_error_login text-center hidden col-md-12 col-xs-12">
                        Click <a href="<?=path_web?>clientes.php?opt=remember_pass">aquí</a> para recordar sus datos de acceso
                    </div>
                    <div class="div_error_login text-center col-md-12 col-xs-12">
                        <?=lang_no_contraseña?> Click <a target="_blank" href="<?=path_web?>clientes.php?opt=remember_pass"><?=lang_aqui?></a> <?=lang_no_contraseña_1?>
                    </div>
                </form>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-12 text-right">
                <a target="_blank" href="<?=path_web?>clientes.php?opt=remember_pass"><?=lang_recordar_contraseña?></a>
            </div>
        </div>
    </div>

    <?php

else:

    echo $_SESSION['name_log'];

endif;