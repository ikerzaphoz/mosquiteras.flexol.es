<?php 

$logo_personalizado = false;
if ($imagen_preview = glob(path_uploads_files . "logos/" . $_SESSION['clproc'] . $_SESSION['clcodi'] . ".*")): 
    $logo_personalizado = true;
    $array_nombre_extension = explode('.', $imagen_preview[0]);
    $extension = array_pop($array_nombre_extension);
 endif;
?>
<div class="row col-md-12 col-xs-12">
    <?php if($logo_personalizado):?>
    <div class="col-md-12 col-xs-12">
        <span><?=lang_ficha_title_vista_previa?></span>
    </div>
    <div class="col-md-12 col-xs-12 content_logo_cliente_preview mt-10">
        <img alt="imagen_logo_cliente_preview" class="imagen_logo_cliente_preview" src="<?= url_web . "uploads_files/logos/" . $_SESSION['clproc'] . $_SESSION['clcodi'] . "." . $extension . "?v=" . microtime(); ?>">
    </div>
    <div class="col-md-12 col-xs-12 mt-10">
        <button class="btn btn-default btn_upload_file bg-border-success btn-sm">Sustituir logo</button>
    </div>
    <?php else:?>
    <div class="col-md-12 col-xs-12">
        <span><?=lang_ficha_title_no_logo?></span>
    </div>
    <div class="col-md-12 col-xs-12 mt-10">
        <button class="btn btn-default btn_upload_file bg-border-success btn-sm"><?=lang_ficha_button_subir_logo?></button>
    </div>
    <?php endif; ?>
</div>