<?php

$pie_personalizado = "";
$pie_personalizado = $cliente->get_pie_presupuesto($_SESSION['clproc'], $_SESSION['clcodi']);

?>
<div class="row col-md-12 col-xs-12">
    <?php if(empty($pie_personalizado[0]['pie_personalizado'])): ?>
    <div class="row col-md-12 col-xs-12 mt-10">
        <span><?=lang_ficha_texto_pie?></span>
    </div>
    <?php endif;?>
    <form class="update_pie_presupuesto">
        <div class="row col-md-12 col-xs-12">
            <textarea style="height: 200px;" class="editor_text w100 noresize" name="pie_personalizado"><?php echo characteresSqlToHtml($pie_personalizado[0]['pie_personalizado']); ?></textarea>
        </div>
    </form>
    <div class="text-center row col-md-12 col-xs-12 mt-10">
        <button class="btn_update_pie_personalizado btn btn-sm bg-border-success btn-default"><?=lang_ficha_guardar_texto_pie?></button>
    </div>
</div>