<?php

$email_footer = "";
/*
$email_footer = "<html xmlns:v='urn:schemas-microsoft-com:vml' xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:word' xmlns:m='http://schemas.microsoft.com/office/2004/12/omml' xmlns='http://www.w3.org/TR/REC-html40'><head><meta http-equiv=Content-Type content='text/html; charset=iso-8859-1'><meta name=Generator content='Microsoft Word 12 (filtered medium)'><!--[if !mso]><style>v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
w\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style><![endif]--><style><!--
/* Font Definitions
@font-face
	{font-family:'Cambria Math';
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
@font-face
	{font-family:Verdana;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
/* Style Definitions
p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:11.0pt;
	font-family:'Calibri','sans-serif';}
a:link, span.MsoHyperlink
	{mso-style-priority:99;
	color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-priority:99;
	color:purple;
	text-decoration:underline;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-priority:99;
	mso-style-link:'Texto de globo Car';
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:8.0pt;
	font-family:'Tahoma','sans-serif';}
span.EstiloCorreo17
	{mso-style-type:personal-compose;
	font-family:'Calibri','sans-serif';
	color:windowtext;}
span.TextodegloboCar
	{mso-style-name:'Texto de globo Car';
	mso-style-priority:99;
	mso-style-link:'Texto de globo';
	font-family:'Tahoma','sans-serif';}
.MsoChpDefault
	{mso-style-type:export-only;}
@page WordSection1
	{size:612.0pt 792.0pt;
	margin:70.85pt 3.0cm 70.85pt 3.0cm;}
div.WordSection1
	{page:WordSection1;}
--></style><!--[if gte mso 9]><xml>
<o:shapedefaults v:ext='edit' spidmax='2050' />
</xml><![endif]--><!--[if gte mso 9]><xml>
<o:shapelayout v:ext='edit'>
<o:idmap v:ext='edit' data='1' />
</o:shapelayout></xml><![endif]--></head><body lang=ES link=blue vlink=purple><div class=WordSection1><p class=MsoNormal><b><i><span style='font-size:18.0pt;font-family:'Verdana','sans-serif';font-variant:small-caps;color:black'><img width=162 height=31 id='_x0000_i1027' src='http://www.flexol.es/SoloLogo.png'></span></i></b><b><i><span style='font-size:18.0pt;font-family:'Verdana','sans-serif';font-variant:small-caps;color:black'><o:p></o:p></span></i></b></p><p class=MsoNormal><b><i><span style='font-size:13.0pt;font-family:'Verdana','sans-serif';color:gray'>E</span></i></b><b><i><span style='font-size:10.0pt;font-family:'Verdana','sans-serif';color:gray'>NTRECORTINAS S.L.<o:p></o:p></span></i></b></p><p class=MsoNormal><b><i><span style='font-size:9.0pt;color:gray'>Departamento de Clientes </span></i></b><a href='mailto:clientes@flexol.es'><i><span style='font-size:9.0pt'>clientes@flexol.es</span></i></a><i><span style='font-size:9.0pt;color:black'>&nbsp; </span></i><i><span style='font-size:9.0pt;color:black'><o:p></o:p></span></i></p><p class=MsoNormal><i><span style='font-size:9.0pt;color:black'>Tel: 958 511 424 - Fax: 958 511 386<o:p></o:p></span></i></p><p class=MsoNormal style='mso-margin-top-alt:6.0pt;margin-right:0cm;margin-bottom:6.0pt;margin-left:0cm'><i><span style='font-size:9.0pt;color:black'><img border=0 width=64 height=63 id='_x0031_5_x0020_Imagen' src='http://www.flexol.es/SGS-ISO_9001.jpg' alt='SGS-ISO 9001-COLOR.jpg'></span></i><i><span style='font-size:8.0pt;color:green'> </span></i><i><span style='font-size:9.0pt;color:black'><img border=0 width=64 height=63 id='_x0033_1_x0020_Imagen' src='http://www.flexol.es/SGS-ISO_14001.jpg' alt='SGS-ISO 14001-COLOR.jpg'></span></i><i><span style='font-size:8.0pt;color:green'><o:p></o:p></span></i></p><p class=MsoNormal style='margin-top:3.0pt'><i><span style='font-size:8.0pt;color:green'>Antes de imprimir este mensaje, asegúrese de que sea imprescindible. Proteger el medio ambiente está a nuestro alcance.</span></i><i><span style='font-size:8.0pt;color:black'><br>En cumplimiento de la Ley Orgánica 15/1999, de 13 de diciembre de protección de datos de carácter personal, se pone en conocimiento del destinatario del presente correo electrónico, que los datos incluidos en este mensaje, están dirigidos exclusivamente al citado destinatario cuyo nombre aparece en el encabezamiento, por lo que si usted no es la persona interesada rogamos nos comunique el error de envío y se abstenga de realizar copias del mensaje o de los datos contenidos en el mismo o remitirlo o entregarlo a otra persona, procediendo a borrarlo de inmediato. Asimismo le informamos que sus datos de correo han quedado incluidos en nuestra base de datos a fin de dirigirle, por este medio, comunicaciones comerciales, profesionales e informativas y que usted dispone de los derechos de acceso, rectificación, cancelación y especificación de los mismos, derechos que podrá hacer efectivos dirigiéndose a ENTRECORTINAS S.L. con domicilio en Polígono Dos de Octubre, C/Bernard Vicent s/n, 18320 Santa Fe, Granada.<o:p></o:p></span></i></p><p class=MsoNormal><o:p>&nbsp;</o:p></p></div></body></html>";*/
