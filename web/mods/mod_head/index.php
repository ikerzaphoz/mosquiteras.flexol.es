<!DOCTYPE html>
<html lang="es">
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-8524490-16"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-8524490-16');
        </script>
        <meta name="robots" content="noindex, nofollow" />
        <meta charset="UTF-8">
        <title>Mosquiflex &#174;</title>
        <!--meta name="viewport" content="width=device-width,height=device-height initial-scale=1"-->
        <link rel="stylesheet" type="text/css" href="<?= path_css ?>bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?= path_css ?>normalize.css">
        <link rel="stylesheet" type="text/css" href="<?= path_css ?>font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?= path_css ?>toastr.min.css">
        <link rel="stylesheet" type="text/css" href="<?= path_css ?>summernote.css">
        <link rel="stylesheet" type="text/css" href="<?= path_css ?>magnific-popup.css">
        <link rel="stylesheet" type="text/css" href="<?= path_css ?>float-pattern.css">
        <link rel="stylesheet" type="text/css" href="<?= path_css ?>style.css">
        <script type="application/javascript" src="<?= path_js ?>jquery.js"></script>
    <!--    <script type="application/javascript" src="<?= path_js ?>less.min.js"></script>-->
        <link rel="stylesheet" type="text/css" href="<?= path_js ?>webform/webforms2.css">
        <script type="application/javascript" src="<?= path_js ?>webform/webforms2.js"></script>
        <!--<script src="https://www.paypalobjects.com/api/checkout.js"></script>-->

        <!--METADATOS-->
        <!--    <meta name="description" content="Mosquiflex es tu web para comprar mosquiteras." />-->
        <!--    <meta name="keywords" content="Mosquiflex, mosquiteras, enrollables, ventana, puerta, plisadas, fijas, correderas, ablatibles, comprar" />-->
    </head>
    <body>