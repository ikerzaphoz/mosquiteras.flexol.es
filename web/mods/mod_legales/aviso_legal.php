<div class="row nomargin">
    <div class="tabbable text-center mt-10">
        <ul class="nav col-md-12">
            <li class="nopadding col-md-5 btn-default bg-border-success active border-radius4"><a href="#tarifa_pvp" data-toggle="tab"><?=lang_title_tarifa_pvp?></a></li>
            <li class="nopadding col-md-5 btn-default bg-border-success border-radius4"><a href="#materia_prima" data-toggle="tab"><?=lang_title_tarifa_materia?></a></li>
        </ul>
        <div class="text-center col-md-12 col-xs-12 title_condiciones_general">
            <h4 class="text-center"><strong>CONDICIONES GENERALES DE VENTA</strong></h4>
        </div>
        <div class="tab-content col-md-12 padding-left-0 text-left mb-10">
        <div id="tarifa_pvp" class="tab-pane active content condiciones_legales col-md-12 col-xs-12">
           <div class="col-md-12 col-xs-12">
               <span class="title_condiciones">1. Los pedidos</span>
            </div>
            <div class="col-md-12 col-xs-12">
                <span>Deben cursarse por escrito (correo, fax, e-mail o web), con todos los datos necesarios para la fabricación del producto. En caso de ser cursados telefónicamente, será responsabilidad del cliente cualquier error que pudiera producirse.
                </span>
            </div>

            <div class="col-md-12 col-xs-12 mt-10">
                <span class="title_condiciones">2. Anulación y rectificación de pedidos</span>
            </div>
            <div class="col-md-12 col-xs-12">
                <span>Una vez cursado el pedido, se considera en firme. No se admitirá la rectificación o anulación de ningún pedido, salvo previa consulta 
                    confirmada por escrito. Los gastos derivados de la anulación o rectificación serán asumidos por el cliente.</span>
            </div>

            <div class="col-md-12 col-xs-12 mt-10">
                <span class="title_condiciones">3. Portes</span>
            </div>
            <div class="col-md-12 col-xs-12">
                <span>Pagados, siempre que el envío se realice por la agencia de transportes concertada por  <span class="condiciones_logo_mosquiflex"></span> . Las expediciones que contengan algún paquete de medida superior a 3 m. viajarán por transporte convencional y el tiempo de reparto será mayor.</span>
            </div>

            <div class="col-md-12 col-xs-12 mt-10">
                <span class="title_condiciones">4. Envío mínimo</span>
            </div>
            <div class="col-md-12 col-xs-12">
                <span>Cuando el valor de la mercancía que se envía es inferior a 50 € (antes de impuestos), se facturará la cantidad de 9 €  (+ IVA) en concepto de gastos de envío.</span>
            </div>

            <div class="col-md-12 col-xs-12 mt-10">
                <span class="title_condiciones">5. Impuestos</span>
            </div>
            <div class="col-md-12 col-xs-12">
                <span>
                    A cargo del comprador, de acuerdo con la legislación vigente.
                </span>
            </div>

            <div class="col-md-12 col-xs-12 mt-10">
                <span class="title_condiciones">6. A efectos de facturación</span>
            </div>
            <div class="col-md-12 col-xs-12">
                <span>
                    En todos los artículos que se facture por m2, las medidas se redondearán de 5 en 5 cm. al alza y el mínimo a facturar será de 1,50 m2/Ud.<br>
                    En todos los artículos que se facture por m. lineal, las medidas se redondearán de 5 en 5 cm. al alza y el mínimo a facturar será de 1 m. lineal.<br>
En todos los artículos que se facture por tabla, se redondearán siempre a la medida superior.
                </span>
            </div>

            <div class="col-md-12 col-xs-12 mt-10">
                <span class="title_condiciones">7. Pagos</span>
            </div>
            <div class="col-md-12 col-xs-12">
                <span>
                    Recibos domiciliados en una cuenta bancaria (SEPA B2B) o Reembolso a la Agencia de Transportes.
                </span>
            </div>

            <div class="col-md-12 col-xs-12 mt-10">
                <span class="title_condiciones">8. Reclamaciones</span>
            </div>
            <div class="col-md-12 col-xs-12">
                <span>No se admitirá ninguna reclamación por daños producidos en el transporte pasadas 48 horas de la fecha de recepción de la mercancía. La firma del albarán a la Agencia de Transportes implica su conformidad.<br>                 
En ningún caso se aceptarán reclamaciones por pequeñas variaciones de tono en el color o textura respecto a los muestrarios. En caso de defectos de fabricación o errores por parte de <span class="condiciones_logo_mosquiflex"></span> ésta se hará cargo exclusivamente de la reposición y/o reparación del producto, excluyendo específicamente gastos de desplazamiento o mano de obra.
                </span>
            </div>

            <div class="col-md-12 col-xs-12 mt-10">
                <span class="title_condiciones">9. Devoluciones</span>
            </div>
            <div class="col-md-12 col-xs-12">
                <span>No se admitirá ninguna devolución de material que no haya sido previamente acordada.</span>
            </div>

            <div class="col-md-12 col-xs-12 mt-10">
                <span class="title_condiciones">10. Producto</span>
            </div>
            <div class="col-md-12 col-xs-12">
                <span><span class="condiciones_logo_mosquiflex"></span>  se reserva el derecho de hacer cualquier cambio técnico en el producto, sin previo aviso.<br>
<span class="condiciones_logo_mosquiflex"></span>  garantiza sus productos contra defectos de fabricación durante 2 años.
Esta garantía no será efectiva en casos derivados de la instalación o del mal uso del producto, ni en aquellos casos en los que por indicación del cliente el producto se haya fabricado con medidas o versiones no aconsejables según la presente tarifa.<br>
<span class="condiciones_logo_mosquiflex"></span>  podrá rechazar cualquier pedido que por causas técnicas pueda presentar problemas de funcionamiento.</span> 
            </div>

            <div class="col-md-12 col-xs-12 mt-10">
                <span class="title_condiciones">11. Reparaciones</span>
            </div>
            <div class="col-md-12 col-xs-12">
                <span>El importe MÍNIMO a facturar por este concepto será de 50 € + IVA / mosquitera.</span>
            </div>
        </div>  
        <div id="materia_prima" class="tab-pane content condiciones_legales col-md-12 col-xs-12">
            <div class="col-md-12 col-xs-12 mt-10">
                <span class="title_condiciones">1. Pedidos</span>
            </div>
            <div class="col-md-12 col-xs-12">
                <span>Deben cursarse por escrito (correo, fax, mail o web), en caso de ser cursados telefónicamente será responsabilidad del cliente cualquier error que pudiera producirse.</span>
            </div>
            
            <div class="col-md-12 col-xs-12 mt-10">
                <span class="title_condiciones">2. Embalajes</span>
            </div>
            <div class="col-md-12 col-xs-12">
                <span>Los embalajes podrían variar por distintas causas, pero todos los precios de esta tarifa están contemplados para cajas completas.</span>
            </div>
            
            <div class="col-md-12 col-xs-12 mt-10">
                <span class="title_condiciones">3. Precios</span>
            </div>
            <div class="col-md-12 col-xs-12">
                <span>Los precios son NETOS. (IVA no incluido)</span>
                <span>ENTRECORTINAS S.L. se reserva el derecho de modificar esta tarifa en función de la oscilación en los precios del aluminio y/o por la subida del USD frente al EURO.</span>
            </div>
            
            <div class="col-md-12 col-xs-12 mt-10">
                <span class="title_condiciones">4. Portes</span>
            </div>
            <div class="col-md-12 col-xs-12">
                <span>DEBIDOS. para PEDIDOS inferiores a 3.000 €</span>
            </div>
            
            <div class="col-md-12 col-xs-12 mt-10">
                <span class="title_condiciones">5. Pagos</span>
            </div>
            <div class="col-md-12 col-xs-12">
                <span>Recibo domiciliado SEPA B2B o Transferencia.</span>
            </div>
            
            <div class="col-md-12 col-xs-12 mt-10">
                <span class="title_condiciones">6. Reclamaciones</span>
            </div>
            <div class="col-md-12 col-xs-12">
                <span>No se admitirá ninguna reclamación por daños producidos en el transporte pasadas 48 horas de la fecha de recepción de la mercancía. La firma del albarán a la Agencia de Transportes implica su conformidad. En ningún caso se aceptarán reclamaciones por pequeñas variaciones de tono en el color o textura respecto a los muestrarios.</span>
            </div>
            
            <div class="col-md-12 col-xs-12 mt-10">
                <span class="title_condiciones">7. Devoluciones</span>
            </div>
            <div class="col-md-12 col-xs-12">
                <span>No se admitirá ninguna devolución de material, que no haya sido previamente acordada.</span><br>
                <span>Todo pedido obliga al comprador a la aceptación de nuestras condiciones generales de venta.
</span>
            </div>
        </div>
        </div>
    </div>
    </div>