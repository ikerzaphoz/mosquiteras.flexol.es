<div class="row col-md-12 mt-15">
    <div class="col-md-12 text-center">
        <span class="title_page_manual">MANUALES DE INSTALACIÓN</span>
    </div>
    <div class="col-md-6">
        <div class="col-md-12 mt-15">
            <div class="col-md-3"><img src="<?= path_image ?>icons_products/2731.jpg"/></div>
            <div class="col-md-8 mt-10"><p class="title_manual">Enrollable Ventana</p></div>
        </div>
        <div class="col-md-12 mt-15">
            <div class="col-md-12 content_muestras">
                <div class="col-md-4 text-center manual_instrucciones">
                    <a title='enrollable ventana' target="_blank" href="<?= url_web ?>/uploads_files/manuales_instalacion/Manual_Enr._Ventana.pdf">Ver &nbsp;<i class="fa icono_instrucciones fa-book" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="col-md-12 mt-15">
            <div class="col-md-3"><img src="<?= path_image ?>icons_products/2732.jpg"/></div>
            <div class="col-md-8 mt-10"><p class="title_manual">Enrollable Puerta</p></div>
        </div>
        <div class="col-md-12 mt-15">
            <div class="col-md-12 content_muestras">
                <div class="col-md-6 nopadding">
                    <div class="nopadding col-md-12 text-center manual_instrucciones">
                        <a title='enrollable puerta única' target="_blank" href="<?= url_web ?>/uploads_files/manuales_instalacion/Manual_Enr_Puerta_Unica.pdf">Ver P.Única&nbsp;<i class="fa icono_instrucciones fa-book" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 content_muestras">
                <div class="col-md-6 mt-10 nopadding">
                    <div class="nopadding col-md-12 text-center manual_instrucciones">
                        <a title='enrollable puerta doble' target="_blank" href="<?= url_web ?>/uploads_files/manuales_instalacion/Manual_Enr_Puerta_Doble.pdf">Ver P.Doble&nbsp;<i class="fa icono_instrucciones fa-book" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">
    <hr class="linea_separar_manuales">
</div>
<div class="col-md-6">
    <div class="col-md-12 mt-15">
        <div class="col-md-3"><img src="<?= path_image ?>icons_products/2733.jpg"/></div>
        <div class="col-md-8 mt-10 text-bold"><p class="title_manual">Plisada de 22</p></div>
    </div>
</div>
<div class="col-md-12 mt-15">
    <div class="col-md-3 content_muestras">
        <div class="col-md-12">
            <p class="title_muestra">Puerta Lateral</p>
        </div>
        <div class="col-md-12">
            <div class="nopadding col-md-12 text-center manual_instrucciones">
                <a title='plisada 22 puerta lateral' target="_blank" href="<?= url_web ?>/uploads_files/manuales_instalacion/Manual_Plisada_22_Puerta_Lateral.pdf">Ver P.Única&nbsp;<i class="fa icono_instrucciones fa-book" aria-hidden="true"></i></a>
            </div>
            <div class="nopadding col-md-12 text-center manual_instrucciones mt-10">
                <a title='plisada 22 puerta doble' target="_blank" href="<?= url_web ?>/uploads_files/manuales_instalacion/Manual_Plisada_22_Puerta_Doble.pdf">Ver P.Doble&nbsp;<i class="fa icono_instrucciones fa-book" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
    <div class="col-md-3 content_muestras">
        <div class="col-md-12">
            <p class="title_muestra">Puerta Reversible</p>
        </div>
        <div class="col-md-12 text-center manual_instrucciones">
            <a title='plisada 22 puerta reversible' target="_blank" href="<?= url_web ?>/uploads_files/manuales_instalacion/Manual_Plisada_22_Puerta_Reversible.pdf">Ver &nbsp;<i class="fa icono_instrucciones fa-book" aria-hidden="true"></i></a>
        </div>
    </div>
    <div class="col-md-3 content_muestras">
        <div class="col-md-12">
            <p class="title_muestra">Versión Ventana</p>
        </div>
        <div class="col-md-12 text-center manual_instrucciones">
            <a title='plisada 22 ventana' target="_blank" href="<?= url_web ?>/uploads_files/manuales_instalacion/Manual_Plisada_22_Ventana.pdf">Ver &nbsp;<i class="fa icono_instrucciones fa-book" aria-hidden="true"></i></a>
        </div>
    </div>
</div>
<div class="col-md-12">
    <hr class="linea_separar_manuales">
</div>
<div class="col-md-6">
    <div class="col-md-12 mt-15">
        <div class="col-md-3"><img src="<?= path_image ?>icons_products/2736.jpg"/></div>
        <div class="col-md-8 mt-10"><p class="title_manual">Abatible Puerta</p></div>
    </div>
    <div class="col-md-12 mt-15">
        <div class="col-md-12 content_muestras">
            <div class="col-md-4 text-center manual_instrucciones">
                <a title='abatible puerta' target="_blank" href="<?= url_web ?>/uploads_files/manuales_instalacion/Manual_Abatible_Puerta_Unica_y_Doble.pdf">Ver &nbsp;<i class="fa icono_instrucciones fa-book" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="col-md-12 mt-15">
        <div class="col-md-3"><img src="<?= path_image ?>icons_products/2735.jpg"/></div>
        <div class="col-md-8 mt-10"><p class="title_manual">Fijas y Correderas</p></div>
    </div>
    <div class="col-md-12 mt-15">
        <div class="col-md-12 content_muestras">
            <div class="col-md-4 text-center manual_instrucciones">
                <a title='fija' target="_blank" href="<?= url_web ?>/uploads_files/manuales_instalacion/Manual_Fija_y_Corredera.pdf">Ver &nbsp;<i class="fa icono_instrucciones fa-book" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</div>