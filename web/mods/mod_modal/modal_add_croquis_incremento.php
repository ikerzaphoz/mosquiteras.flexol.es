<div id="modal_add_croquis_incremento" class="modal fade" role="dialog">
    <div class="modal-dialog w90">

        <div class="modal-content container">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Adjuntar croquis</h4>
            </div>
            <div class="modal-body">
                <div class="row form-group">
                    <form enctype="multipart/form-data" class="upload_croquis_increment">
                        <div class="col-xs-12 col-md-12">
                            <div class="col-md-8 col-xs-8">
                                <input name="file_croquis" value="" class="file_croquis" type="file"/>
                            </div>
                            <div class="col-xs-4 col-md-4">
                                <button type="submit" value="button_upload_croquis" name="button_upload_croquis"
                                        class="btn btn-default btn-info button_upload_croquis">Confirmar subida
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>