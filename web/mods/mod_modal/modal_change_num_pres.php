<div id="modal_change_num_pres" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modificar número presupuesto</h4>
            </div>
            <div class="modal-body row">
                
                <form class="form_change_num_pres">
                    <div class="form-group col-md-12">
                        <div class="col-md-4">
                            <label>Número presupuesto: </label>
                        </div>
                        <div class="col-md-8">
                        <input class="form-control" type="text" name="text_change_num_pres">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="boton_componentes_flotante_add text-center">
                    <input class="btn btn-default bg-border-success btn_save_num_pres" type="submit" value="Guardar número presupuesto">
                </div>
                <button type="button" data-dismiss="modal" class="btn btn-default"><?= lang_title_cerrar ?></button>
            </div>
        </div>

    </div>
</div>