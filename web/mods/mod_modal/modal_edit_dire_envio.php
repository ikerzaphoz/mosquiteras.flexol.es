<div id="modal_edit_dire_envio" class="modal fade" role="dialog">
    <div class="modal-dialog w90">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <?php if ($_SESSION['is_cliente_final'] == 1): ?>
                    <h4 class="modal-title">Seleccionar forma de pago</h4>
                <?php else: ?>
                    <h4 class="modal-title">Seleccionar dirección envio de la mercancia</h4>
                <?php endif; ?>
            </div>
            <div class="modal-body row">

            </div>
        </div>

    </div>
</div>