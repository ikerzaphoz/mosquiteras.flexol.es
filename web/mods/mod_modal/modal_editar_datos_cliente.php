<div id="modal_editar_datos_cliente" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modificar datos cliente</h4>
            </div>
            <div class="modal-body row">
                
                <form class="form_editar_datos_cliente">
                    <div class="form-group col-md-12">
                        <div class="col-md-4 col-xs-4">
                            Número presupuesto:
                        </div>
                        <div class="col-md-8 col-xs-8">
                        <input class="form-control text_change_num_pres" placeholder="Número de presupuesto personalizado" type="text" name="text_change_num_pres">
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <div class="col-md-4 col-xs-4">
                            Datos del cliente:
                        </div>
                        <div class="col-md-8 col-xs-8">
                        <textarea class="form-control editor_text" name="text_area_datos_cliente"></textarea>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <input class="btn btn-default bg-border-success" type="submit" value="MODIFICAR">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                
            </div>
        </div>

    </div>
</div>