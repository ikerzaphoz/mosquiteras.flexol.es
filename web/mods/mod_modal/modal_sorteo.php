<div id="modal_sorteo" class="modal fade mt-25" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog w90">
        <div class="modal-content container">
            <!-- Modal content-->
            <div class="modal-header">
                <strong style="font-size: 16px">SORTEO SEMANAL BICICLETA</strong>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body content_modal_sorteo">
                <div class="row col-md-12">
                    <div class="col-md-6 col-xs-6">
                        <img style="height: auto !important;width: 90%;" src="<?= path_image ?>bicicleta.png" width="120px" height="120px"/>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <p style="font-size: 18px">El ganador del SORTEO Nº14 es el albarán número 99.725 con destino HUESCA.</p>
                        <br><strong class="text-center" style="font-size: 20px">¡Enhorabuena!</strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>