<div id="modal_upload_file" class="modal fade" role="dialog">
    <div class="modal-dialog w75">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?=lang_title_modal_upload_file?></h4>
            </div>
            <div class="modal-body row">
                <form enctype="multipart/form-data" id="formuploadajax">
                    <div class="form-group col-md-12">

                            <input class="form-control-file" type="file" id="archivo1" name="archivo1"/>
                    </div>
                    <div class="form-group col-md-12">
                            <input class="btn btn-sm bg-border-success" type="submit" value="<?=lang_title_modal_upload_file?>"/>
                    </div>  
                    <div class="form-group col-md-12">
                        <p class="text-danger">*<?=lang_title_modal_upload_file_texto_info?>*</p>
                    </div>
                </form>
            </div>
        </div>  
    </div>
</div>