<style>
    .icon_muestra{
        width: 100%;
        height: auto;
    }
    .title_muestra{
        margin-top: 14px;
        font-weight: bold;
    }

    .imagen_muestra{
        /*width: 80%;
        height: auto;
        max-width: 230px !important;*/
        width: auto;
        height: auto;
        max-height: 200px;
    }

    .content_text_muestra{
        font-size: 12px;
        padding-right: 20px;
    }
    .subtitle_muestra{
        font-weight: bold;
        margin-top: 10px;
    }

    .title_fila_3{
        margin-top: 40px;
    }

    .border_muestras{
        border-top: 6px solid #37af04;
        border-radius: 4px;
    }

    .imagen_muestra_ultima{
        max-height: 320px;
    }

    .icono-lupa{
        display: none;
    }

    @media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
        .imagen_muestra{
            width: auto;
            height: auto;
            max-height: 350px;
        }
        .imagen_muestra_ultima{
            max-width: 500px;
            max-height: 327px;
        }

        .icono-lupa{
            display: none;
        }
    }
    @supports (-ms-accelerator:true) {
        .imagen_muestra{
            width: auto;
            height: auto;
            max-height: 350px;
        }
        .imagen_muestra_ultima{
            max-width: 500px;
            max-height: 327px;
        }

        .icono-lupa{
            display: none;
        }
    }

</style>
<!--[if IE]>
<style type="text/css">
.imagen_muestra{
        width: auto;
        height: auto;
        max-height: 350px;
    }
    .imagen_muestra_ultima{
        max-width: 500px;
        max-height: 327px;
    }
</style>
<![endif]-->
<div class="row col-md-12 mt-10">
    <div class="col-md-6 content_muestras mt-15">
        <div class="row content_title_muestra">
            <div class="col-md-2"><img class="icon_muestra" src="<?= path_image ?>icons_products/2731.jpg"/></div>
            <div class="col-md-10 border_muestras"><p class="title_muestra">Enrollables Ventana</p></div>
        </div>
        <div class="row contenido_muestra">
            <div class="content_text_muestra">
                <p class="subtitle_muestra">¡La más práctica!</p>

                <p class="text_muestra">Formada por un cajón y dos guías, se adapta perfectamente a la carpintería, quedando integrada en la misma. 

                    Disponible en dos modelos de cajón: 35mm. y 42mm.

                    El sistema de accionamiento a muelle conjuga la facilidad de maniobra y la fiabilidad del enrollamiento. 
                    Opcionalmente, dispone de un sistema de instalación frontal.</p>

            </div>
            <div class="text-center">
                <div class="div_imagen_muestra">
                    <img class="imagen_muestra" src="<?= path_image ?>images_product/muestras/1_1.png"/>
                </div>
                <div class="div_ampliar_imagen">
                    <a class="image-popup-no-margins icono-lupa fa fa-search" href="<?= path_image ?>images_product/muestras/1.png">
                        <img class="hidden" src="<?= path_image ?>images_product/muestras/1.png">
                        <span class="text_lupa">Ampliar imagen</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-6 padding-left-0">
            <div class="nopadding col-md-12 text-center hidden manual_instrucciones mt-10">
                <a title='enrollable ventana' target="_blank" href="<?= url_web ?>uploads_files/manuales_instalacion/Manual_Enr._Ventana.pdf">Manual&nbsp;<i class="fa icono_instrucciones fa-book" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
    <div class="col-md-6 content_muestras mt-15">
        <div class="row content_title_muestra">
            <div class="col-md-2"><img class="icon_muestra" src="<?= path_image ?>icons_products/2732.jpg"/></div>
            <div class="col-md-10 border_muestras"><p class="title_muestra">Enrollable Puerta</p></div>
        </div>
        <div class="row contenido_muestra">
            <div class="content_text_muestra">
                <p class="subtitle_muestra">¡La más moderna!</p>
                <p class="text_muestra">
                    La Plisada sin plisar. 
                    Dispone de un perfil fijo de aluminio en el cual se aloja un eje con la malla enrollada. Novedoso sistema para instalación en puertas de paso frecuente, con un carril inferior de reducidas dimensiones. Disponible en la versión “Puerta Doble” con anchos de hasta 2,80m.
                </p>


            </div>
            <div class="text-center">
                <div class="div_imagen_muestra">
                    <img class="imagen_muestra" src="<?= path_image ?>images_product/muestras/3_1.png"/>
                </div>
                <div class="div_ampliar_imagen">
                    <a class="image-popup-no-margins icono-lupa fa fa-search" href="<?= path_image ?>images_product/muestras/2.png">
                        <img class="hidden" src="<?= path_image ?>images_product/muestras/3.png">
                        <span class="text_lupa">Ampliar imagen</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-12 padding-left-0" style="margin-top: 17px;">
            <div class="nopadding col-md-6 text-center manual_instrucciones hidden mt-10" style="margin-right:10px; margin-left:-10px;">
                <a title='enrollable puerta única' target="_blank" href="<?= url_web ?>uploads_files/manuales_instalacion/Manual_Enr_Puerta_Unica.pdf">Manual P.Única&nbsp;<i class="fa icono_instrucciones fa-book" aria-hidden="true"></i></a>
            </div>
            <div class="nopadding col-md-6 text-center manual_instrucciones hidden mt-10">
                <a title='enrollable puerta doble' target="_blank" href="<?= url_web ?>uploads_files/manuales_instalacion/Manual_Enr_Puerta_Doble.pdf">Manual P.Doble&nbsp;<i class="fa icono_instrucciones fa-book" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</div>
<div class="row col-md-12">
    <div class="col-md-6 content_muestras mt-15">
        <div class="row content_title_muestra">
            <div class="col-md-2"><img class="icon_muestra" src="<?= path_image ?>icons_products/2736.jpg"/></div>
            <div class="col-md-10 border_muestras"><p class="title_muestra">Abatible Puerta</p></div>
        </div>
        <div class="row contenido_muestra">
            <div class="content_text_muestra">
                <p class="subtitle_muestra">¡La más sólida!</p>
                <p class="text_muestra">
                    Estructura de marco con puerta de cierre automático. La solidez de su diseño permite el montaje conjunto de dos hojas en un mismo marco. Los perfiles se encuentran unidos mediante ingletes con escuadras metálicas ocultas confiriendo a la puerta una gran resistencia. La utilización de perfiles compensadores, tanto en los laterales como en la parte superior, garantizan una perfecta instalación de la puerta. 
                </p>


            </div>
            <div class="text-center">
                <img class="imagen_muestra" src="<?= path_image ?>images_product/muestras/2_1.png"/>
                <br><a class="image-popup-no-margins icono-lupa fa fa-search" href="<?= path_image ?>images_product/muestras/3.png">
                    <img class="hidden" src="<?= path_image ?>images_product/muestras/2.png">
                    <span class="text_lupa">Ampliar imagen</span>
                </a>
            </div>
        </div>
        <div class="col-md-6 padding-left-0">
            <div class="nopadding col-md-12 text-center manual_instrucciones hidden mt-10">
                <a title='abatible puerta' target="_blank" href="<?= url_web ?>uploads_files/manuales_instalacion/Manual_Abatible_Puerta_Unica_y_Doble.pdf">Manual&nbsp;<i class="fa icono_instrucciones fa-book" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
    <div class="col-md-6 content_muestras mt-15">
        <div class="row content_title_muestra">
            <div class="col-md-2"><img class="icon_muestra" src="<?= path_image ?>icons_products/2733.jpg"/></div>
            <div class="col-md-10 border_muestras"><p class="title_muestra">Plisada de 40 Puerta</p></div>
        </div>
        <div class="row contenido_muestra">
            <div class="content_text_muestra">
                <p class="subtitle_muestra">¡La más estilizada!</p>
                <p class="text_muestra">
                    Diseño innovador y fortaleza para cubrir grandes dimensiones con una excelente resistencia. Formada por un cabezal de reducidas dimensiones que incorpora un sistema “multipunto” de apertura, que le permite anchos de hasta 3,50 m. en un solo paño. Dispone de un carril inferior de reducidas dimensiones, quedando la zona de paso libre de obstáculos, así como un carril superior que permite compensar descuadres de hasta 2 cm. en el alto.
                </p>
            </div>
            <div class="text-center">
                <img class="imagen_muestra" src="<?= path_image ?>images_product/muestras/4_1.png"/>
                <br><a class="image-popup-no-margins icono-lupa fa fa-search" href="<?= path_image ?>images_product/muestras/4.png">
                    <img class="hidden" src="<?= path_image ?>images_product/muestras/4.png">
                    <span class="text_lupa">Ampliar imagen</span>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="row col-md-12 mt-15">
    <div class="row col-md-12">
        <div class="col-md-1"><img src="<?= path_image ?>icons_products/2733.jpg"/></div>
        <div class="col-md-11 border_muestras"><p class="title_muestra">Plisada de 22</p></div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-6">
            <p class="subtitle_muestra">¡La más versátil!</p>
            <p class="text_muestra">Disponible en 3 versiones:</p>
        </div>
    </div>
    <div class="col-md-4 content_muestras">
        <div class="row">
            <div class="nopadding col-md-5">
                <p class="title_muestra">Puerta Lateral:</p>
            </div>
        </div>
        <div class="row contenido_muestra">
            <div class="content_text_muestra">
                <p class="text_muestra">
                    La malla queda recogida en uno de los laterales desplegándose al contrario para su cierre.
                </p>
            </div>
            <div class="text-center">
                <img class="imagen_muestra" src="<?= path_image ?>images_product/muestras/5_1.png"/>
                <br><a class="image-popup-no-margins icono-lupa fa fa-search" href="<?= path_image ?>images_product/muestras/5.png">
                    <img class="hidden" src="<?= path_image ?>images_product/muestras/5.png">
                    <span class="text_lupa">Ampliar imagen</span>
                </a>            </div>
        </div>
        <div class="col-md-12 padding-left-0" style="margin-top: 17px;">
            <div class="nopadding col-md-6 text-center manual_instrucciones hidden mt-10" style="margin-right:10px; margin-left:-10px;">
                <a title='plisada 22 puerta lateral' target="_blank" href="<?= url_web ?>uploads_files/manuales_instalacion/Manual_Plisada_22_Puerta_Lateral.pdf">Manual P.Única&nbsp;<i class="fa icono_instrucciones fa-book" aria-hidden="true"></i></a>
            </div>
            <div class="nopadding col-md-6 text-center manual_instrucciones hidden mt-10">
                <a title='plisada 22 puerta doble' target="_blank" href="<?= url_web ?>uploads_files/manuales_instalacion/Manual_Plisada_22_Puerta_Doble.pdf">Manual P.Doble&nbsp;<i class="fa icono_instrucciones fa-book" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
    <div class="col-md-4 content_muestras">
        <div class="row">
            <div class="nopadding col-md-8">
                <p class="title_muestra">Puerta Reversible:</p>
            </div>
        </div>
        <div class="row contenido_muestra">
            <div class="content_text_muestra">
                <p class="text_muestra">
                    La malla queda replegada entre dos perfiles móviles, lo que permite deslizar todo hacía un lado u otro e incluso al centro.
                </p>
            </div>
            <div class="text-center">
                <img class="imagen_muestra" src="<?= path_image ?>images_product/muestras/6_1.png"/>
                <br><a class="image-popup-no-margins icono-lupa fa fa-search" href="<?= path_image ?>images_product/muestras/5.png">
                    <img class="hidden" src="<?= path_image ?>images_product/muestras/6.png">
                    <span class="text_lupa">Ampliar imagen</span>
                </a>  
            </div>
        </div>
        <div class="col-md-8 padding-left-0">
            <div class="nopadding col-md-12 text-center manual_instrucciones hidden mt-10">
                <a title='plisada 22 puerta reversible' target="_blank" href="<?= url_web ?>uploads_files/manuales_instalacion/Manual_Plisada_22_Puerta_Reversible.pdf">Manual&nbsp;<i class="fa icono_instrucciones fa-book" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
    <div class="col-md-4 content_muestras">
        <div class="row">
            <div class="nopadding col-md-8">
                <p class="title_muestra">Versión Ventana:</p>
            </div>
        </div>
        <div class="row contenido_muestra">
            <div class="content_text_muestra">
                <p class="text_muestra">
                    El pliegue del tejido cambia su posición de vertical a horizontal, su accionamiento manual es suave y permite dejarla abierta en cualquier posición. 
                </p>
            </div>
            <div class="text-center">
                <img class="imagen_muestra" src="<?= path_image ?>images_product/muestras/7_1.png"/>
                <br><a class="image-popup-no-margins icono-lupa fa fa-search" href="<?= path_image ?>images_product/muestras/7.png">
                    <img class="hidden" src="<?= path_image ?>images_product/muestras/7.png">
                    <span class="text_lupa">Ampliar imagen</span>
                </a>
            </div>
        </div>
        <div class="col-md-8 padding-left-0">
            <div class="nopadding col-md-12 text-center manual_instrucciones hidden mt-10">
                <a title='plisada 22 ventana' target="_blank" href="<?= url_web ?>uploads_files/manuales_instalacion/Manual_Plisada_22_Ventana.pdf">Manual&nbsp;<i class="fa icono_instrucciones fa-book" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</div>

<div class="row col-md-12 mt-15">
    <div class="row col-md-12">
        <div class="col-md-1"><img src="<?= path_image ?>icons_products/2734.jpg"/></div>
        <div class="col-md-11 border_muestras"><p class="title_muestra">Fijas y Correderas</p></div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-6">
            <p class="subtitle_muestra">¡Las más sencillas!</p>
            <p class="text_muestra">Disponible en 3 versiones:</p>
        </div>
    </div>
    <div class="col-md-4 content_muestras">
        <div class="row">
            <p class="title_muestra">Fija:</p>
        </div>
        <div class="row contenido_muestra">
            <div class="content_text_muestra">
                <p class="text_muestra">
                    Ideal para cubrir pequeños huecos. Muy resistente, formada por un perfil de aluminio cortado a inglete y unido mediante escuadras ocultas.
                </p>
            </div>
            <div class="text-center">
                <img class="imagen_muestra imagen_muestra_ultima" src="<?= path_image ?>images_product/muestras/8_3.png"/>
                <br><a class="image-popup-no-margins icono-lupa fa fa-search" href="<?= path_image ?>images_product/muestras/8_big.png">
                    <img class="hidden" src="<?= path_image ?>images_product/muestras/8_big.png">
                    <span class="text_lupa">Ampliar imagen</span>
                </a>
            </div>
        </div>
        <div class="col-md-8 padding-left-0">
            <div class="nopadding col-md-12 text-center manual_instrucciones hidden mt-10">
                <a title='fija' target="_blank" href="<?= url_web ?>uploads_files/manuales_instalacion/Manual_Fija_y_Corredera.pdf">Manual&nbsp;<i class="fa icono_instrucciones fa-book" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
    <div class="col-md-4 content_muestras">
        <div class="row">
            <p class="title_muestra">Corredera sin marco:</p>
        </div>
        <div class="row contenido_muestra">
            <div class="content_text_muestra">
                <p class="text_muestra">
                    Está diseñada para deslizarse sobre la propia guía de la ventana de aluminio. Su función de corredera le permite deslizar las dos hojas de cristal hacia un lado y la mosquitera al contrario, consiguiendo ventilación en la estancia.
                </p>
            </div>
            <div class="text-center">
                <img class="imagen_muestra imagen_muestra_ultima" src="<?= path_image ?>images_product/muestras/9_1.png"/>
                <br><a class="image-popup-no-margins icono-lupa fa fa-search" href="<?= path_image ?>images_product/muestras/9.png">
                    <img class="hidden" src="<?= path_image ?>images_product/muestras/9.png">
                    <span class="text_lupa">Ampliar imagen</span>
                </a>
            </div>
        </div>
        <div class="col-md-8 padding-left-0">
            <div class="nopadding col-md-12 text-center manual_instrucciones hidden mt-10">
                <a title='corredera sin marco' target="_blank" href="<?= url_web ?>uploads_files/manuales_instalacion/Manual_Fija_y_Corredera.pdf">Manual&nbsp;<i class="fa icono_instrucciones fa-book" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
    <div class="col-md-4 content_muestras">
        <div class="row">
            <p class="title_muestra">Corredera con marco:</p>
        </div>
        <div class="row contenido_muestra">
            <div class="content_text_muestra">
                <p class="text_muestra">
                    Es la misma corredera provista de un marco de aluminio, para su instalación en hueco.
                </p>
            </div>
            <div class="text-center">
                <img class="imagen_muestra imagen_muestra_ultima" src="<?= path_image ?>images_product/muestras/10_4.png"/>
                <br><a class="image-popup-no-margins icono-lupa fa fa-search" href="<?= path_image ?>images_product/muestras/10_big.png">
                    <img class="hidden" src="<?= path_image ?>images_product/muestras/10_big.png">
                    <span class="text_lupa">Ampliar imagen</span>
                </a>
            </div>
        </div>
        <div class="col-md-8 padding-left-0">
            <div class="nopadding col-md-12 text-center manual_instrucciones hidden mt-10">
                <a title='corredera con marco' target="_blank" href="<?= url_web ?>uploads_files/manuales_instalacion/Manual_Fija_y_Corredera.pdf">Manual&nbsp;<i class="fa icono_instrucciones fa-book" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</div>