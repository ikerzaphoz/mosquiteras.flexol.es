<?php
if (!isset($_SESSION)):
    session_start();
endif;
$familia = new Familia();
$array_familia = $familia->getFamilia();

$class_hidden_cart = "hidden";
if (!empty($_SESSION['products'])):
    $class_hidden_cart = "";
endif;
?>

<div class="container">
    <header id="header">
        <div class="row col-md-12 col-xs-12 text-right">
            <div class="col-md-12 col-xs-12">
                <span class="button_close_session"><a href="<?= path_web_mods ?>/mod_customers/action/log_out.php"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;<?= lang_nav_cerrar_sesion ?> (<?= utf8_encode($_SESSION['name_log']) ?></a></span>
            </div>
        </div>
        <div class="row col-md-12 col-xs-12">
            <div class="col-md-6 col-xs-6 text-left">
                <a href="index.php"><img alt="logo" id="img_header" src="<?= path_image ?>logo_mosquiflex.png"></a>
            </div>
            <div class="col-md-6 col-xs-6 text-right">
                <a target="_blank" href="<?= path_web_mods ?>mod_modal/help/pdf/indexV3.pdf"><button class="btn button-modal-help modal_help_pdf" aria-hidden="true"><i class="icono_ayuda fa fa-question-circle"></i>&nbsp;<?= strtoupper(lang_nav_manual_ayuda) ?></button></a>
            </div>
            <div class="col-xs-6 col-md-6 text-right">
                <?php if ($page != "clientes"): ?>
                    <div id="div_search" class="hidden">
                        <form class="search" name="search">
                            <div class="form-group has-feedback">
                                <input type="search" name="input_search" class="input_search form-control"
                                       placeholder="<?= lang_search_text ?>"/>
                                <i class="glyphicon glyphicon-search form-control-feedback"></i>
                            </div>
                        </form>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <nav id="nav_menu">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">

                <ul class="nav navbar-nav nav-menu-button">

                    <div class="hidden dropdown">
                        <a href="<?= path_web ?>productos.php?all=all">
                            <button
                                class="btn_menu dropbtn <?php if ($page == 'productos'): echo ' active';
                endif;
                ?>"><?= lang_menu_productos ?></button>
                        </a>
                        <div class="dropdown-menu dropdown-content">
                            <?php foreach ($array_familia as $item): ?>
                                <a href="<?= path_web ?>productos.php?id_pro=<?= $item['fafami'] ?>"><?= $item['fadesc'] ?></a>
<?php endforeach; ?>
                            <a href="<?= path_web ?>productos.php?exp=exp"><?= lang_menu_expositores ?></button></a>
                        </div>
                    </div>
                    <div class="dropdown">
                        <button path="<?= path_web ?>clientes.php?opt=ficha"
                                class="btn_modal_ini_cli btn_menu dropbtn <?php if ($page == 'clientes'): echo ' active';
endif;
?>"><?= lang_menu_clientes ?></button>
                        <div class="dropdown-menu dropdown-content">
                            <a path="<?= path_web ?>productos.php?all=all" class="btn_modal_ini_cli_ref"
                               href="<?= path_web ?>productos.php?all=all"><?= lang_menu_presupuestos_pedidos ?></a>
                            <a path="<?= path_web ?>clientes.php?opt=historial" class="btn_modal_ini_cli_ref"
                               href="<?= path_web ?>clientes.php?opt=historial"><?= lang_menu_historial ?></a>
                            <a target="_blank" path="http://www.flexol.es/cliente/catalogo/" class="btn_modal_ini_cli_ref" href="http://www.flexol.es/cliente/catalogo/"><?= lang_menu_catalogos ?></a>
                            <a target="_blank" path="<?= path_web ?>condiciones_legales.php?page=aviso_legal" class="btn_modal_ini_cli_ref" href="<?= path_web ?>condiciones_legales.php?page=aviso_legal"><?= lang_menu_condiciones ?></a>
                            <a path="<?= path_web ?>manuales.php" class="btn_modal_ini_cli_ref" href="<?= path_web ?>manuales.php">Manuales de instalación</a>
                            <a path="<?= path_web ?>clientes.php?opt=ficha" class="btn_modal_ini_cli_ref"
                               href="<?= path_web ?>clientes.php?opt=ficha"><?= lang_menu_mis_datos ?></a>
                        </div>
                    </div>
                    <div class="dropdown">
                        <a href="<?= path_web ?>muestras.php">
                            <button
                                class="btn_menu dropbtn <?php if ($page == 'productos'): echo ' active';
endif;
?>"><?= lang_menu_productos ?></button>
                        </a>
                    </div>
                </ul>
            </div>
        </nav>
    </header>