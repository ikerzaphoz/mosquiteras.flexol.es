<?php

if (empty($_SESSION)):
    session_start();
endif;

$presupuesto = new Presupuesto();
$cliente = new Customers();
$familia = new Familia();

$id_pres = "";
if (isset($_GET['sol_mat']))
    $id_pres = $_GET['sol_mat'];

$datos_presupuesto = $presupuesto->get_presupuesto($id_pres);
$cabecera_pvp = $presupuesto->get_cabecera_personalizada($id_pres);
$datos_cabecera_cliente = "";

$is_pedido = $datos_presupuesto[0]['is_pedido'];
$class_hidden_pedido = "hidden";
if ($is_pedido == 1):
    $class_hidden_pedido = "hidden";
    $num_presupuesto = hideNumPed($datos_presupuesto[0]['num_pre']);
else:
    $num_presupuesto = hideNumPre($datos_presupuesto[0]['num_pre']);
    if (!empty($cabecera_pvp['num_pre_pers'])):
        $num_presupuesto = $cabecera_pvp['num_pre_pers'];
        $datos_cabecera_cliente = $cabecera_pvp['texto_cabecera'];
        $datos_cabecera_cliente = str_replace("<p>", "", $datos_cabecera_cliente);
        $datos_cabecera_cliente = str_replace("</p>", "<br>", $datos_cabecera_cliente);
    endif;
endif;
$datos_cliente = $cliente->get_cliente($_SESSION['clproc'], $_SESSION['clcodi']);
$nombre_cliente = $datos_cliente[0]['clnomb'];
$fecha = $datos_presupuesto[0]['fecha'];
$fecha = date("d-m-Y", strtotime($fecha));
$total_presupuesto = $datos_presupuesto[0]['total'];
$tipo_iva = $datos_cliente[0]['cltipi'];
$datos_iva = $presupuesto->getIva($tipo_iva);
$iva = "";
if ($tipo_iva != "E"):
    $iva = $datos_iva['iva'];
    $iva = str_replace(".", "", $iva);
    $iva = "0." . $iva;
endif;

$recargo = "0.00";
$texto_recargo = "0,00";
$tipo_recargo = $datos_cliente[0]['cltipr'];
if ($tipo_recargo == "S"):
    $recargo = $datos_iva['rec_eq'];
    $texto_recargo = $recargo;
    $texto_recargo = str_replace(",", "", $texto_recargo);
    $texto_recargo = formato_dinero($texto_recargo);
    $recargo = str_replace(".", "", $recargo);
    $recargo = "0.0" . $recargo;
endif;

$forma_pago = $datos_cliente['0']['clforp'];
$datos_forma_pago = $presupuesto->getFormaPago($forma_pago);
$texto_forma_pago = $datos_forma_pago['fpdsfp'];

$aplazamiento = $datos_cliente['0']['clncfr'];
$datos_aplazamiento = $presupuesto->getAplazamiento($aplazamiento);
$texto_aplazamiento = $datos_aplazamiento['APDENC'];

$fecha_validez = "";
$fecha_validez = strtotime('+1 month', strtotime($fecha));
$fecha_validez = date('d.m.Y', $fecha_validez);

$dire_aux = "";
if ($datos_presupuesto[0]['dire_envio'] == "fiscal"):
    $dire_aux = $cliente->get_direccion_envio_pdf($_SESSION['clproc'], $_SESSION['clcodi']);
    if (!empty($datos_cliente[0]['cldire']) != 0):
        $dire_aux = $cliente->get_direccion_envio_is_fija($_SESSION['clproc'], $_SESSION['clcodi'], $datos_cliente[0]['cldire']);
    endif;
elseif ($datos_presupuesto[0]['dire_envio'] == "0"):
    $dire_aux = $cliente->get_direccion_recogida();
else:
    //$dire_aux = $cliente->get_direccion_envio_id_pdf($datos_presupuesto[0]['dire_envio']);
    $dire_aux = $cliente->get_direccion_envio_is_fija($datos_presupuesto[0]['cod_proc'], $datos_presupuesto[0]['cod_clie'], $datos_presupuesto[0]['dire_envio']);
//MODIFICADO 25/04/18
    if (!empty($datos_cliente[0]['cldire']) != 0):
        $dire_aux = $cliente->get_direccion_envio_is_fija($_SESSION['clproc'], $_SESSION['clcodi'], $datos_presupuesto[0]['dire_envio']);
    endif;
    if (!empty($datos_cliente[0]['cldire']) != 0 && empty($dire_aux)):
        $dire_aux = $cliente->get_direccion_envio_id_pdf($datos_presupuesto[0]['dire_envio']);
    endif;
endif;

$dire = $dire_aux[0];

//if ($datos_presupuesto[0]['dire_envio'] == 'fiscal' || $datos_presupuesto[0]['dire_envio'] == '0'&& !empty($dire['cldire'])):
//    $dire = $cliente->get_direccion_envio_is_fija($_SESSION['clproc'], $_SESSION['clcodi'], $dire['cldire']);
//    $dire = $dire[0];
//endif;

$dire_fiscal = $cliente->get_direccion_envio($_SESSION['clproc'], $_SESSION['clcodi'])[0];
$pais_fiscal = $cliente->getPais($dire_fiscal['clpais']);
$provincia_fiscal = $cliente->getProvincia($dire_fiscal['clpais'], $dire_fiscal['clprov']);
$poblacion_fiscal = $cliente->getPoblacion($dire_fiscal['clpais'], $dire_fiscal['clprov'], $dire_fiscal['clpobl']);


$pais = $cliente->getPais($dire['clpais']);
$provincia = $cliente->getProvincia($dire['clpais'], $dire['clprov']);
$poblacion = $cliente->getPoblacion($dire['clpais'], $dire['clprov'], $dire['clpobl']);

$agencia = $datos_presupuesto[0]['agencia'];

if($agencia == "rsa" || $agencia == "rec"):
    $agencia = $datos_presupuesto[0]['agencia'];
    $agencia = strtoupper($agencia);
else:
    $agencia = $datos_cliente[0]['claget'];
endif;

$albaranes = $presupuesto->get_albaranes($id_pres);

$is_almacen = $presupuesto->is_almacen($id_pres);

if (isset($_GET['order'])):
    $albaranes = ordernar_array_albaranes_impresion($albaranes);
endif;

$total_presupuesto = 0.00;

$num_lineas_presupuesto = 0;
$saltos_pagina = 1;

$total_conceptos = $presupuesto->count_concepto($id_pres);
$total_albaranes = $presupuesto->count_albaranes($id_pres);
$total_lineas = $total_conceptos + $total_albaranes;
$consciente = $total_lineas / 32;
$total_page = ceil($consciente);

$array_conceptos_pedidos = $presupuesto->get_conceptos_pedido($id_pres);

$pie_presupuesto = "";
$pie_presupuesto = $cliente->get_pie_presupuesto($_SESSION['clproc'], $_SESSION['clcodi']);

$referencia = "";
if (isset($datos_presupuesto[0]['referencia'])):
    $referencia = $datos_presupuesto[0]['referencia'];
endif;

$forma_pago = $datos_cliente['0']['clforp'];
$datos_forma_pago = $presupuesto->getFormaPago($forma_pago);
$texto_forma_pago = $datos_forma_pago['fpdsfp'];

$aplazamiento = $datos_cliente['0']['clncfr'];
$datos_aplazamiento = $presupuesto->getAplazamiento($aplazamiento);
$texto_aplazamiento = $datos_aplazamiento['APDENC'];
