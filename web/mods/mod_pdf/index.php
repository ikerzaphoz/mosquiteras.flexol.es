<?php

require '../../../config.php';

if (!isset($_SESSION)):
    session_start();
endif;

check_login();

ini_set("display_errors", 0);

$check_id_pres = "";
if(isset($_GET['pres'])):
        $check_id_pres = $_GET['pres'];
    else:
        $check_id_pres = $_GET['pres_cond'];
endif;

if(isset($_GET['sol_mat'])):
    $check_id_pres = $_GET['sol_mat'];
endif;

if(!empty($check_id_pres)):
    $presupuesto = new Presupuesto();
    $afectadas = $presupuesto->comprobar_cliente_presupuesto($check_id_pres, $_SESSION['clprov'], $_SESSION['clcodi']);
    if($_SESSION['usu_admin'] == "clie"){
        $afectadas = 1;
    }
    if($afectadas != 1):
        header("Location:http://mosquiteras.flexol.es/web/clientes.php?opt=historial");
    endif;
endif;

require 'html2pdf/html2pdf.class.php';
$is_pedido_check = $presupuesto->obtener_is_pedido($check_id_pres);
if(isset($_GET['pres']) && $is_pedido_check == '1'):
    require 'pdf_pedidos_valorados.php';
    exit;
endif;

if(isset($_GET['pres'])):
        
    require 'pdf_pedido.php';
        
endif;

if(isset($_GET['pres_cond'])):

    require 'pdf_pedido_cond.php';

endif;

if(isset($_GET['sol_mat'])):
    require 'pdf_solicitud_material.php';
endif;

?>