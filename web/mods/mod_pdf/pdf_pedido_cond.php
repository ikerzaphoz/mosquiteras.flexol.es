<?php

ob_start();
if ($_SESSION['usu_admin'] == "clie") {
    include(dirname(__FILE__) . '/datos_admin/pdf_pedido_cond.php');
} else {
    include(dirname(__FILE__) . '/datos/pdf_pedido_cond.php');
}
$filename = $datos_presupuesto[0]['num_pre'] . '_cond.pdf';
$filename = str_replace(" ", "-", $filename);
$filename = str_replace("%20", "-", $filename);
require_once("dompdf-master/autoload.inc.php");
ob_start();

use Dompdf\Dompdf;

$dompdf = new DOMPDF();
if ($_SESSION['pago_anticipado'] == "1"):
    include(dirname(__FILE__) . '/vistas/pdf_pedido_pago_anticipado.php');
else:
    if ($_SESSION['usu_admin'] == "clie") {
        include(dirname(__FILE__) . '/vistas_admin/pdf_pedido_cond.php');
    } else {
        include(dirname(__FILE__) . '/vistas/pdf_pedido_cond.php');
    }

endif;
$dompdf->load_html(ob_get_clean());
$dompdf->setPaper('A4');
$dompdf->render();
$dompdf->stream($filename, array("Attachment" => false));
?>