<?php
ob_start();
if($_SESSION['usu_admin'] == "clie"){
   include(dirname(__FILE__).'/datos_admin/pdf_solicitud_material.php'); 
}else{
    include(dirname(__FILE__).'/datos/pdf_solicitud_material.php');
}
$filename = $datos_presupuesto[0]['num_pre'].'.pdf';
$filename = str_replace(" ", "-", $filename);
$filename = str_replace("%20", "-", $filename);
require_once("dompdf-master/autoload.inc.php");
ob_start();
use Dompdf\Dompdf;
$dompdf = new DOMPDF();
if($_SESSION['usu_admin'] == "clie"){
    include(dirname(__FILE__).'/vistas_admin/pdf_solicitud_material.php');
}else{
    include(dirname(__FILE__).'/vistas/pdf_solicitud_material.php');
}
$dompdf->load_html(ob_get_clean());
$dompdf->setPaper('A4');
$dompdf->render();
$dompdf->stream($filename, array("Attachment" => false));
?>