<?php
ini_set("display_errors", 0);
$presupuesto = new Presupuesto();
$cliente = new Customers();
$familia = new Familia();

$datos_presupuesto = $presupuesto->get_presupuesto($id_pres);
$cabecera_pvp = $presupuesto->get_cabecera_personalizada($id_pres);
$datos_cabecera_cliente = "";

$is_pedido = $datos_presupuesto[0]['is_pedido'];
$class_hidden_pedido = "hidden";
if ($is_pedido == 1):
    $class_hidden_pedido = "hidden";
    $num_presupuesto = hideNumPed($datos_presupuesto[0]['num_pre']);
else:
    $num_presupuesto = hideNumPre($datos_presupuesto[0]['num_pre']);
    if (!empty($cabecera_pvp['num_pre_pers'])):
        $num_presupuesto = $cabecera_pvp['num_pre_pers'];
        $datos_cabecera_cliente = $cabecera_pvp['texto_cabecera'];
        $datos_cabecera_cliente = str_replace("<p>", "", $datos_cabecera_cliente);
        $datos_cabecera_cliente = str_replace("</p>", "<br>", $datos_cabecera_cliente);
    endif;
endif;
$datos_cliente = $cliente->get_cliente($_SESSION['clproc'], $_SESSION['clcodi']);
$nombre_cliente = $datos_cliente[0]['clnomb'];
$fecha = $datos_presupuesto[0]['fecha'];
$fecha = date("d-m-Y", strtotime($fecha));
$total_presupuesto = $datos_presupuesto[0]['total'];
$tipo_iva = $datos_cliente[0]['cltipi'];
$datos_iva = $presupuesto->getIva($tipo_iva);
$iva = "";
if ($tipo_iva != "E"):
    $iva = $datos_iva['iva'];
    $iva = str_replace(".", "", $iva);
    $iva = "0." . $iva;
endif;

$recargo = "0.00";
$texto_recargo = "0,00";
$tipo_recargo = $datos_cliente[0]['cltipr'];
if ($tipo_recargo == "S"):
    $recargo = $datos_iva['rec_eq'];
    $texto_recargo = $recargo;
    $texto_recargo = str_replace(",", "", $texto_recargo);
    $texto_recargo = formato_dinero($texto_recargo);
    $recargo = str_replace(".", "", $recargo);
    $recargo = "0.0" . $recargo;
endif;

$forma_pago = $datos_cliente['0']['clforp'];
$datos_forma_pago = $presupuesto->getFormaPago($forma_pago);
$texto_forma_pago = $datos_forma_pago['fpdsfp'];

$aplazamiento = $datos_cliente['0']['clncfr'];
$datos_aplazamiento = $presupuesto->getAplazamiento($aplazamiento);
$texto_aplazamiento = $datos_aplazamiento['APDENC'];

$fecha_validez = "";
$fecha_validez = strtotime('+1 month', strtotime($fecha));
$fecha_validez = date('d.m.Y', $fecha_validez);

$dire_aux = "";
if ($datos_presupuesto[0]['dire_envio'] == "fiscal"):
    $dire_aux = $cliente->get_direccion_envio_pdf($_SESSION['clproc'], $_SESSION['clcodi']);
    if (!empty($datos_cliente[0]['cldire']) != 0):
        $dire_aux = $cliente->get_direccion_envio_is_fija($_SESSION['clproc'], $_SESSION['clcodi'], $datos_cliente[0]['cldire']);
    endif;
elseif ($datos_presupuesto[0]['dire_envio'] == "0"):
    $dire_aux = $cliente->get_direccion_recogida();
else:
    $dire_aux = $cliente->get_direccion_envio_id_pdf($datos_presupuesto[0]['dire_envio']);
//MODIFICADO 25/04/18
    if (!empty($datos_cliente[0]['cldire']) != 0):
        $dire_aux = $cliente->get_direccion_envio_is_fija($_SESSION['clproc'], $_SESSION['clcodi'], $datos_presupuesto[0]['dire_envio']);
    endif;
endif;

$dire = $dire_aux[0];

//if ($datos_presupuesto[0]['dire_envio'] == 'fiscal' || $datos_presupuesto[0]['dire_envio'] == '0'&& !empty($dire['cldire'])):
//    $dire = $cliente->get_direccion_envio_is_fija($_SESSION['clproc'], $_SESSION['clcodi'], $dire['cldire']);
//    $dire = $dire[0];
//endif;

$dire_fiscal = $cliente->get_direccion_envio($_SESSION['clproc'], $_SESSION['clcodi'])[0];
$pais_fiscal = $cliente->getPais($dire_fiscal['clpais']);
$provincia_fiscal = $cliente->getProvincia($dire_fiscal['clpais'], $dire_fiscal['clprov']);
$poblacion_fiscal = $cliente->getPoblacion($dire_fiscal['clpais'], $dire_fiscal['clprov'], $dire_fiscal['clpobl']);


$pais = $cliente->getPais($dire['clpais']);
$provincia = $cliente->getProvincia($dire['clpais'], $dire['clprov']);
$poblacion = $cliente->getPoblacion($dire['clpais'], $dire['clprov'], $dire['clpobl']);

$agencia = $datos_presupuesto[0]['agencia'];
if ($agencia != "rsa" OR $agencia != "rec"):
    $agencia = $datos_cliente[0]['claget'];
else:
    $agencia = $datos_presupuesto[0]['agencia'];
endif;

$albaranes = $presupuesto->get_albaranes($id_pres);

$is_almacen = $presupuesto->is_almacen($id_pres);

if (isset($_GET['order'])):
    $albaranes = ordernar_array_albaranes_impresion($albaranes);
endif;

$total_presupuesto = 0.00;

$num_lineas_presupuesto = 0;
$saltos_pagina = 1;

$total_conceptos = $presupuesto->count_concepto($id_pres);
$total_albaranes = $presupuesto->count_albaranes($id_pres);
$total_lineas = $total_conceptos + $total_albaranes;
$consciente = $total_lineas / 30;
$total_page = ceil($consciente);

$array_conceptos_pedidos = $presupuesto->get_conceptos_pedido($id_pres);

$pie_presupuesto = "";
$pie_presupuesto = $cliente->get_pie_presupuesto($_SESSION['clproc'], $_SESSION['clcodi']);

$referencia = "";
if (isset($datos_presupuesto[0]['referencia'])):
    $referencia = $datos_presupuesto[0]['referencia'];
endif;

$forma_pago = $datos_cliente['0']['clforp'];
$datos_forma_pago = $presupuesto->getFormaPago($forma_pago);
$texto_forma_pago = $datos_forma_pago['fpdsfp'];

$aplazamiento = $datos_cliente['0']['clncfr'];
$datos_aplazamiento = $presupuesto->getAplazamiento($aplazamiento);
$texto_aplazamiento = $datos_aplazamiento['APDENC'];

$filename = $datos_presupuesto[0]['num_pre'] . '.pdf';
$filename = str_replace(" ", "-", $filename);
$filename = str_replace("%20", "-", $filename);

include "../../mod_pdf/dompdf-master/autoload.inc.php";
ob_start();

use Dompdf\Dompdf;

$dompdf = new DOMPDF();
?>
<html>
    <head>
        <style>

            .hidden{
                display: none;
            }

            .font-10{
                font-size: 10px;
            }

            sup{
                font-size: 6px;
            }

            .break_page{
                margin-top: 200px;
            }

            .page-number:after {
                content: counter(page);
            }

            .page_break {
                page-break-before: always;
            }

            @page {
                margin-top: 130px;
            }

            .header {
                position: fixed;
                left: 0px;
                top: -80px;
                right: 0px;
                height: 280px;
                text-align: center;
            }

            .footer {
                position: fixed;
                left: 0px;
                bottom: 0px;
                right: 0px;
                height: 20px;
            }

            .footer .page:after {
                content: counter(page, upper-roman);
            }

            table.page_header {
                width: 100%;
                border: none;
            }

            .img_logo {
                width: 240px;
                height: auto;
                background-repeat: no-repeat;
                display: inline-block;
            }

            .img_pie {
                width: 700px;
                height: auto;
                background-repeat: no-repeat;
                display: inline-block;
            }

            .content_texto_lateral {
                width: 0px;
                display: inline-block;
                position: fixed;
                margin-left: 0px;
            }

            .img_texto {
                height: 800px;
                background-repeat: no-repeat;
                display: inline-block;
                margin-top: 0px;
                margin-left: -45px;
                width: 50px;
            }

            .img_texto_break{
                width: 50px !important;
                margin-left: -46px;
                height: 930px !important;
            }

            .img_texto_break{
                height: 500px;
                width: 10px;
                margin-left: 40px;
                background: url(../../mod_pdf/img_pdf/texto_lateral_plantilla_repeat.png) no-repeat;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }

            .content {
                width: 90%;
                margin-top: 200px;
                margin-left: 20px;
            }

            * {
                font-size: 11px;
                font-family: "Helvetica Neue", Helvetica;
            }

            .table_datos_pre {
                margin-left: 205px;
            }

            .columna1_cabecera {
                width: 30px;
                padding-left: 5px;
                text-align: left;
            }

            .columna1 {
                width: 30px;
                padding-left: 5px;
                text-align: right;
                font-size: 10px;
            }

            .columna2_cabecera{
                width: 200px;
                padding-left: 5px;
                text-align: center;
            }

            .columna2 {
                width: 200px;
                padding-left: 5px;
                text-align: left;
                font-size: 10px;
            }

            .columna3_cabecera{
                width: 37px;
                padding-left: 5px;
                text-align: center;
            }

            .columna3{
                width: 37px;
                padding-left: 5px;
                text-align: right;
                font-size: 10px;
            }

            .columna4_cabecera{
                width: 37px;
                padding-left: 5px;
                text-align: center;
            }

            .columna4{
                width: 37px;
                padding-left: 5px;
                text-align: right;
                font-size: 10px;
            }

            .columna5_cabecera{
                width: 37px;
                padding-left: 5px;
                text-align: center;
            }

            .columna5{
                width: 37px;
                padding-left: 5px;
                text-align: right;
                font-size: 10px;
            }

            .columna6_cabecera{
                width: 37px;
                padding-left: 5px;
                text-align: center;
            }

            .columna6{
                width: 37px;
                padding-left: 5px;
                text-align: right;
                font-size: 10px;
            }

            .columna7_cabecera{
                width: 37px;
                padding-left: 5px;
                text-align: center;
            }

            .columna7{
                width: 37px;
                padding-left: 5px;
                text-align: right;
                font-size: 10px;
            }

            .columna8_cabecera{
                width: 40px;
                padding-left: 5px;
                text-align: center;
            }

            .columna8{
                width: 40px;
                padding-left: 5px;
                text-align: right;
                font-size: 10px;
            }

            .columna9_cabecera{
                width: 50px;
                padding-left: 5px;
                text-align: center;
            }

            .columna9{
                width: 50px;
                padding-left: 5px;
                text-align: right;
                font-size: 10px;
            }

            .columna10_cabecera{
                width: 37px;
                padding-left: 5px;
                text-align: center;
            }

            .columna10{
                width: 37px;
                padding-left: 5px;
                text-align: right;
                font-size: 10px;
            }

            .columna11_cabecera{
                width: 50px;
                padding-left: 5px;
                text-align: center;
            }

            .columna11{
                width: 50px;
                padding-left: 5px;
                text-align: right;
                font-size: 10px;
            }

            .table_pdf {
                min-width: 673px !important;
                border: 1px solid #000;
                border-collapse: collapse;
            }

            .table_border_right {
                border-right: 1px solid black;
            }

            .table_border_top {
                border-top: 1px solid black;
            }

            .table_border_left {
                border-left: 1px solid black;
            }

            .table_border_bottom {
                border-bottom: 1px solid black;
            }

            table.page_footer {
                width: 100%;
                border: none;
            }

            .title_info_pedido {
                font-size: 8px;
                font-weight: bold;
                background-color: #dadada
            }

            .cif {
                margin-left: 50px;
            }

            .title_datos {
                font-weight: bold;
            }

            .font-bold {
                font-weight: bold;
            }

            .border {
                border: 1px solid black;
                border-radius: 4px;
                width: 340px;
                font-size: 12px;
            }

            .border1 {
                font-size: 12px;
                border: 1px solid black;
                border-radius: 4px;
                height: 30px;
                padding-right: 10px;
                padding-left: 10px;
                vertical-align: middle;
                width: 125px !important;
                text-align: center;
            }

            .border_text {
                border: 1px solid black;
                vertical-align: middle;
                border-radius: 4px;
            }

            .texto_validez, .texto_pago {
                width: 340px;
                font-size: 10px;
                padding-left: 10px;
                padding-top: 4px;
                padding-bottom: 2px;
                height: 15px;
            }

            .texto_pago {
                margin-top: 2px;
                height: 43px;
            }

            .texto_conforme {
                width: 145px;
                font-size: 10px;
                margin-left: 20px;
                font-weight: bold;
                text-align: center;
                height: 75px;
                display: inline-block;
                margin-top: 7.5px;
            }

            .texto_sello {
                font-size: 10px;
                font-weight: bold;
                text-align: center;
                display: inline-block;
                height: 75px;
                margin-left: 10px;
                width: 158px;
            }

            .datos_fiscales {
                width: 331px;
                margin-left: -2px !important;
            }

            .dire_reco {
                width: 331px;
            }

            .td_space {
                width: 20px;
            }

            .border3 {
                border: 1px solid black;
                border-radius: 4px;
                margin-left: 108px !important;
                width: 160px;
            }

            .info_firma {
                font-weight: bold;
                font-size: 9.8px;
                margin-left: 8px;
            }

            hr {
                width: 106.6%;
            }

            .content_precios_line1 {
                width: 75%;
            }

            .total_presupuesto {
                width: 25%;
                margin-left: 82%;
                text-align: right;
                font-weight: bold;
                font-size: 14px;
                margin-top: -55px;
            }

            .border_total_presupuesto {
                border: 1px solid black;
                border-radius: 4px;
                height: 28px;
                font-size: 22px !important;
                padding-right: 4px;
                padding-top: 4px;
            }

            .content_precios_line3 {
                margin-top: 3px;
            }

            .content_precios_line3 {
                width: 75%;
            }

            .line {
                display: inline-block;
                font-size: 8px;
            }

            .line1_1 {
                margin-left: 170px;
            }

            .line1_2 {
                margin-left: 130px;
            }

            .line2_1 {
                margin-left: 10px;
            }

            .line2_2 {
                margin-left: 52px;
            }

            .line2_3 {
                margin-left: 75px;
            }

            .line2_4 {
                margin-left: 72px;
            }

            .line2_5 {
                margin-left: 72px;
            }

            .line3_1 {
                margin-left: 1px;
            }

            .line3_1, .line3_2, .line3_3, .line3_4, .line3_5 {
                width: 98px;
                font-size: 10px;
                border: 1px solid #000000;
                background-color: #dadada;
                margin-left: -2px;
                height: 15px;
                padding-top: 2px;
            }

            .text-center {
                text-align: center;
            }

            .text-right {
                text-align: right;
            }

            .file_2_total {
                width: 107%;
                display: inline-flex;
                height: 80px;
            }

            .file_2_w50 {
                width: 50%;

            }

            .file_2_w50_2 {
                width: 50%;
                margin-left: 50%;
            }

            .table_direcciones {
                margin-left: 15px;
            }

            .text_continua{
                margin-left: 98%;
                margin-top: 90px;
                font-weight: bold;
            }

            .margin_ped{
                margin-left: 156px !important;
            }

        </style>
    <div class="header">
        <table class="table_direcciones">

            <?php if (!$class_hidden_pedido == "hidden"): ?>
                <tr>
                    <td>
                        <?php
                        if ($imagen_preview = glob(path_uploads_files . "logos/" . $_SESSION['clproc'] . $_SESSION['clcodi'] . ".*")):
                            $logo_personalizado = true;
                            $array_nombre_extension = explode('.', $imagen_preview[0]);
                            $extension = array_pop($array_nombre_extension);
                            ?>

                            <img class="image_logo_cliente" src="<?= path_uploads_files . "logos/" . $_SESSION['clproc'] . $_SESSION['clcodi'] . "." . $extension ?>">
                        <?php else: ?>
                            <br> 
                        <?php endif;
                        ?>
                    </td>
                </tr>
            <?php endif; ?>

            <tr>
                <td class="title_datos datos_fiscales">DATOS FISCALES</td>
                <?php
                /* if ($datos_presupuesto[0]['dire_envio'] == 0) {
                  $title_envio = "DIRECCIÓN DE RECOGIDA MERCANCIA";
                  $title_nombre = "MOSQUIFLEX S.L.";
                  } else {
                  $title_envio = "DIRECCIÓN DE ENVÍO MERCANCIA";
                  $title_nombre = $_SESSION['name_log'];
                  } */
                $title_envio = "DIRECCIÓN DE ENVÍO MERCANCIA";
                $title_nombre = $_SESSION['name_log'];
                if (!empty($dire['destinatario'])):
                    $title_nombre = $dire['destinatario'];
                endif;
                ?>
                <td class="td_space"></td>
                <?php if ($class_hidden_pedido == "hidden"): ?>
                    <td class="title_datos dire_reco"><?= $title_envio ?></td>
                <?php else: ?>
                    <td class="title_datos dire_reco">DATOS CLIENTE</td>
                <?php endif; ?>
            </tr>
            <tr>
                <td class="border datos_fiscales">
                    <br><span class="font-bold dire_clnomb"><?= $_SESSION['name_log'] ?></span><br><br><br>
                    <span class="dire_cldir1"><?= $dire_fiscal['cldir1'] ?></span>&nbsp;
                    <span class="dire_cldir2"><?= $dire_fiscal['cldir2'] ?></span><br>
                    <span class="dire_poblacion"><?= $poblacion_fiscal ?></span><br>
                    <span class="dire_provincia"><?= $provincia_fiscal ?></span><br>
                    <span class="dire_pais"><?= $pais_fiscal ?></span><br>
                    <span class="dire_tlf1"><?= $dire_fiscal['cltlf1'] ?></span><br>
                    <span class="dire_tlf2"><?= $dire_fiscal['cltlf2'] ?></span><br>
                    <span
                        class="font-bold">CÓDIGO CLIENTE: </span><?= str_pad($dire_fiscal['clproc'], 2, "0", STR_PAD_LEFT) ?><?= str_pad($dire_fiscal['clcodi'], 5, "0", STR_PAD_LEFT) ?>
                    <span class="font-bold cif">CIF:</span><?= $dire_fiscal['cldnic'] ?><br>
                </td>
                <td class="td_space"></td>
                <?php if ($class_hidden_pedido == "hidden"): ?>
                    <td class="border dire_reco">
                        <br><span class="font-bold dire_clnomb"><?= $title_nombre ?></span><br><br><br>
                        <span class="dire_cldir1"><?= $dire['cldir1'] ?></span>&nbsp;
                        <span class="dire_cldir2"><?= $dire['cldir2'] ?></span><br>
                        <span class="dire_poblacion"><?= $poblacion ?></span><br>
                        <span class="dire_provincia"><?= $provincia ?></span><br>
                        <span class="dire_pais"><?= $pais ?></span><br>
                        <span class="dire_tlf1"><?= $dire['cltlf1'] ?></span><br>
                        <span class="dire_tlf2"><?= $dire['cltlf2'] ?></span><br>
                    </td>
                <?php else: ?>
                    <td class="dire_reco border">
                        <span class="dire_clnomb">
                            <?php
                            echo $datos_cabecera_cliente;
                            ?>
                        </span>
                    </td>
                <?php endif; ?>
            </tr>
        </table>

        <table class="table_datos_pre">
            <tr>

                <td class="border1" rowspan="2"><span
                        class="font-bold">Nº SOLICITUD:</span> <?= hideSol($num_presupuesto) ?></td>

                <td class="border3"><span class="font-bold">FECHA:</span><?= $fecha ?>
                    &nbsp;&nbsp;&nbsp;Pág. <span class="page-number"></span>/<?= $total_page ?></td>
            </tr>
            <tr>
                <?php if ($class_hidden_pedido == "hidden"): ?>
                    <td class="border3"><span class="font-bold">AGENCIA:</span><?= $agencia ?></td>
                <?php endif; ?>
            </tr>
            <?php if (!empty($referencia)): ?>
                <tr>
                    <td class="border1"><span class="font-bold">REFERENCIA:</span><?= $referencia ?></td>
                </tr>
            <?php endif; ?>
        </table>
    </div>

    <?php if ($class_hidden_pedido == "hidden"): ?>
        <div class="footer">
            <table class="page_footer">
                <tr>
                    <td>
                        <img class="img_pie" src="../../mod_pdf/img_pdf/pie_plantilla.png">
                    </td>
                </tr>
            </table>
        </div>
    <?php endif; ?>

    <div class="content">
        <div class="content_texto_lateral">
            <?php if ($class_hidden_pedido == "hidden"): ?>
                <img class="img_texto" src="../../mod_pdf/img_pdf/texto_lateral_plantilla.png">
            <?php else: ?>
                <img class="img_texto" src="">
            <?php endif; ?>
        </div>

        <table class="table_pdf" style="text-align: center;">
            <tr>
                <td class="columna1_cabecera table_border_left table_border_bottom table_border_top title_info_pedido">
                    CANTIDAD
                </td>
                <td class="columna2_cabecera table_border_bottom table_border_top title_info_pedido">ARTÍCULO</td>
            </tr>
            <?php
            foreach ($albaranes as $item):

                $units_product = "";
                $title_product = "";
                $color = "";
                $ancho_product = "";
                $alto_product = "";
                $precio_unitario = "";
                $total_importe = "";
                $arumiv = "";

                //Producto acabado
                if ($item['albanc'] > 0 AND empty($item['albobs']) AND $item['albfami'] != "1999" OR ( $item['albfami'] == "2736")):

                    $units_product = $item['albunit'];
                    $title_product = $familia->getTitleProduct($item['albfami'], $item['albsub']);
                    if ($item['albfami'] == '2736' && !empty($item['albobs'])):
                        $title_product = $title_product . " - " . strtoupper($item['albobs']) . " - ";
                    endif;
                    $color = "";
                    if (isset($familia->getColoressByCocolo($item['albfami'], $item['albco'])[0]['codesc'])):
                        $color = $familia->getColoressByCocolo($item['albfami'], $item['albco'])[0]['codesc'];
                    else:
                        $color = $item['albco'];
                    endif;

                    $color = ucfirst(str_replace("Lacado ", "", $color));

                    $is_lacado = $familia->getIsLacado($item['albco'], $item['albfami']);
                    if (!empty($is_lacado)):
                        $color = " LACADO " . $color;
                    endif;

                    $ancho_product = number_format($item['albanc'], 3);
                    $alto_product = number_format($item['albalt'], 3);

                    $precio_unitario = round2decimals($item['albpre']) / $item['albunit'];
                    $precio_unitario = round2decimals($precio_unitario);

                    $total_importe = round2decimals($precio_unitario) * $item['albunit'];
                    $total_importe = round2decimals($total_importe);

                    $total_presupuesto = $total_presupuesto + round2decimals($total_importe);

                //componente metros
                elseif ($item['albobs'] == "is_component_meter"):

                    $units_product = $item['albunit'];
                    $aux_title = str_replace('279990', '', $item['albco']);
                    $arumiv = $familia->getUnidadFacturacion($item['albfami'], $item['albsub'], $aux_title);
                    $title_product = $familia->getTitleComponent($item['albfami'], $item['albsub'], $aux_title);

                    $version_product = getCajon($item['albsub']);
                    $color = $familia->getColoressByCocolo($item['albfami'], $item['albco'])[0]['codesc'];

                    $ancho_product = number_format($item['albanc'], 3);

                    $precio_unitario = round2decimals($item['albpre']);
                    $precio_unitario = round2decimals($precio_unitario);

                    $total_importe = $precio_unitario * $units_product * $ancho_product;
                    if ($is_almacen == "1"):
                        $total_importe = round2decimals($total_importe / $arumiv);
                    else:
                        $total_importe = round2decimals($total_importe);
                    endif;

                    $total_presupuesto = $total_presupuesto + $total_importe;

                //componente
                elseif ($item['albobs'] == "is_component"):

                    $units_product = $item['albunit'];
                    $title_product = str_replace('279990', '', $item['albco']);
                    $arumiv = $familia->getUnidadFacturacion($item['albfami'], $item['albsub'], $title_product);
                    $title_product = $familia->getTitleComponent($item['albfami'], $item['albsub'], $title_product);
                    $version_product = getCajon($item['albsub']);
                    $color = $familia->getColoressByCocolo($item['albfami'], $item['albco'])[0]['codesc'];

                    $precio_unitario = round2decimals($item['albpre']);
                    $precio_unitario = round2decimals($precio_unitario);

                    $total_importe = $precio_unitario * $units_product;
                    if ($is_almacen == "1"):
                        $total_importe = round2decimals($total_importe / $arumiv);
                    else:
                        $total_importe = round2decimals($total_importe);
                    endif;

                    $total_presupuesto = $total_presupuesto + $total_importe;

                //incremento
                elseif (strpos($item['albobs'], "is_increment") !== false):

                    $units_product = $item['albunit'];
                    $aux_id = str_replace("is_increment", "", $item['albobs']);
                    $aux_id = str_replace('"', "", $aux_id);
                    $aux_id = str_replace(' ', "", $aux_id);
                    $arfami = substr($aux_id, 0, 4);
                    $arsubf = substr($aux_id, 4, 2);
                    $ararti = substr($aux_id, 6, 6);
                    $componente = $familia->searchComponentByCode($arfami, $arsubf, $ararti);
                    $title_product = "";

                    if (!empty($item['adjunto'])):
                        $title_product .= "-------¡¡¡OJO CROQUIS!!!-------<br>";
                    endif;
                    if (isset($componente[0]['ardesc'])):
                        $title_product .= $componente[0]['ardesc'];
                    endif;

                    $precio_unitario = round2decimals($item['albpre']) / $units_product;
                    $precio_unitario = round2decimals($precio_unitario);

                    $total_importe = round2decimals($precio_unitario) * $units_product;
                    $total_importe = round2decimals($total_importe);

                    $total_presupuesto = $total_presupuesto + round2decimals($total_importe);

                    if (!empty($item['adjunto'])):
                        $title_product .= "<br>-------¡¡¡FIN CROQUIS!!!-------";
                    endif;

                //componente unidad
                elseif ($item['albobs'] == "is_component_unit"):

                    $units_product = $item['albunit'];
                    $title_product = str_replace('279990', '', $item['albco']);
                    $arumiv = $familia->getUnidadFacturacion($item['albfami'], $item['albsub'], $title_product);
                    $title_product = $familia->getTitleComponent($item['albfami'], $item['albsub'], $title_product);
                    $ancho_product = number_format($item['albanc'], 3);
                    $alto_product = number_format($item['albalt'], 3);

                    $precio_unitario = round2decimals($item['albpre']);
                    $precio_unitario = round2decimals($precio_unitario);

                    $version_product = getCajon($item['albsub']);
                    $color = $familia->getColoressByCocolo($item['albfami'], $item['albco'])[0]['codesc'];
                    $total_importe = $ancho_product * $alto_product;
                    $total_importe = round2decimals($total_importe) * round2decimals($precio_unitario) * $units_product;
                    if ($is_almacen == "1"):
                        $total_importe = round2decimals($total_importe / $arumiv);
                    else:
                        $total_importe = round2decimals($total_importe);
                    endif;

                    $total_presupuesto = $total_presupuesto + $total_importe;

                //lacado
                elseif ($item['albobs'] == "lacado"):

                    $color = "";
                    $color = $item['albco'];
                    $units_product = $item['albunit'];
                    $precio_unitario = round2decimals($item['albpre']);
                    $title_product = "Lacado - RAL. ";
                    $total_importe = round2decimals($item['albpre']);

                    $total_presupuesto = $total_presupuesto + $total_importe;

                //expositores
                elseif ($item['albfami'] == "1999" AND ! empty($item['albunit'])):

                    $units_product = $item['albunit'];
                    $title_product = obtener_titulo_expositor($item['albarti']);
                    $color = "";

                    $ancho_product = number_format($item['albanc'], 3);
                    $alto_product = number_format($item['albalt'], 3);

                    $precio_unitario = round2decimals($item['albpre']);

                    $total_importe = round2decimals($precio_unitario) * $item['albunit'];
                    $total_importe = round2decimals($total_importe);

                    $total_presupuesto = $total_presupuesto + round2decimals($total_importe);

                //linea3
                elseif ($item['albobs'] == "Linea3"):
                    $title_product = "-----------------------------------------------------------------";
                endif;

                //Ultima linea antes del salto
                if ($num_lineas_presupuesto > 0 AND $num_lineas_presupuesto % 31 == 0):
                    ?>

                    <tr>
                        <td class="columna1 table_border_left table_border_bottom"><?= $units_product ?></td>
                        <td class="columna2 table_border_right table_border_bottom table_border_right"><?= $title_product ?> <?= $color ?></td>

                    </tr>

                <?php else: ?>

                    <tr>
                        <td class="columna1 table_border_left table_border_right"><?= $units_product ?></td>
                        <td class="columna2 table_border_right table_border_right"><?= $title_product ?> <?= $color ?></td>

                    </tr>

                <?php
                endif;

                $num_lineas_presupuesto++;

                if ($num_lineas_presupuesto % 30 == 0):
                    ?>
                    <?php $saltos_pagina++; ?>
                </table>
                <div class="text_continua">Continua...</div>
                <div style="page-break-after: always;"></div>
                <div class="content_texto_lateral">
                    <?php if ($class_hidden_pedido == "hidden"): ?>
                        <img class="img_text img_texto_break" src="img_pdf/texto_lateral_plantilla.png">
                    <?php else: ?>
                        <img class="img_texto" src="">
                    <?php endif; ?>
                </div>
                <table class="table_pdf break_page" style="text-align: center;">
                    <tr>
                        <td class="columna1_cabecera table_border_left table_border_bottom table_border_top title_info_pedido">
                            CANTIDAD
                        </td>
                        <td class="columna2_cabecera table_border_bottom table_border_top title_info_pedido">ARTÍCULO</td>

                    </tr>
                    <?php
                endif;

            endforeach;

            $total_presupuesto_iva = round2decimals($total_presupuesto * $iva);
            $total_presupuesto_recargo = round2decimals($total_presupuesto * 0.0520);
            $total_presupuesto_impuestos = $total_presupuesto + $total_presupuesto_iva + $total_presupuesto_recargo;
            $aux_lineas = $saltos_pagina * 30;

            if ($class_hidden_pedido == "hidden"):
                $aux_lineas = $aux_lineas + 10;
            endif;

            //LINEAS EN BLANCO
            for ($i = $num_lineas_presupuesto; $i < $aux_lineas; $i++):
                ?>
                <tr>
                    <td class="columna1 table_border_left table_border_right">&nbsp;</td>
                    <td class="columna2 table_border_right table_border_right">&nbsp;</td>

                </tr>
            <?php endfor;
            ?>

            <tr>
                <td class="columna1 table_border_left table_border_bottom table_border_right">&nbsp;</td>
                <td class="columna2 table_border_bottom table_border_right table_border_right">&nbsp;</td>

            </tr>
        </table>

        <?php if ($class_hidden_pedido == "hidden"): ?>
            <span class="info_firma <?= $class_hidden_pedido ?>">
                LA FIRMA DE ESTE DOCUMENTO IMPLICA LA ACEPTACIÓN DEL DETALLE TOTAL DEL PRODUCTO, FORMA DE PAGO E IMPORTE
            </span>
            <hr width="106.6%" class="<?= $class_hidden_pedido ?>">
            <div class="content_precios <?= $class_hidden_pedido ?>">
                <div class="content_precios_line1 line">
                    <div class="line1_1 line">I.V.A</div>
                    <div class="line1_2 line">RECARGO EQUIVALENCIA</div>
                </div>
                <br>
                <div class="content_precios_line2 line">
                    <div class="line2_1 line">BASE IMPONIBLE</div>
                    <div class="line2_2 line">%</div>
                    <div class="line2_3 line">CUOTA</div>
                    <div class="line2_4 line">%</div>
                    <div class="line2_5 line">CUOTA</div>
                </div>
                <br>
                <div class="content_precios_line3 line">
                    <div class="line3_1 line text-center"><span><?= formato_dinero(round2decimals($total_presupuesto)) ?></span></div>
                    <div class="line3_2 line text-center"><span>21,00</span></div>
                    <div class="line3_3 line text-right"><span><?= formato_dinero(round2decimals($total_presupuesto_iva)) ?></span></div>
                    <div class="line3_4 line text-center"><span>5,20</span></div>
                    <div class="line3_5 line text-right"><span><?= formato_dinero(round2decimals($total_presupuesto_recargo)) ?></span></div>
                </div>
                <div class="total_presupuesto">
                    <span>TOTAL PRESUPUESTO</span>
                    <div class="border_total_presupuesto">
                        <span><?= formato_dinero(round2decimals($total_presupuesto_impuestos)) ?>€</span>
                    </div>
                </div>
            </div>
            <div class="file_2_total <?= $class_hidden_pedido ?>">
                <div class="file_2_w50">
                    <div class="border_text texto_validez <?= $class_hidden_pedido ?>">
                        LA PRESENTE OFERTA TIENE UNA VALIDEZ HASTA EL <?= $fecha_validez ?>
                    </div>
                    <div class="border_text texto_pago <?= $class_hidden_pedido ?>">
                        <span class="font-10 font-bold">FORMA DE PAGO:</span><br>
                        <span class="font-10"><?= $texto_forma_pago ?></span>&nbsp;&nbsp;&nbsp;
                        <span class="font-10"><?= $texto_aplazamiento ?></span>
                    </div>
                </div>
                <div class="file_2_w50_2 <?= $class_hidden_pedido ?>">
                    <div class="border_text texto_conforme">
                        CONFORME CLIENTE
                    </div>
                    <div class="border_text texto_sello <?= $class_hidden_pedido ?>">
                        ENTRECORTINAS, S.L.
                    </div>
                </div>
            </div>
        </div>
    <?php else: ?>
        <div class="content_precios <?= $class_hidden_pedido ?>">
            <div class="total_presupuesto">
                <span>TOTAL PRESUPUESTO</span>
                <div class="border_total_presupuesto">
                    <span><?= formato_dinero(round2decimals($total_presupuesto)) ?>€</span>
                </div>
            </div>
        </div>
        <div class="content_precios <?= $class_hidden_pedido ?>">
            <div class="total_presupuesto">
                <span class="text-left">*IMPUESTOS NO INCLUIDOS*</span>
            </div>
        </div>
        <div class="contenido_firma_personalizada content_precios"><span><?php echo characteresSqlToHtml($pie_presupuesto[0]['pie_personalizado']); ?></span></div>
            <?php endif; ?>

    <?php
    $dompdf->load_html(ob_get_clean());
    $dompdf->render();
    $output = $dompdf->output();
    $ruta_pdf_pvp_email = path_pdfs . "solicitud/" . $filename;
    file_put_contents(path_pdfs . "solicitud/" . $filename, $output);
    