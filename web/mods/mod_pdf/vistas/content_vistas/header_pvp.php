<div class="header">
    <table class="table_direcciones">

        <?php if (!$class_hidden_pedido == "hidden"): ?>
            <tr>
                <td>
                    <?php
                    if ($imagen_preview = glob(path_uploads_files . "logos/" . $_SESSION['clproc'] . $_SESSION['clcodi'] . ".*")):
                        $logo_personalizado = true;
                        $array_nombre_extension = explode('.', $imagen_preview[0]);
                        $extension = array_pop($array_nombre_extension);
                        ?>

                        <img class="image_logo_cliente" src="<?= path_uploads_files . "logos/" . $_SESSION['clproc'] . $_SESSION['clcodi'] . "." . $extension ?>">
                    <?php else: ?>
                        <br> 
                    <?php endif;
                    ?>
                </td>
            </tr>
        <?php endif; ?>

        <tr>
            <td class="title_datos datos_fiscales">DATOS FISCALES</td>
            <?php
            /* if ($datos_presupuesto[0]['dire_envio'] == 0) {
              $title_envio = "DIRECCIÓN DE RECOGIDA MERCANCIA";
              $title_nombre = "MOSQUIFLEX S.L.";
              } else {
              $title_envio = "DIRECCIÓN DE ENVÍO MERCANCIA";
              $title_nombre = $_SESSION['name_log'];
              } */
            $title_envio = "DIRECCIÓN DE ENVÍO MERCANCIA";
            $title_nombre = $_SESSION['name_log'];
            if (!empty($dire['destinatario'])):
                $title_nombre = $dire['destinatario'];
            endif;
            ?>
            <td class="td_space"></td>
            <?php if ($class_hidden_pedido == "hidden"): ?>
                <td class="title_datos dire_reco"><?= $title_envio ?></td>
            <?php else: ?>
                <td class="title_datos dire_reco">DATOS CLIENTE</td>
            <?php endif; ?>
        </tr>
        <tr>
            <td class="border datos_fiscales">
                <br><span class="font-bold dire_clnomb"><?= $_SESSION['name_log'] ?></span><br><br><br>
                <span class="dire_cldir1"><?= $dire_fiscal['cldir1'] ?></span>&nbsp;
                <span class="dire_cldir2"><?= $dire_fiscal['cldir2'] ?></span><br>
                <span class="dire_poblacion"><?= $poblacion_fiscal ?></span><br>
                <span class="dire_provincia"><?= $provincia_fiscal ?></span><br>
                <span class="dire_pais"><?= $pais_fiscal ?></span><br>
                <span class="dire_tlf1"><?= $dire_fiscal['cltlf1'] ?></span><br>
                <span class="dire_tlf2"><?= $dire_fiscal['cltlf2'] ?></span><br>
                <span
                    class="font-bold">CÓDIGO CLIENTE: </span><?= str_pad($dire_fiscal['clproc'], 2, "0", STR_PAD_LEFT) ?><?= str_pad($dire_fiscal['clcodi'], 5, "0", STR_PAD_LEFT) ?>
                <span class="font-bold cif">CIF:</span><?= $dire_fiscal['cldnic'] ?><br>
            </td>
            <td class="td_space"></td>
            <?php if ($class_hidden_pedido == "hidden"): ?>
                <td class="border dire_reco">
                    <br><span class="font-bold dire_clnomb"><?= $title_nombre ?></span><br><br><br>
                    <span class="dire_cldir1"><?= $dire['cldir1'] ?></span>&nbsp;
                    <span class="dire_cldir2"><?= $dire['cldir2'] ?></span><br>
                    <span class="dire_poblacion"><?= $poblacion ?></span><br>
                    <span class="dire_provincia"><?= $provincia ?></span><br>
                    <span class="dire_pais"><?= $pais ?></span><br>
                    <span class="dire_tlf1"><?= $dire['cltlf1'] ?></span><br>
                    <span class="dire_tlf2"><?= $dire['cltlf2'] ?></span><br>
                </td>
            <?php else: ?>
                <td class="dire_reco border">
                    <span class="dire_clnomb">
                        <?php
                        echo $datos_cabecera_cliente;
                        ?>
                    </span>
                </td>
            <?php endif; ?>
        </tr>
    </table>

    <table class="table_datos_pre">
        <tr>
            <?php if ($is_pedido == "0" || $is_pedido == "2"): ?>
                <?php if ($class_hidden_pedido == "hidden"): ?>
                    <td class="border1" rowspan="2"><span
                            class="font-bold">Nº PRESUPUESTO:</span> <?= hidePpto($num_presupuesto) ?></td>
                    <?php else: ?>
                    <td class="border1 num_pre_cliente" rowspan="1"><span
                            class="font-bold">PRESUPUESTO:</span><p><?= hidePpto($num_presupuesto) ?></p></td>
                    <?php endif; ?>
                <?php else: ?>
                <td class="border1" rowspan="2"><span
                        class="font-bold">Nº PEDIDO:</span> <?= hidePed($num_presupuesto) ?></td>

            <?php endif; ?>

            <td class="border3"><span class="font-bold">FECHA:</span><?= $fecha ?>
                &nbsp;&nbsp;&nbsp;Pág. <span class="page-number"></span>/<?= $total_page ?></td>
        </tr>
        <tr>
            <?php if ($class_hidden_pedido == "hidden"): ?>
                <td class="border3"><span class="font-bold">AGENCIA:</span><?= $agencia ?></td>
            <?php endif; ?>
        </tr>
        <?php if (!empty($referencia) && $is_pedido == "1"): ?>
            <tr>
                <td class="border1"><span class="font-bold">REFERENCIA:</span><?= $referencia ?></td>
            </tr>
        <?php endif; ?>
    </table>
</div>