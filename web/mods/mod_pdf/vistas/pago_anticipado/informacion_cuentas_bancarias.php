<?php ?>
<div style="page-break-after: always;"></div>

<table>
    <tr>
        <td style="font-size: 16px" colspan="12">Estimado cliente:</td>
    </tr>
    <tr><td></td></tr>
    <tr>
        <td style="font-size: 16px" colspan="12">Su pedido se tramitará una vez comprobado el ingreso del importe correspondiente en algunas de las cuentas bancarias que le facilitamos.</td>
    </tr>
    <tr><td></td></tr>
    <tr>
        <td style="font-size: 16px" colspan="12">Le facilitamos los números de cuentas donde puede hacer la transferencia:</td>
    </tr>
    <tr><td></td></tr>
    <tr>
        <td style="font-size: 18px" colspan="12">BBVA ES52 0182 0455 2302 0161 1986</td>
    </tr>
    <tr><td></td></tr>
    <tr>
        <td style="font-size: 18px" colspan="12">BANKIA ES60 2038 9807 4760 0035 6906</td>
    </tr>
    <tr><td></td></tr>
    <tr>
        <td style="font-size: 18px" colspan="12">ABANCA ES90 2080 1205 9255 0000 0048</td>
    </tr>
    <tr><td></td></tr>    
    <tr>
        <td style="font-size: 18px" colspan="12">ING DIRECT ES66 1465 0100 9119 0040 8098</td>
    </tr>
    <tr><td></td></tr>    
    <tr>
        <td style="font-size: 18px" colspan="12">LA CAIXA ES40 2100 1619 8702 0013 2247</td>
    </tr>
    <tr><td></td></tr>
    <tr>
        <td style="font-size: 18px" colspan="12">IBERCAJA ES31 2085 8166 0103 3040 3631</td>
    </tr>
    <tr><td></td></tr>
    <tr>
        <td style="font-size: 18px" colspan="12">CAJAMAR ES14 3058 3009 0427 2001 2790</td>
    </tr>
    <tr><td></td></tr>
    <tr>
        <td style="font-size: 18px" colspan="12">TARGOBANK ES92 0216 1114 8581 0015 7070</td>
    </tr>
    <tr><td></td></tr>
    <tr><td></td></tr>
    <tr>
        <td style="font-size: 16px" colspan="12">Sin otro particular, reciba un cordial saludo.</td>
    </tr>
    <tr><td></td></tr>
</table>