<html>
<head>
    <style>

        .page_break { page-break-before: always; }

        @page {
            margin-top: 150px;
        }

        #header {
            position: fixed;
            left: 0px;
            top: -80px;
            right: 0px;
            height: 40px;
            text-align: center;
        }

        #footer {
            position: fixed;
            left: 0px;
            bottom: 0px;
            right: 0px;
            height: 20px;
        }

        #footer .page:after {
            content: counter(page, upper-roman);
        }

        table.page_header {
            width: 100%;
            border: none;
        }

        .img_logo {
            width: 240px;
            height: auto;
            background-repeat: no-repeat;
            display: inline-block;
        }

        .img_pie {
            width: 700px;
            height: auto;
            background-repeat: no-repeat;
            display: inline-block;
        }

        .content_texto_lateral{
            width: 30px;
            display: inline-block;
            position: fixed;
            margin-left: -40px;
        }

        .img_texto{
            height: 400px;
            width: auto;
            background-repeat: no-repeat;
            display: inline-block;
            margin-top: 300px;
        }

        #content {
            width: 90%;
            margin-top: -10px;
            margin-left: 20px;
        }

        * {
            font-size: 11px;
            font-family: Helvetica;
        }

        .table_datos_pre {
            margin-left: 190px;
        }

        .first_column {
            width: 200px;
            padding-left: 5px;
            text-align: left;
        }

        .second_column {
            width: 150px;
            padding-left: 5px;
            text-align: left;
        }

        .third_column {
            width: 75px;
            text-align: right;
        }

        .four_column {
            width: 75px;
            text-align: right;
        }

        .five_column {
            width: 75px;
            text-align: right;
        }

        .six_column {
            width: 75px;
            text-align: right;
        }

        .table_pdf {
            width: 100%;
            border: 1px solid red;
            border-collapse: collapse;
        }

        .table_border_right {
            border-right: 1px solid black;
        }

        .table_border_top {
            border-top: 1px solid black;
        }

        .table_border_left {
            border-left: 1px solid black;
        }

        .table_border_bottom {
            border-bottom: 1px solid black;
        }

        table.page_header {
            width: 100%;
            border: none;
        }

        table.page_footer {
            width: 100%;
            border: none;
        }

        .title_info_pedido {
            font-weight: bold;
            background-color: #dadada
        }

        .cif {
            margin-left: 50px;
        }

        .title_datos {
            font-weight: bold;
        }

        .font-bold {
            font-weight: bold;
        }

        .border {
            border: 1px solid black;
            border-radius: 4px;
            width: 340px;
            font-size: 12px;
        }

        .border1 {
            font-size: 12px;
            border: 1px solid black;
            border-radius: 4px;
            height: 30px;
            padding-right: 10px;
            padding-left: 10px;
            vertical-align: middle;
        }

        .datos_fiscales {
            width: 331px;
            margin-left: -2px !important;
        }

        .dire_reco {
            width: 331px;
        }

        .td_space {
            width: 20px;
        }

        .border3 {
            border: 1px solid black;
            border-radius: 4px;
            margin-left: 120px !important;
        }

        .info_firma{
            font-weight: bold;
            font-size: 9.8px;
            margin-left: 8px;
        }

        hr{
            width: 106.6%;
        }

    </style>
<body>
<div id="header">
    <table class="page_header noborder">
        <tr>
            <td>
                <img class="img_logo" src="img_pdf/logo_plantilla.png">
            </td>
        </tr>
    </table>
</div>
<div id="footer">
    <table class="page_footer">
        <tr>
            <td>
                <img class="img_pie" src="img_pdf/pie_plantilla.png">
            </td>
        </tr>
    </table>
</div>
<div id="content">
    <div class="content_texto_lateral">
        <img class="img_texto" src="img_pdf/texto_lateral_plantilla.png">
    </div>
    <table>
        <tr>
            <td class="title_datos datos_fiscales">DATOS FISCALES</td>
            <?php

            if ($datos_presupuesto[0]['dire_envio'] == 0) {
                $title_envio = "DIRECCIÓN DE RECOGIDA MERCANCIA";
                $title_nombre = "MOSQUIFLEX S.L.";
            } else {
                $title_envio = "DIRECCIÓN DE ENVÍO MERCANCIA";
                $title_nombre = $_SESSION['name_log'];
            }

            ?>
            <td class="td_space"></td>
            <td class="title_datos dire_reco"><?= $title_envio ?></td>
        </tr>
        <tr>
            <td class="border datos_fiscales">
                <br><span class="font-bold dire_clnomb"><?= $_SESSION['name_log'] ?></span><br><br><br>
                <span class="dire_cldir1"><?= $dire_fiscal['cldir1'] ?></span>&nbsp;
                <span class="dire_cldir2"><?= $dire_fiscal['cldir2'] ?></span><br>
                <span class="dire_poblacion"><?= $poblacion_fiscal ?></span><br>
                <span class="dire_provincia"><?= $provincia_fiscal ?></span><br>
                <span class="dire_pais"><?= $pais_fiscal ?></span><br>
                <span class="dire_tlf1"><?= $dire_fiscal['cltlf1'] ?></span><br>
                <span class="dire_tlf2"><?= $dire_fiscal['cltlf2'] ?></span><br>
                <span
                    class="font-bold">CÓDIGO CLIENTE: </span><?= $dire_fiscal['clproc'] ?><?= $dire_fiscal['clcodi'] ?>
                <span class="font-bold cif">CIF:</span><?= $dire_fiscal['cldnic'] ?><br>
            </td>
            <td class="td_space"></td>
            <td class="border dire_reco">
                <br><span class="font-bold dire_clnomb"><?= $title_nombre ?></span><br><br><br>
                <span class="dire_cldir1"><?= $dire['cldir1'] ?></span>&nbsp;
                <span class="dire_cldir2"><?= $dire['cldir2'] ?></span><br>
                <span class="dire_poblacion"><?= $poblacion ?></span><br>
                <span class="dire_provincia"><?= $provincia ?></span><br>
                <span class="dire_pais"><?= $pais ?></span><br>
                <span class="dire_tlf1"><?= $dire['cltlf1'] ?></span><br>
                <span class="dire_tlf2"><?= $dire['cltlf2'] ?></span><br>
            </td>
        </tr>
    </table>
    <table class="table_datos_pre">
        <tr>
            <?php

            if ($is_pedido == "0"): ?>
                <td class="border1" rowspan="2"><span
                        class="font-bold">Nº PRESUPUESTO:</span> <?= hidePpto($num_presupuesto) ?></td>

            <?php else: ?>
                <td class="border1" rowspan="2"><span
                        class="font-bold">Nº PEDIDO:</span> <?= hidePed($num_presupuesto) ?></td>

            <?php endif; ?>

            <td class="border3"><span class="font-bold">FECHA:</span><?= $fecha ?>
                &nbsp;&nbsp;&nbsp;Pág.<?= $page_count ?>/<?= $total_page ?></td>
        </tr>
        <tr>
            <td class="border3"><span class="font-bold">AGENCIA:</span>pendiente</td>
        </tr>
    </table>

    <table class="table_pdf" style="text-align: center;">
        <tr>
            <td class="first_column table_border_left table_border_bottom table_border_top title_info_pedido">Producto
            </td>
            <td class="second_column title_info_pedido table_border_bottom table_border_top">Color</td>
            <td class="third_column title_info_pedido table_border_bottom table_border_top">Unidades</td>
            <td class="four_column title_info_pedido table_border_bottom table_border_top">Ancho</td>
            <td class="five_column title_info_pedido table_border_bottom table_border_top">Alto</td>
            <td class="six_column title_info_pedido table_border_bottom table_border_top table_border_right">Precio
                (€)
            </td>
        </tr>
        <?php foreach ($albaranes as $item): ?>
            <?php

            //Producto acabado
            if ($item['albanc'] > 0 AND empty($item['albobs'])):

                $title_product = $familia->getTitleProduct($item['albfami']);
                $version_product = getCajon($item['albsub']);
                $color = "";
                if (isset($familia->getColoressByCocolo($item['albfami'], $item['albco'])[0]['codesc'])):
                    $color = $familia->getColoressByCocolo($item['albfami'], $item['albco'])[0]['codesc'];
                else:
                    $color = $item['albco'];
                endif;

                ?>
                <tr>
                    <td class="first_column text-left table_border_left table_border_right"><?= $title_product ?>
                        -<?= $version_product ?></td>
                    <td class="second_column text-left table_border_right"><?= $color ?></td>
                    <td class="third_column text-center table_border_right"><?= $item['albunit'] ?></td>
                    <td class="four_column text-left table_border_right"><?= number_format($item['albanc'], 3) ?></td>
                    <td class="five_column text-left table_border_right"><?= number_format($item['albalt'], 3) ?></td>
                    <td class="six_column text-right table_border_right"><?= round2decimals($item['albpre']) ?>&nbsp;€
                    </td>
                </tr>

            <?php endif; ?>


            <?php //componente metros
            if($item['albobs'] == "is_component_meter"):

                $title_component = str_replace('279990', '', $item['albsub']);
                $title_component = $familia->getTitleComponent($title_component);
                $version_product = getCajon($item['albsub']);
                $color = $familia->getColoressByCocolo($item['albfami'], $item['albco'])[0]['codesc'];

                ?>

                <tr>
                    <td class="first_column text-left table_border_left table_border_right"><?= $title_component ?></td>
                    <td class="second_column table_border_right">-</td>
                    <td class="third_column text-center table_border_right"><?= $item['albunit'] ?></td>
                    <td class="four_column table_border_right">-</td>
                    <td class="five_column table_border_right">-</td>
                    <td class="six_column text-right table_border_right"><?= round2decimals($item['albpre']) ?>&nbsp;€
                    </td>
                </tr>

            <?php endif; ?>

            <?php //componente unidad
             if($item['albobs'] == "is_component_unit"):

                $title_component = str_replace('279990', '', $item['albsub']);
                $title_component = $familia->getTitleComponent($title_component);
                $version_product = getCajon($item['albsub']);
                $color = $familia->getColoressByCocolo($item['albfami'], $item['albco'])[0]['codesc'];

                ?>

                <tr>
                    <td class="first_column text-left table_border_left table_border_right"><?= $title_component ?></td>
                    <td class="second_column table_border_right">-</td>
                    <td class="third_column text-center table_border_right"><?= $item['albunit'] ?></td>
                    <td class="four_column table_border_right">-</td>
                    <td class="five_column table_border_right">-</td>
                    <td class="six_column text-right table_border_right"><?= round2decimals($item['albpre']) ?>&nbsp;€
                    </td>
                </tr>

            <?php endif;

            //lacado
            if($item['albobs'] == "lacado"):

                $color = "";
                $color = $item['albco'];
                $unidades = $item['albunit'];
                $precio = $item['albpre'];

                ?>

                <tr>
                    <td class="first_column text-left table_border_left table_border_right">Lacado -
                        RAL. <?= $version_product ?></td>
                    <td class="second_column text-left table_border_right"><?= $color ?></td>
                    <td class="third_column text-center table_border_right"><?= $unidades ?></td>
                    <td class="four_column text-left table_border_right">-</td>
                    <td class="five_column text-left table_border_right">-</td>
                    <td class="six_column text-right table_border_right"><?=$precio?>&nbsp;€</td>
                </tr>

            <?php endif;

        endforeach; ?>

        <?php

        for ($i = 0; $i < 30; $i++): ?>
            <tr>
                <td class="first_column table_border_left table_border_left table_border_right">&nbsp;</td>
                <td class="second_column table_border_left table_border_right">&nbsp;</td>
                <td class="third_column table_border_left table_border_right">&nbsp;</td>
                <td class="four_column table_border_left table_border_right">&nbsp;</td>
                <td class="five_column table_border_left table_border_right">&nbsp;</td>
                <td class="six_column table_border_right">&nbsp;</td>
            </tr>
        <?php endfor;

        ?>

        <tr>
            <td class="first_column table_border_bottom table_border_left table_border_left table_border_right"></td>
            <td class="second_column table_border_bottom table_border_left table_border_right"></td>
            <td class="third_column table_border_bottom table_border_left table_border_right"></td>
            <td class="four_column table_border_bottom table_border_left table_border_right"></td>
            <td class="five_column table_border_bottom table_border_left table_border_right"></td>
            <td class="six_column table_border_bottom table_border_right"></td>
        </tr>
    </table>
    <span class="info_firma">
        LA FIRMA DE ESTE DOCUMENTO IMPLICA LA ACEPTACIÓN DEL DETALLE TOTAL DEL PRODUCTO, FORMA DE PAGO E IMPORTE
    </span>
    <hr width="106.6%">
</div>
</body>
</html>

<!--        <table class="table_total">-->
<!--            <tr>-->
<!--                <td></td>-->
<!--                <td class="ft-7" colspan="2">I.V.A</td>-->
<!--                <td class="ft-7" colspan="2">RECARGO EQUIVALENCIA</td>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <td class="ft-7">BASE IMPONIBLE</td>-->
<!--                <td class="ft-7">%</td>-->
<!--                <td class="ft-7">CUOTA</td>-->
<!--                <td class="ft-7">%</td>-->
<!--                <td class="ft-7">CUOTA</td>-->
<!--            </tr>-->
<!--            <tr class="tr_background_total">-->
<!--                <td class="td_background_total">--><?//=$total_presupuesto?><!--</td>-->
<!--                <td class="td_background_total">21,0</td>-->
<!--                <td class="td_background_total">--><?//=$total_presupuesto_iva?><!--</td>-->
<!--                <td class="td_background_total">5,20</td>-->
<!--                <td class="td_background_total">--><?//=$total_presupuesto_recargo?><!--</td>-->
<!--            </tr>-->
<!--        </table>-->