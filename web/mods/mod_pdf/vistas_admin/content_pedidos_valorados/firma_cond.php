<span class="info_firma hidden <?= $class_hidden_pedido ?>">
    LA FIRMA DE ESTE DOCUMENTO IMPLICA LA ACEPTACIÓN DEL DETALLE TOTAL DEL PRODUCTO, FORMA DE PAGO E IMPORTE
</span>
<hr width="106.6%" class="<?= $class_hidden_pedido ?>">
<div class="content_precios <?= $class_hidden_pedido ?>">
    <div class="content_precios_line1 line">
        <div class="line1_1 line">I.V.A</div>
        <div class="line1_2 line">RECARGO EQUIVALENCIA</div>
    </div>
    <br>
    <div class="content_precios_line2 line">
        <div class="line2_1 line">BASE IMPONIBLE</div>
        <div class="line2_2 line">%</div>
        <div class="line2_3 line">CUOTA</div>
        <div class="line2_4 line">%</div>
        <div class="line2_5 line">CUOTA</div>
    </div>
    <br>
    <div class="content_precios_line3 line">
        <div class="line3_1 line text-center"><span><?= formato_dinero(round2decimals($total_presupuesto)) ?></span></div>
        <div class="line3_2 line text-center"><span><?=$texto_iva?></span></div>
        <div class="line3_3 line text-right"><span><?= formato_dinero(round2decimals($total_presupuesto_iva)) ?></span></div>
        <div class="line3_4 line text-center"><span><?=$texto_recargo?></span></div>
        <div class="line3_5 line text-right"><span><?= formato_dinero(round2decimals($total_presupuesto_recargo)) ?></span></div>
    </div>
    <div class="total_presupuesto">
        <span>TOTAL PEDIDO</span>
        <div class="border_total_presupuesto">
            <span><?= formato_dinero(round2decimals($total_presupuesto_impuestos)) ?>€</span>
        </div>
    </div>
</div>
- En relación a los gastos de envío se estará a lo dispuesto en las condiciones generales de venta de nuestras tarifas.
<?php if ($existe_expositor == 1): ?>
    
<?php endif; ?>  
</div>