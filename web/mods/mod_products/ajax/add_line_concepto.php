<?php

require '../../../../config.php';
$presupuesto = new Presupuesto();

$text_concepto = "";
if (isset($_POST['text_concepto'])): $text_concepto = $_POST['text_concepto'];
endif;
$tipo_concepto = "";
if (isset($_POST['tipo_concepto'])): $tipo_concepto = $_POST['tipo_concepto'];
endif;
$aumento_concepto = "";
if (isset($_POST['aumento_concepto'])): $aumento_concepto = $_POST['aumento_concepto'];
endif;
$valor_concepto = "";
if (isset($_POST['valor_concepto'])): $valor_concepto = $_POST['valor_concepto'];
endif;
$id_pres = "";
if (isset($_POST['id_pres'])): $id_pres = $_POST['id_pres'];
endif;

$insertar_pres = 0;
if (!empty($id_pres) && !empty($text_concepto) && !empty($valor_concepto)):
    $insertar_pres = $presupuesto->insertar_concepto($id_pres, $text_concepto, $tipo_concepto, $aumento_concepto, $valor_concepto);
endif;

if ($insertar_pres == 1):
    $linea_insertada = "<div class='col-md-12 col-xs-12 line_concepto'><div class='col-md-3 col-xs-3'>".$text_concepto."</div><div class='col-md-3 col-xs-3'>".$tipo_concepto."</div><div class='col-md-3 col-xs-3'>".$aumento_concepto."</div><div class='col-md-2 col-xs-2'>".$valor_concepto."</div></div>";
    $array_error = array('error' => '0', 'message' => 'Linea de concepto añadida correctamente', 'content' => $linea_insertada);
else:
    $array_error = array('error' => '-1', 'message' => 'Error al añadir concepto');
endif;

echo json_encode($array_error);

exit;
?>