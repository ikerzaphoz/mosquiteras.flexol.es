<?php

require '../../../../config.php';
$presupuesto = new Presupuesto();

$text_change_num_pres = "";
if (isset($_POST['text_change_num_pres'])):
    $text_change_num_pres = $_POST['text_change_num_pres'];
endif;
$id_pres = "";
if (isset($_POST['id_pres'])):
    $id_pres = $_POST['id_pres'];
endif;

$array_error = array('error' => '-2', 'message' => 'Error desconocido');

if (!empty($text_change_num_pres) && !empty($id_pres)):
    $affected = $presupuesto->set_num_personalizado($text_change_num_pres, $id_pres);

    if ($affected == 1):
        $array_error = array('error' => '0', 'message' => 'Guardado correctamente');
    elseif ($affected < 0):
        $array_error = array('error' => '-1', 'message' => 'Error al guardar');
    else:
        $array_error = array('error' => '1', 'message' => 'No se han encontrado cambios');
    endif;

endif;

echo json_encode($array_error);
