<?php
$array_components_by_color = $familia->getComponentsGroupsColor($id_component, $item['arsubf'], $satus_2 = '1');

foreach ($array_components_by_color as $item):

    if (!isset($id_pro)):
        switch ($item['arfami']):
            case '1791':
                $id_pro = "2731";
                break;
            case '1792':
                $id_pro = "2732";
                break;
            case '1793':
                $id_pro = "2733";
                break;
            case '1794':
                $id_pro = "2734";
                break;
            case '1795':
                $id_pro = "2734";
                break;
            case '1796':
                $id_pro = "2736";
                break;
        endswitch;
    endif;

    $arsubf = str_pad($item['arsubf'], 2, 0, STR_PAD_LEFT);
    $ararti = str_pad($item['ararti'], 6, 0, STR_PAD_LEFT);

    $embalaje = $familia->getEmbalaje($id_component, $arsubf, $ararti, $item['arprpr']);

    $max_measure = $familia->getMaxMeasure($id_component)[0]['max_measure'];

    $tipo_medida = $familia->get_tipoMedida_tarifa($id_component, $arsubf, $ararti);

    if (isset($precio_almacen)):
        $precio_almacen = round2decimals($item['arpval']);
    else:
        $precio_fabrica = round2decimals($item['arpvta']);
    endif;

    $referencia = $id_component . "-" . $arsubf . "-" . $ararti;

    $referencia_img = $id_component . $arsubf . $ararti;

    if ($referencia == "1792-01-000002"):
        $referencia_img = "179201000001";
    endif;
    if ($referencia == "1792-02-000002"):
        $referencia_img = "179202000001";
    endif;
    if ($referencia == "1792-03-000002"):
        $referencia_img = "179203000001";
    endif;
    if ($referencia == "1792-04-000002"):
        $referencia_img = "179204000001";
    endif;
    if ($referencia == "1794-02-000002"):
        $referencia_img = "179402000001";
    endif;
    if ($referencia == "1794-04-000002"):
        $referencia_img = "179404000001";
    endif;
    //Corredera
    if ($referencia == "1794-03-000002"):
        $referencia_img = "179403000001";
    endif;
    if ($referencia == "1794-05-000002"):
        $referencia_img = "179405000001";
    endif;
    if ($referencia == "1795-01-000002"):
        $referencia_img = "179501000001";
    endif;
    if ($referencia == "1795-02-000002"):
        $referencia_img = "179502000001";
    endif;
    if ($referencia == "1795-03-000002"):
        $referencia_img = "179503000001";
    endif;
    if ($referencia == "1795-04-000002"):
        $referencia_img = "179504000001";
    endif;
    if ($referencia == "1795-05-000002"):
        $referencia_img = "179505000001";
    endif;
    if ($referencia == "1795-06-000002"):
        $referencia_img = "179506000001";
    endif;


    if ($id_pro == "2735"):
        $imagen = path_image_components . "/2734/" . $referencia_img . ".jpg";
    else:
        $imagen = path_image_components . "/" . $id_pro . "/" . $referencia_img . ".jpg";
    endif;

    $class_hidden = "";
    if ($referencia == "1791-02-000001" || $referencia == "1791-11-000002" || $referencia == "1791-03-000001" || $referencia == "1791-04-000001"):
        $class_hidden = "hidden";
    endif;
    ?>

    <div class="fila_components_products col-md-12 col-xs-12 row text-center <?= $class_hidden ?>">
        <div class="border_components row">
            <div class="col-md-1 col-xs-1 padding-top-20 padding-left-0 padding-right-0">
                <?php if (isset($is_industrial) AND $is_industrial == 1): ?>
                    <input name="input_unit_components[]" step="<?= $embalaje ?>" type="number" value="0" min="0"
                           class="nopadding form-control input_unit_components">
                       <?php else: ?>
                    <input type="hidden" name="input_unit_components[]" class="input_unit_components" value="0">
                    <input type="button" class="btn btn-xs bg-border-info add_medidas_2" value="<?= lang_boton_add_unidades ?>">
                    <?php if (isset($precio_fabrica)): ?>
                        <input type="button" class="hidden btn btn-xs bg-border-info add_medidas_2"
                               value="<?= lang_boton_add_medidas ?>">
                           <?php endif; ?>
                       <?php endif; ?>
            </div>
            <div class="col-md-1 col-xs-1 padding-top-20 padding-left-0 padding-right-0">
                <?= getTipoMedida($tipo_medida) ?>
            </div>
            <div class="col-md-2 col-xs-2">
                <img alt="img_components" class="img_components 3" src="<?= $imagen ?>">
                <a class="image-popup-no-margins icono-lupa fa fa-search" href="<?= $imagen ?>">
                    <img alt="img_components" class="hidden 4" src="<?= $imagen ?>">
                </a>
            </div>
            <div class="col-md-2 col-xs-2 padding-top-20"><?= $referencia ?></div>
            <div class="col-md-3 col-xs-3 padding-top-20 text-left">
                <div class="col-md-10 col-xs-10"><?= hideColorListComponent($item['ardesc']) ?></div>
                <div class="col-md-2 col-xs-2">
                    <?php if ($group_color < 6 && ($group_color != '04' && $id_pro == "2731")): ?>
                        <?php if (empty($is_industrial)): ?>
                            <div class="change_color_component">
                                <img alt="change_color_component" class="img_color_component hidden" src="">
                            </div>
                        <?php endif; ?>
                        <span class="title_color_component"><?= lang_title_bruto ?></span>
                    <?php else: ?>
                        <?php if (empty($is_industrial) || !empty($is_industrial) && $id_pro == "2731" && ($group_color == '04' || $group_color == "12" || $group_color == "13")): ?>
                            <div class="change_color_component change_color_component_no_brut">
                                <img alt="change_color_component_no_brut" class="img_color_component hidden" src="">
                            </div>
                        <?php endif; ?>
                        <span class="title_color_component"><?= lang_title_lacado_blanco ?></span>
                    <?php endif; ?>
                </div>
            </div>
            <?php if (isset($precio_almacen)): ?>
                <div class="col-md-1 padding-top-20"><?= $embalaje ?></div>
                <div class="col-md-1 col-xs-1 padding-top-20 precio_component"><?= $precio_almacen ?></div>
                <input type="hidden" class="precio_almacen" name="precio_almacen[]" value="<?= $precio_almacen ?>">
                <input type="hidden" class="is_almacen" name="is_almacen[]" value="1">
                <?php 
                $arumiv = "1";
                if(isset($item['arumiv'])): $arumiv = $item['arumiv']; endif;
            ?>
            <input type="hidden" name="arumiv" class="arumiv" value="<?=$item['arumiv']?>">
            <?php else: ?>
                <div class="col-md-1 col-xs-1 padding-top-20 precio_component"><?= $precio_fabrica ?></div>
                <input type="hidden" class="precio_fabrica" name="precio_fabrica[]" value="<?= $precio_fabrica ?>">
                <input type="hidden" class="is_almacen" name="is_almacen[]" value="0">
            <?php endif; ?>

            <div class="col-md-1 col-xs-1 padding-top-20"><span class="total_component"></span></div>
            <div class="hidden col-xs-12 col-md-12 row content_lineas_medidas_componente text-left"></div>
        </div>
        <input type="hidden" class="id_component_product" name="id_component_product[]" value="<?= $id_component ?>">
        <input type="hidden" class="id_component" name="id_component[]" value="<?= $id_component ?>">
        <input type="hidden" class="ararti_first" name="ararti_first[]" value="<?= substr($ararti, 0, 2) ?>">

        <?php if ($group_color < 6): ?>
            <input type="hidden" class="components_color" name="components_color[]" value="<?= $ararti ?>">
            <?php
        else:
            //El color es lacado blanco
            ?>
            <input type="hidden" class="components_color" name="components_color[]" value="2">
        <?php endif; ?>
        <input type="hidden" class="input_id_product" name="input_id_product[]" value="<?= $id_pro ?>">
        <input type="hidden" class="arsubf" name="arsubf[]" value="<?= $arsubf ?>">
        <input type="hidden" class="ararti" name="ararti[]" value="<?= $ararti ?>">
        <input type="hidden" class="arprpr" name="arprpr[]" value="<?= $item['arprpr'] ?>">
        <input type="hidden" class="descripcion" name="descripcion[]" value="<?= $item['ardesc'] ?>">
        <input type="hidden" class="total_component_hidden" name="precio[]" value="">
        <input type="hidden" class="embalaje" name="embalaje" value="<?= $embalaje ?>">
        <?php if (isset($is_industrial) AND $is_industrial == 1): ?>
            <input type="hidden" class="type_medida_component" name="type_medida_component[]" value="3">

        <?php else: ?>
            <input type="hidden" class="type_medida_component" name="type_medida_component[]"
                   value="<?= $tipo_medida ?>">

        <?php endif; ?>
        <input type="hidden" class="total_units_metros" name="total_units_metros" value="">
        <input type="hidden" name="data_component[]" class="data_component" value="">
        <input type="hidden" name="max_measure" class="max_measure" value="<?= $max_measure ?>">
    </div>

    <?php
endforeach;

$group_color = $arsubf;
?>