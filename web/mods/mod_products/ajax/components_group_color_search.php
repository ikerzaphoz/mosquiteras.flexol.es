<?php

$array_components_by_color = $familia->getComponentsGroupsColor($id_component, $item['arsubf']);
//Busqueda por codigo y solamente encuentra un resultado, evitando asi que no se muestre la fotografia o agrupado por color
if($type_search == "codigo" AND count($array_components) == 1):
    $array_components_by_color = $familia->searchComponentByCode($id_component, $arsubf, $ararti);
endif;

foreach ($array_components_by_color as $item):

    if (!isset($id_pro)):
        if ($item['arfami'] == "1791"):
            $id_pro = "2731";
        endif;
    endif;

    $embalaje = $familia->getEmbalaje($id_component, $item['arsubf'], $item['ararti'], $item['arprpr']);

    $max_measure = $familia->getMaxMeasure($id_component)[0]['max_measure'];

    $tipo_medida = $familia->get_tipoMedida_tarifa($id_component, $item['arsubf'], $item['ararti']);

    if (isset($precio_almacen)):
        $precio_almacen = round2decimals($item['arpval']);
    else:
        $precio_fabrica = round2decimals($item['arpvta']);
    endif;

    $referencia_img = $id_component . $item['arsubf'] . $item['ararti'] . "_";

    $referencia = $id_component . "-" . str_pad($item['arsubf'], 2, 0, STR_PAD_LEFT) . "-" . str_pad($item['ararti'], 6, 0, STR_PAD_LEFT);

    $imagen = "";
    $imagen = path_image_components . "/" . $referencia_img . ".jpg";

    ?>

    <div class="fila_components_products col-md-12 col-xs-12 row text-center">
        <div class="border_components row">
            <div class="col-md-1 col-xs-1 padding-top-20 padding-left-0 padding-right-0">
                <?php if (isset($is_industrial) AND $is_industrial == 1): ?>
                    <input name="input_unit_components[]" step="<?= $embalaje ?>" type="number" value="0" min="0"
                           class="nopadding form-control input_unit_components">
                <?php else: ?>
                    <input type="hidden" name="input_unit_components[]" class="input_unit_components" value="0">
                    <input type="button" class="btn btn-xs bg-border-info add_medidas_2" value="<?=lang_boton_add_unidades?>">
                    <?php if (isset($precio_fabrica)): ?>
                        <input type="button" class="hidden btn btn-xs bg-border-info add_medidas_2"
                               value="Añadir medidas">
                    <?php endif; ?>
                <?php endif; ?>
            </div>
            <div class="col-md-1 col-xs-1 padding-top-20 padding-left-0 padding-right-0">
                <?= getTipoMedida($tipo_medida) ?>
            </div>
            <div class="col-md-2 col-xs-2">
                <img class="img_components" src="<?= $imagen ?>">
                <a class="image-popup-no-margins icono-lupa fa fa-search" href="<?= $imagen ?>">
                    <img class="hidden" src="<?= $imagen ?>">
                </a>
            </div>
            <div class="col-md-2 col-xs-2 padding-top-20"><?= $referencia ?></div>
            <div class="col-md-3 col-xs-3 padding-top-20 text-left">
                <div class="col-md-10 col-xs-10"><?= hideColorListComponent($item['ardesc']) ?></div>
                <div class="col-md-2 col-xs-2">
                    <?php if ($group_color < 6): ?>
                        <?php if (empty($is_industrial)): ?>
                            <div class="change_color_component">
                                <img class="img_color_component hidden" src="">
                            </div>
                        <?php endif; ?>
                        <span class="title_color_component"><?=lang_title_bruto?></span>
                    <?php else: ?>
                        <?php if (empty($is_industrial)): ?>
                            <div class="change_color_component change_color_component_no_brut">
                                <img class="img_color_component hidden" src="">
                            </div>
                        <?php endif; ?>
                        <span class="title_color_component"><?=lang_title_lacado_blanco?></span>
                    <?php endif; ?>
                </div>
            </div>
            <?php if (isset($precio_almacen)): ?>
                <div class="col-md-1 padding-top-20"><?= $embalaje ?></div>
                <div class="col-md-1 col-xs-1 padding-top-20 precio_component"><?= $precio_almacen ?></div>
                <input type="hidden" class="precio_almacen" name="precio_almacen[]" value="<?= $precio_almacen ?>">
                <input type="hidden" class="is_almacen" name="is_almacen[]" value="1">
            <?php else: ?>
                <div class="col-md-1 col-xs-1 padding-top-20 precio_component"><?= $precio_fabrica ?></div>
                <input type="hidden" class="precio_fabrica" name="precio_fabrica[]" value="<?= $precio_fabrica ?>">
                <input type="hidden" class="is_almacen" name="is_almacen[]" value="0">
            <?php endif; ?>

            <div class="col-md-1 col-xs-1 padding-top-20"><span class="total_component"></span></div>
            <div class="hidden col-xs-12 col-md-12 row content_lineas_medidas_componente text-left"></div>
        </div>
        <input type="hidden" class="id_component_product" name="id_component_product[]" value="<?= $id_component ?>">
        <input type="hidden" class="id_component" name="id_component[]" value="<?= $id_component ?>">
        <input type="hidden" class="ararti_first" name="ararti_first[]" value="<?= substr($item['ararti'], 0, 2) ?>">

        <?php if ($group_color < 6): ?>
            <input type="hidden" class="components_color" name="components_color[]" value="<?= $item['ararti'] ?>">
        <?php else:
            //El color es lacado blanco
            ?>
            <input type="hidden" class="components_color" name="components_color[]" value="2">
        <?php endif; ?>
        <input type="hidden" class="input_id_product" name="input_id_product[]" value="<?= $id_pro ?>">
        <input type="hidden" class="arsubf" name="arsubf[]" value="<?= $item['arsubf'] ?>">
        <input type="hidden" class="ararti" name="ararti[]" value="<?= $item['ararti'] ?>">
        <input type="hidden" class="arprpr" name="arprpr[]" value="<?= $item['arprpr'] ?>">
        <input type="hidden" class="descripcion" name="descripcion[]" value="<?= $item['ardesc'] ?>">
        <input type="hidden" class="total_component_hidden" name="precio[]" value="">
        <input type="hidden" class="embalaje" name="embalaje" value="<?= $embalaje ?>">
        <?php if (isset($is_industrial) AND $is_industrial == 1): ?>
            <input type="hidden" class="type_medida_component" name="type_medida_component[]" value="3">

        <?php else: ?>
            <input type="hidden" class="type_medida_component" name="type_medida_component[]"
                   value="<?= $tipo_medida ?>">

        <?php endif; ?>
        <input type="hidden" class="total_units_metros" name="total_units_metros" value="">
        <input type="hidden" name="data_component[]" class="data_component" value="">
        <input type="hidden" name="max_measure" class="max_measure" value="<?= $max_measure ?>">
    </div>

<?php endforeach;

$group_color = $item['arsubf'];

?>