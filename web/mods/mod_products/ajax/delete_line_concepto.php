<?php

require '../../../../config.php';
$presupuesto = new Presupuesto();

$id_linea_concepto = "";
if($_POST['id_linea_concepto']) $id_linea_concepto = $_POST['id_linea_concepto'];
$id_pres = "";
if($_POST['id_pres_modal']) $id_pres = $_POST['id_pres_modal'];

$eliminar_linea = $presupuesto->eliminar_concepto($id_pres, $id_linea_concepto);

if ($eliminar_linea == 1):
    $array_error = array('error' => '0', 'mensaje' => 'Linea de concepto eliminada correctamente');
else:
    $array_error = array('error' => '-1', 'mensaje' => 'Error al eliminar concepto');
endif;

echo json_encode($array_error);

exit;