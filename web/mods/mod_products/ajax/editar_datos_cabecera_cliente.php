<?php

require '../../../../config.php';
$presupuesto = new Presupuesto();

$text_change_num_pres = "";
if (isset($_POST['text_change_num_pres'])):
    $text_change_num_pres = $_POST['text_change_num_pres'];
endif;
$id_pres = "";
if (isset($_POST['id_pres'])):
    $id_pres = $_POST['id_pres'];
endif;
$text_area_datos_cliente = "";
if (isset($_POST['text_area_datos_cliente'])):
    $text_area_datos_cliente = $_POST['text_area_datos_cliente'];
endif;

$array_error = array('error' => '-2', 'message' => 'Error desconocido');

if (!empty($text_change_num_pres) && !empty($id_pres)):
    $affected = $presupuesto->set_cabecera_personalizada($id_pres, $text_area_datos_cliente, $text_change_num_pres);

    if ($affected == 1):
        $array_error = array('error' => '0', 'message' => 'Guardado correctamente');
    elseif ($affected < 0):
        $array_error = array('error' => '-1', 'message' => 'Error al guardar');
    else:
        $array_error = array('error' => '1', 'message' => 'No se han encontrado cambios');
    endif;

endif;

echo json_encode($array_error);
