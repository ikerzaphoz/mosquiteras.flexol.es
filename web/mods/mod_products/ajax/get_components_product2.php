<?php

require '../../../../config.php';

$id_pro = "";
if(isset($_POST['id_product'])): $id_pro = $_POST['id_product']; endif;

if($id_pro == "2731"):
    $id_component = "1791";
endif;

$familia = new Familia();
$array_components = $familia->getComponentsBySubfamily($id_component); ?>

<div class="panel panel-default form-inline col-md-12 col-xs-12 nopadding">
    <div class="panel-heading selected">
        <h4 class="panel-title">
            <div class="title_panel_title title_versiones">Componentes</div>
        </h4>
    </div>

    <div class="col-md-12 col-xs-12">
        <form class="search_components" name="search_components">
            <div class="form-group has-feedback div_content_search_components hidden">
                <div class="panel-collapse nopadding">
                    <div class="col-md-6 col-xs-6 text-left mt-15">
                        <div class="col-md-6 col-xs-6">
                            <input checked type="radio" name="type_search_components" class="type_search_components" value="descripcion"><label class="font-light check_search_components">&nbsp;&nbsp;&nbsp;Buscar descripción</label>
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <input type="radio" name="type_search_components" class="type_search_components" value="codigo"><label class="font-light check_search_components">&nbsp;&nbsp;&nbsp;Buscar código</label>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <input type="search" class="mt-10 mb-10 form-control btn_search_component" name="search_components" placeholder="Introduce búsqueda...">
                        <i class="glyphicon glyphicon-search icono_lupa_search_components form-control-feedback"></i>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="col-md-1 col-xs-1 row">
        <div class="boton_componentes_flotante text-center hidden">
            <button type="submit" value="presupuesto" name="type_button" class="btn-flotante btn_flotante_generar btn bg-border-info"><span>Continuar Presupuesto</span></button><br>
            <button type="submit" value="pedido" class="btn-flotante btn_flotante_generar btn bg-border-warning">Continuar Pedido</button>
        </div>
    </div>

    <div class="col-md-12 col-xs-12 row text-center head_components">
        <div class="col-md-1">Unidades</div>
        <div class="col-md-2">Artículo</div>
        <div class="col-md-2">Referencia</div>
        <div class="col-md-3">Descripción</div>
        <div class="col-md-1">Embalaje <br>estándar Ud./m<sup>2.</sup>/m</div>
        <div class="col-md-2">Precio (€) <br>Ud./m<sup>2</sup>./m</div>
        <div class="col-md-1">PRECIO Total (€)</div>
    </div>

    <div class="body_components">

    <?php

    $group_color = 0;

    foreach($array_components as $item):

        $embalaje = $familia->getEmbalaje($id_component, $item['arsubf'], $item['ararti'], $item['arprpr']);

        $referencia_img = $id_component.$item['arsubf'].$item['ararti'];

        if($referencia_img == "179112" OR $referencia_img == "179113"):
            $referencia_img = "179111";
        endif;

        $referencia = $id_component."-".str_pad($item['arsubf'], 2, 0, STR_PAD_LEFT)."-".str_pad($item['ararti'], 6, 0, STR_PAD_LEFT);

        $imagen = "";
        if($item['arsubf'] == 1 AND ($item['ararti'] == "000001" OR $item['ararti'] == "000002" OR $item['ararti'] == "000003")):
            $imagen = path_image_components."/179111.jpg";
        elseif($item['arsubf'] == 1 AND ($item['ararti'] == "000101" OR $item['ararti'] == "000102" OR $item['ararti'] == "000103" OR $item['ararti'] == "000104" OR $item['ararti'] == "000105")):
            $imagen = path_image_components."/17911101.jpg";
        else:
            $imagen = path_image_components."/".$referencia_img.".jpg";
        endif;

        if($item['arsubf'] == 2):
            if($group_color == 0):
                include 'components_group_color.php';
            endif;
        elseif($item['arsubf'] == 3):
            if($group_color == 2):
                include 'components_group_color.php';
            endif;
        elseif($item['arsubf'] == 4):
            if($group_color == 3):
                include 'components_group_color.php';
            endif;
        elseif($item['arsubf'] == 5):
            if($group_color == 4):
                include 'components_group_color.php';
            endif;
        elseif($item['arsubf'] == 6):
            if($group_color == 5):
                include 'components_group_color.php';
            endif;
        elseif($item['arsubf'] == 11):
            if($group_color == 6):
                include 'components_group_color.php';
            endif;
        elseif($item['arsubf'] == 12):
            if($group_color == 11):
                include 'components_group_color.php';
            endif;
        elseif($item['arsubf'] == 13):
            if($group_color == 12):
                include 'components_group_color.php';
            endif;
        elseif($item['arsubf'] == 21):
            if($group_color == 13):
                include 'components_group_color.php';
            endif;
        else:

        ?>

        <div class="fila_components_products col-md-12 col-xs-12 row text-center">
            <div class="border_components row">
                <div class="col-md-1 padding-top-20 padding-left-0 padding-right-0">
                    <input name="input_unit_components[]" type="number" value="0" min="0" class="nopadding form-control input_unit_components">
                </div>
                <div class="col-md-2">
                    <img class="img_components" src="<?=$imagen?>">
                    <a class="image-popup-no-margins icono-lupa fa fa-search" href="<?=$imagen?>">
                        <img class="hidden" src="<?=$imagen?>">
                    </a>
                </div>
                <div class="col-md-2 padding-top-20"><?=$referencia?></div>
                <div class="col-md-3 padding-top-20 text-left"><?=hideColorListComponent($item['ardesc'])?></div>
                <div class="col-md-1 padding-top-20"><?=$embalaje?></div>
                <div class="col-md-2 padding-top-10"><span class="span_almacen"></span><br><span class="span_unitario"></span></div>
                <div class="col-md-1 padding-top-20"><span class="total_component"></span></div>
                <input type="hidden" class="id_component" name="id_component[]" value="<?=$id_component?>">
                <input type="hidden" class="arsubf" name="arsubf[]" value="<?=$item['arsubf']?>">
                <input type="hidden" class="ararti" name="ararti[]" value="<?=$item['ararti']?>">
                <input type="hidden" class="arprpr" name="arprpr[]" value="<?=$item['arprpr']?>">
                <input type="hidden" class="descripcion" name="descripcion[]" value="<?=$item['ardesc']?>">
                <input type="hidden" class="total_component_hidden" name="precio[]" value="">
                <input type="hidden" class="type_button_hidden" name="button_component" value="">
                <input type="hidden" class="arumiv" value="1">
            </div>
        </div>

    <?php

    endif;

    endforeach; ?>

    </div>

</div>

<input type="hidden" name="id_component_product[]" class="id_component_product" value="<?=$id_component?>">
