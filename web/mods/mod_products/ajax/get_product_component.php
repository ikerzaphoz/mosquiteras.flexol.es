<?php

require '../../../../config.php';
$familia = new Familia();

$unit_components = "";
if(isset($_POST['unit_components'])) $unit_components = $_POST['unit_components'];
$id_component = "";
if(isset($_POST['id_component'])) $id_component = $_POST['id_component'];
$arsubf = "";
if(isset($_POST['arsubf'])) $arsubf = $_POST['arsubf'];
$ararti = "";
if(isset($_POST['ararti'])) $ararti = $_POST['ararti'];
$arprpr = "";
if(isset($_POST['arprpr'])) $arprpr = $_POST['arprpr'];

$embalaje = $familia->getEmbalaje($id_component, $arsubf, $ararti, $arprpr);
$array_price_articulo = $familia->getInfoComponent($id_component, $arsubf, $ararti, $arprpr);
$precio_almacen = $array_price_articulo['arpval'];
$precio_unitario = $array_price_articulo['arpvta'];
$precio_total = 0;
//Unidades a precio embalaje
if($unit_components > 0 AND $unit_components >= $embalaje AND $unit_components % $embalaje == 0){
    echo "múltiplo";
    $precio_total = $unit_components * $precio_almacen;
}else{
    //Si el numero de unidades es mayor que componentes pero no es multiplo restar las unidades y calcular de forma individual.
    echo "No es múltiplo";
}

echo "Precio total->".$precio_total;