<div class="panel-body text-center nopadding">
    <div class="col-md-12 col-xs-12 nopadding">
        <input type="hidden" name="id_product" value="exp">
        <div class="radio col-md-6 col-xs-6">
            <div class="col-md-6 col-xs-6 pd4">
                <div class="col-md-12 col-xs-12 contenido_expositor div_title_expositores expositores_ventana">
                    <div class="col-md-8 col-xs-8">
                        <strong><span class="text ft9em">Enr. Ventana Cajón 35</span></strong>
                    </div>
                    <div class="col-md-4 col-xs-4 div_unidades_expositores">
                        <input class="unidades_expositores_modal" type="number" value="0" min="0">
                        <span class="text_uds">Uds.</span>
                        <input type="hidden" class="precio_expo hidden" value="75"/>
                        <input type="hidden" class="total_line">
                        <input class="info_submit" type="hidden" name="enr_ventana_cajon_35" value="">
                    </div>
                </div>
                <div class="col-md- col-xs-12 div_title_expositores contenido_expositor expositores_ventana">
                    <div class="col-md-8 col-xs-8">
                        <strong><span class="text ft9em">Enr. Ventana Cajón 42</span></strong>
                    </div>
                    <div class="col-md-4 col-xs-4 div_unidades_expositores">
                        <input class="unidades_expositores_modal" type="number" value="0" min="0">
                        <span class="text_uds">Uds.</span>
                        <input type="hidden" class="precio_expo hidden" value="75"/>
                        <input type="hidden" class="total_line">
                        <input class="info_submit" type="hidden" name="enr_ventana_cajon_42" value="">
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 contenido_expositor_border">
                    <div class="col-md-12 col-xs-12 div_img_expositores">
                        <img alt="img_expositor" class="img_expositor"
                             src="<?= path_image ?>images_product/expositores/enrollable_ventana.png">
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <a class="hidden image-popup-no-margins icono-lupa fa fa-search"
                           href="<?= path_image ?>images_product/expositores/enrollable_ventana.png">
                            <img alt="img_expositor" class="hidden"
                                 src="<?= path_image ?>images_product/expositores/enrollable_ventana.png">
                        </a>
                        <span class="hidden text_lupa">Ver imagen</span>
                        <div class="col-md-12 price_expositor_green">
                            <span style='font-size: 1.2em; color: #37af04;font-weight: bolder;'>75€</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-6 contenido_expositor pd4">
                <div class="col-md-12 col-xs-12 div_title_expositores">
                    <div class="col-md-8 col-xs-8">
                        <strong><span class="text ft9em">Enrollable Puerta</span></strong>
                    </div>
                    <div class="col-md-4 col-xs-4 div_unidades_expositores">
                        <input class="unidades_expositores_modal" type="number" value="0" min="0">
                        <span class="text_uds">Uds.</span>
                        <input type="hidden" class="precio_expo hidden" value="150"/>
                        <input type="hidden" class="total_line">
                        <input class="info_submit" type="hidden" name="enr_puerta" value="">
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 contenido_expositor_border">
                    <div class="col-md-12 col-xs-12 div_img_expositores">
                        <img alt="img_expositor" class="img_expositor"
                             src="<?= path_image ?>images_product/expositores/enrollable_puerta.png">
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <a class="hidden image-popup-no-margins icono-lupa fa fa-search"
                           href="<?= path_image ?>images_product/expositores/enrollable_puerta.png">
                            <img alt="img_expositor" class="hidden"
                                 src="<?= path_image ?>images_product/expositores/enrollable_puerta.png">
                        </a>
                        <span class="hidden text_lupa">Ver imagen</span>
                        <div class="col-md-12 price_expositor_green">
                            <span style='font-size: 1.2em; color: #37af04;font-weight: bolder;'>150€</span>
                        </div>
                    </div> 
                </div>
            </div>
            <div class="col-md-6 col-xs-6 contenido_expositor mt-3 pd4">
                <div class="col-md-12 col-xs-12 div_title_expositores">
                    <div class="col-md-8 col-xs-8">
                        <strong><span class="text ft9em">Plisada de 40<br>Puerta</span></strong>
                    </div>
                    <div class="col-md-4 col-xs-4 div_unidades_expositores">
                        <input class="unidades_expositores_modal" type="number" value="0" min="0">
                        <span class="text_uds">Uds.</span>
                        <input type="hidden" class="precio_expo hidden" value="150"/>
                        <input type="hidden" class="total_line">
                        <input class="info_submit" type="hidden" name="plisada_40" value="">
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 contenido_expositor_border">
                    <div class="col-md-12 col-xs-12 div_img_expositores">
                        <img alt="img_expositor" class="img_expositor"
                             src="<?= path_image ?>images_product/expositores/plisada.png">
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <a class="hidden image-popup-no-margins icono-lupa fa fa-search"
                           href="<?= path_image ?>images_product/expositores/plisada.png">
                            <img alt="img_expositor" class="hidden"
                                 src="<?= path_image ?>images_product/expositores/plisada.png">
                        </a>
                        <span class="hidden text_lupa">Ver imagen</span>
                        <div class="col-md-12 price_expositor_green">
                            <span style='font-size: 1.2em; color: #37af04;font-weight: bolder;'>150€</span>
                        </div>
                    </div> 
                </div>
            </div>
            <div class="col-md-6 col-xs-6 mt-3 contenido_expositor pd4">
                <div class="col-md-12 col-xs-12 div_title_expositores">
                    <div class="col-md-8 col-xs-8">
                        <strong><span class="text ft9em">Abatibles</span></strong>
                    </div>
                    <div class="col-md-4 col-xs-4 div_unidades_expositores">
                        <input class="unidades_expositores_modal" type="number" value="0" min="0">
                        <span class="text_uds">Uds.</span>
                        <input type="hidden" class="precio_expo hidden" value="150"/>
                        <input type="hidden" class="total_line">
                        <input class="info_submit" type="hidden" name="exp_abatible" value="">
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 contenido_expositor_border">

                    <div class="col-md-12 col-xs-12 div_img_expositores">
                        <img alt="img_expositor" class="img_expositor"
                             src="<?= path_image ?>images_product/expositores/abatible.png">
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <a class="hidden image-popup-no-margins icono-lupa fa fa-search"
                           href="<?= path_image ?>images_product/expositores/abatible.png">
                            <img alt="img_expositor" class="hidden" src="<?= path_image ?>images_product/expositores/abatible.png">
                        </a>
                        <span class="hidden text_lupa">Ver imagen</span>
                        <div class="col-md-12 price_expositor_green">
                            <span style='font-size: 1.2em; color: #37af04;font-weight: bolder;'>150€</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-6 contenido_expositor mt-3 pd4">
                <div class="col-md-12 col-xs-12 div_title_expositores">
                    <div class="col-md-8 col-xs-8">
                        <strong><span class="text ft9em">Plisada de 22<br>Puerta Lateral</span></strong>
                    </div>
                    <div class="col-md-4 col-xs-4 div_unidades_expositores">
                        <input class="unidades_expositores_modal" type="number" value="0" min="0">
                        <span class="text_uds">Uds.</span>
                        <input type="hidden" class="precio_expo hidden" value="150"/>
                        <input type="hidden" class="total_line">
                        <input class="info_submit" type="hidden" name="plisada_lateral" value="">
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 contenido_expositor_border">
                    <div class="col-md-12 col-xs-12 div_img_expositores">
                        <img alt="img_expositor" class="img_expositor"
                             src="<?= path_image ?>images_product/expositores/plisada_22_lateral.png">
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <a class="hidden image-popup-no-margins icono-lupa fa fa-search"
                           href="<?= path_image ?>images_product/expositores/plisada_22_lateral.png">
                            <img alt="img_expositor" class="hidden"
                                 src="<?= path_image ?>images_product/expositores/plisada_22_lateral.png">
                        </a>
                        <span class="hidden text_lupa">Ver imagen</span>
                        <div class="col-md-12 price_expositor_green">
                            <span style='font-size: 1.2em; color: #37af04;font-weight: bolder;'>150€</span>
                        </div>
                    </div> 
                </div>
            </div>
            <div class="hidden col-md-6 col-xs-6 contenido_expositor mt-3 pd4">
                <div class="col-md-12 col-xs-12 div_title_expositores">
                    <div class="col-md-8 col-xs-8">
                        <strong><span class="text ft9em">Plisada Puerta Reversible 22</span></strong>
                    </div>
                    <div class="col-md-4 col-xs-4 div_unidades_expositores">
                        <input class="unidades_expositores_modal" type="number" value="0" min="0">
                        <span class="text_uds">Uds.</span>
                        <input type="hidden" class="precio_expo hidden" value="150"/>
                        <input type="hidden" class="total_line">
                        <input class="info_submit" type="hidden" name="plisada_reversible" value="">
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 contenido_expositor_border">
                    <div class="col-md-12 col-xs-12 div_img_expositores">
                        <img alt="img_expositor" class="img_expositor"
                             src="<?= path_image ?>images_product/expositores/plisada_22_lateral.png">
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <a class="hidden image-popup-no-margins icono-lupa fa fa-search"
                           href="<?= path_image ?>images_product/expositores/plisada_22_lateral.png">
                            <img alt="img_expositor" class="hidden"
                                 src="<?= path_image ?>images_product/expositores/plisada_22_lateral.png">
                        </a>
                        <span class="hidden text_lupa">Ver imagen</span>
                        <div class="col-md-12 price_expositor_green">
                            <span style='font-size: 1.2em; color: #37af04;font-weight: bolder;'>150€</span>
                        </div>
                    </div> 
                </div>
            </div>
            <div class="col-md-6 col-xs-6 contenido_expositor mt-3 pd4">
                <div class="col-md-12 col-xs-12 div_title_expositores">
                    <div class="col-md-8 col-xs-8">
                        <strong><span class="text ft9em">Plisada de 22<br>Ventana</span></strong>
                    </div>
                    <div class="col-md-4 col-xs-4 div_unidades_expositores">
                        <input class="unidades_expositores_modal" type="number" value="0" min="0">
                        <span class="text_uds">Uds.</span>
                        <input type="hidden" class="precio_expo hidden" value="150"/>
                        <input type="hidden" class="total_line">
                        <input class="info_submit" type="hidden" name="plisada_ventana" value="">
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 contenido_expositor_border">
                    <div class="col-md-12 col-xs-12 div_img_expositores">
                        <img alt="img_expositor" class="img_expositor"
                             src="<?= path_image ?>images_product/expositores/plisada_22_ventana.png">
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <a class="hidden image-popup-no-margins icono-lupa fa fa-search"
                           href="<?= path_image ?>images_product/expositores/plisada_22_ventana.png">
                            <img alt="img_expositor" class="hidden"
                                 src="<?= path_image ?>images_product/expositores/plisada_22_ventana.png">
                        </a>
                        <span class="hidden text_lupa">Ver imagen</span>
                        <div class="col-md-12 price_expositor_green">
                            <span style='font-size: 1.2em; color: #37af04;font-weight: bolder;'>150€</span>
                        </div>
                    </div> 
                </div>
            </div>
            <div class="hidden col-md-6 col-xs-6 mt-3 contenido_expositor pd4">
                <div class="col-md-12 col-xs-12 div_title_expositores">
                    <div class="col-md-8 col-xs-8">
                        <strong><span class="text ft9em">Fijas y Correderas</span></strong>
                    </div>
                    <div class="col-md-4 col-xs-4 div_unidades_expositores">
                        <input class="unidades_expositores_modal" type="number" value="0" min="0">
                        <span class="text_uds">Uds.</span>
                        <input type="hidden" class="precio_expo hidden" value="75"/>
                        <input type="hidden" class="total_line">
                        <input class="info_submit" type="hidden" name="exp_fija_corredera" value="">
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 contenido_expositor_border">

                    <div class="col-md-12 col-xs-12 div_img_expositores">
                        <img alt="img_expositor" class="img_expositor"
                             src="<?= path_image ?>images_product/expositores/fija_corredera.png">
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <a class="hidden image-popup-no-margins icono-lupa fa fa-search"
                           href="<?= path_image ?>images_product/expositores/fija_corredera.png">
                            <img alt="img_expositor" class="hidden"
                                 src="<?= path_image ?>images_product/expositores/fija_corredera.png">
                        </a>
                        <span class="hidden text_lupa">Ver imagen</span>
                        <div class="col-md-12 price_expositor_green">
                            <span style='font-size: 1.2em; color: #37af04;font-weight: bolder;'>150€</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hidden col-md-6 col-xs-6 mt-3 contenido_expositor pd4">
                <div class="col-md-12 col-xs-12 div_title_expositores">
                    <div class="col-md-8 col-xs-8">
                        <strong><span class="text ft9em">Selector de Color</span></strong>
                    </div>
                    <div class="col-md-4 col-xs-4 div_unidades_expositores">
                        <input class="unidades_expositores_modal" type="number" value="0" min="0">
                        <span class="text_uds">Uds.</span>
                        <input type="hidden" class="precio_expo hidden" value="10"/>
                        <input type="hidden" class="total_line">
                        <input class="info_submit" type="hidden" name="exp_selector_color" value="">
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 contenido_expositor_border">

                    <div class="col-md-12 col-xs-12 div_img_expositores">
                        <img alt="img_expositor" class="img_expositor"
                             src="<?= path_image ?>images_product/expositores/selector.png">
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <a class="hidden image-popup-no-margins icono-lupa fa fa-search"
                           href="<?= path_image ?>images_product/expositores/selector.png">
                            <img alt="img_expositor" class="hidden"
                                 src="<?= path_image ?>images_product/expositores/selector.png">
                        </a>
                        <span class="hidden text_lupa">Ver imagen</span>
                        <div class="col-md-12 price_expositor_green">
                            <span style='font-size: 1.2em; color: #37af04;font-weight: bolder;'>10€</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="radio col-md-6 col-xs-6 div_imagen_expositor contenido_expositor pd4">
            <div class="col-md-12 col-xs-12 div_title_expositores">
                <div class='col-md-8 col-xs-8'>
                    <strong><span class="text">Expositor Personalizable</span></strong>
                </div>
                <div class="col-md-4 col-xs-4 div_unidades_expositores">
                    <input class="unidades_expositores_modal" type="number" value="0" min="0">
                    <span class="text_uds">Uds.</span>
                    <input type="hidden" class="precio_expo hidden" value="580"/>
                    <input type="hidden" class="total_line">
                    <input class="info_submit" type="hidden" name="exp_fija_expositor" value="">
                </div>
            </div>
            <div class="col-md-12 col-xs-12 contenido_expositor_border">

                <div class="col-md-12 col-xs-12 div_img_expositores">
                    <img alt="img_expositor" style="height: 700px;" src="<?= path_image ?>images_product/expositores/expositor.png">
                </div>
                <div>
                    <a class="hidden image-popup-no-margins icono-lupa fa fa-search"
                       href="<?= path_image ?>images_product/expositores/expositor.png">
                        <img alt="img_expositor" class="hidden" src="<?= path_image ?>images_product/expositores/expositor.png">
                    </a>
                    <span class="hidden text_lupa">Ver imagen</span>
                    <div class="col-md-12 price_expositor_green">
                        <span style='font-size: 1.2em; color: #37af04;font-weight: bolder;'>590€</span>
                    </div>
                </div>
            </div>
                                <div style="padding: 0;" class="col-md-6 col-xs-6 mt-3 contenido_expositor">
                        <div class="col-md-12 col-xs-12 div_title_expositores">
                            <div class="col-md-7 col-xs-7">
                                <strong><span class="text">Selector de Color</span></strong>
                            </div>
                            <div class="col-md-5 col-xs-5 div_unidades_expositores">
                                <input class="unidades_expositores_modal" type="number" value="0" min="0">
                                <span class="text_uds">Uds.</span>
                                <input type="hidden" class="precio_expo hidden" value="10"/>
                                <input type="hidden" class="total_line">
                                <input class="info_submit" type="hidden" name="exp_selector_color" value="">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 contenido_expositor_border" style="padding-top: 45px">

                            <div class="col-md-12 col-xs-12 div_img_expositores" style="padding: 0 !important;">
                                <img alt="img_expositor" class="img_expositor" style="height: 200px"
                                     src="<?= path_image ?>images_product/expositores/selector.png">
                            </div>
                            <div class="col-md-12 col-xs-12" style="padding-top: 18px">
                                <a class="image-popup-no-margins icono-lupa fa fa-search"
                                   href="<?= path_image ?>images_product/expositores/selector.png">
                                    <img alt="img_expositor" class="hidden"
                                         src="<?= path_image ?>images_product/expositores/selector.png">
                                </a>
                                <span class="text_lupa"><?= lang_title_ver_imagen ?></span>
                                <div class="col-md-12 price_expositor_green">
                                    <span style='font-size: 1.2em; color: #37af04;font-weight: bolder;'>10€</span>
                                </div>
                            </div>                    
                        </div>
                    </div>
                    <div style="padding: 0;" class="col-md-1 col-xs-1 mt-3 contenido_expositor"></div>
                    <div style="padding: 0;" class="col-md-5 col-xs-5 mt-3 contenido_expositor">
                        <div class="col-md-12 col-xs-12 div_title_expositores">
                            <div class="col-md-7 col-xs-7">
                                <strong><span class="text">Busca tornillos</span></strong>
                            </div>
                            <div class="col-md-5 col-xs-5 div_unidades_expositores">
                                <input class="unidades_expositores_modal" type="number" value="0" min="0">
                                <span class="text_uds">Uds.</span>
                                <input type="hidden" class="precio_expo hidden" value="1"/>
                                <input type="hidden" class="total_line">
                                <input class="info_submit" type="hidden" name="exp_buscatornillos" value="">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 contenido_expositor_border">

                            <div class="col-md-12 col-xs-12 div_img_expositores" style="padding-top:90px;">
                                <img alt="img_expositor" class="img_expositor" style="height: 130px;"
                                     src="<?= path_image ?>images_product/expositores/buscatornillos.png">
                            </div>
                            <div class="col-md-12 col-xs-12" style="padding-top: 35px;">
                                <a class="image-popup-no-margins icono-lupa fa fa-search"
                                   href="<?= path_image ?>images_product/expositores/buscatornillos.png">
                                    <img alt="img_expositor" class="hidden"
                                         src="<?= path_image ?>images_product/expositores/buscatornillos.png">
                                </a>
                                <span class="text_lupa"><?= lang_title_ver_imagen ?></span>
                                <div class="col-md-12 price_expositor_green">
                                    <span style='font-size: 1.2em; color: #37af04;font-weight: bolder;'>1€</span>
                                </div>
                            </div>                    
                        </div>
                    </div>
        </div>
    </div>
</div>