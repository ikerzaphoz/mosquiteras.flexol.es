<?php

if (!isset($_SESSION)) {
    session_start();
}

require '../../../../config.php';

unset($_SESSION['products']);

$data = $_POST['data'];

foreach ($data as $index => $item):

    $type_product = "";
    if (isset($item['type_product'])): $type_product = $item['type_product'];
    endif;
    $is_lacado = "";
    if (isset($item['is_lacado'])): $is_lacado = $item['is_lacado'];
    endif;

    if (!empty($is_lacado)):

        $color_lacado = "";
        if (isset($item['color_lacado'])): $color_lacado = $item['color_lacado'];
        endif;
        $unit_lacado = "";
        if (isset($item['unit_lacado'])): $unit_lacado = $item['unit_lacado'];
        endif;
        $product_price_lacado = "";
        if (isset($item['product_price_lacado'])): $product_price_lacado = $item['product_price_lacado'];
        endif;

        $array_product = ['is_lacado' => $is_lacado, 'color_lacado' => $color_lacado, 'unit_lacado' => $unit_lacado, 'product_price_lacado' => $product_price_lacado];

        $_SESSION['products']['lacado'][$index] = $array_product;

    elseif ($type_product == "product"):

        $id_product = "";
        if (isset($item['id_product'])): $id_product = $item['id_product'];
        endif;
        $color_product = "";
        if (isset($item['color_product'])): $color_product = $item['color_product'];
        endif;
        $group_color = "";
        if (isset($item['group_color'])): $group_color = $item['group_color'];
        endif;
        $subfamily_product = "";
        if (isset($item['subfamily_product'])): $subfamily_product = $item['subfamily_product'];
        endif;
        $input_unit_product = "";
        if (isset($item['unit_product']))
            $input_unit_product = $item['unit_product'];
        $input_width_product = "";
        if (isset($item['width_product']))
            $input_width_product = $item['width_product'];
        $input_height_product = "";
        if (isset($item['height_product']))
            $input_height_product = $item['height_product'];
        $observaciones_product = "";
        if (isset($item['observaciones_product']))
            $observaciones_product = $item['observaciones_product'];
        $array_increment = "";
        if (isset($item['array_increment']))
            $array_increment = $item['array_increment'];
        $tipo_corredera = "";
        if (isset($item['tipo_corredera'])): $tipo_corredera = $item['tipo_corredera'];
        endif;

        if ($id_product != "2735") {

            $array_product = ['id_product' => $id_product, 'color_product' => $color_product, 'group_color' => $group_color, 'subfamily_product' => $subfamily_product, 'unit_product' => $input_unit_product, 'width_product' => $input_width_product, 'height_product' => $input_height_product, 'observaciones_product' => $observaciones_product, 'type_product' => $type_product, 'array_increment' => $array_increment];

            if ($id_product == "1999"):

                $_SESSION['products']['expositor'][$index] = $array_product;


            else:

                $_SESSION['products']['product'][$index] = $array_product;

            endif;
        }else {

            $product_price = "";
            if (isset($item['product_price'])): $product_price = $item['product_price'];
            endif;

            $array_product = ['id_product' => $id_product, 'tipo_corredera' => $tipo_corredera, 'color_product' => $color_product, 'group_color' => $group_color, 'subfamily_product' => $subfamily_product, 'unit_product' => $input_unit_product, 'width_product' => $input_width_product, 'height_product' => $input_height_product, 'observaciones_product' => $observaciones_product, 'type_product' => $type_product, 'array_increment' => $array_increment, 'product_price' => $product_price];

            $_SESSION['products']['product'][$index] = $array_product;
        } elseif ($type_product == "component"):

        $id_product = "";
        if (isset($item['id_product'])): $id_product = $item['id_product'];
        endif;
        $color_product = "";
        if (isset($item['color_product'])): $color_product = $item['color_product'];
        endif;
        $group_color = "";
        if (isset($item['group_color'])): $group_color = $item['group_color'];
        endif;
        $subfamily_product = "";
        if (isset($item['subfamily_product'])): $subfamily_product = $item['subfamily_product'];
        endif;
        $input_unit_product = "";
        if (isset($item['unit_product']))
            $input_unit_product = $item['unit_product'];
        $input_width_product = "";
        if (isset($item['width_product']))
            $input_width_product = $item['width_product'];
        $input_height_product = "";
        if (isset($item['height_product']))
            $input_height_product = $item['height_product'];
        $observaciones_product = "";
        if (isset($item['observaciones_product']))
            $observaciones_product = $item['observaciones_product'];
        $array_increment = "";
        if (isset($item['array_increment']))
            $array_increment = $item['array_increment'];
        $product_price = "";
        if (isset($item['product_price']))
            $product_price = $item['product_price'];
        $is_almacen = 0;
        if (isset($item['is_almacen']))
            $is_almacen = $item['is_almacen'];
        $arprpr = 0;
        if (isset($item['arprpr']))
            $arprpr = $item['arprpr'];
        $tipo_medida_componente = "0";
        if (isset($item['tipo_medida_componente']))
            $tipo_medida_componente = $item['tipo_medida_componente'];

        $array_product = ['id_product' => $id_product, 'color_product' => $color_product, 'group_color' => $group_color, 'subfamily_product' => $subfamily_product, 'unit_product' => $input_unit_product, 'width_product' => $input_width_product, 'height_product' => $input_height_product, 'observaciones_product' => $observaciones_product, 'type_product' => $type_product, 'array_increment' => $array_increment, 'product_price' => $product_price, 'is_almacen' => $is_almacen, 'arprpr' => $arprpr, 'tipo_medida_componente' => $tipo_medida_componente];

        $_SESSION['products']['component'][$index] = $array_product;

    endif;

endforeach;
