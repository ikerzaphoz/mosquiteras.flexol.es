<?php

require '../../../../config.php';
$presupuesto = new Presupuesto();

$id_pres = "";
if(isset($_POST['id_linea_nota'])) $id_pres = $_POST['id_linea_nota'];
$obs = "";
if(isset($_POST['obs'])) $obs = $_POST['obs'];

$obs = replaceCharacteresSql($obs);

$array_error = array('error' => '-2', 'message' => 'Error desconocido');

$affected = $presupuesto->updateObs($id_pres, $obs);

if($affected == 1):
    $array_error = array('error' => '0', 'message' => 'Observaciones guardadas correctamente');
elseif($affected < 0):
    $array_error = array('error' => '-1', 'message' => 'Error al guardar observaciones');
else:
    $array_error = array('error' => '1', 'message' => 'No se han encontrado cambios');
endif;

echo json_encode($array_error);