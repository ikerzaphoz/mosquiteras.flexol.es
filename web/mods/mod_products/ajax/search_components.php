<?php

require '../../../../config.php';
$familia = new Familia();

$text = "";
if (isset($_GET['text']))
    $text = $_GET['text'];
$type_search = "";
if (isset($_GET['type_search']))
    $type_search = $_GET['type_search'];
$id_pro = "";
if (isset($_GET['id_component_product']))
    $id_pro = $_GET['id_component_product'];
$change_components = "";
if (isset($_GET['change_components']))
    $change_components = $_GET['change_components'];

if (empty($text)):
    include root.'web/mods/mod_search/empty_search.php';
else:

    $text = normaliza($text);

    include root.'web/mods/mod_search/product_search.php';
endif;