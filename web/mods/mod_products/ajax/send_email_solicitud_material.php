<?php

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$mail = new PHPMailer();                              // Passing `true` enables exceptions
try {
    //Server settings
    $mail->SMTPDebug = 1;                                 // Enable verbose debug output
    //$mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'plesk150.red166.trevenque.es';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'websolicitudmuestras@flexol.es';                 // SMTP username
    $mail->Password = 'zNox939&';                           // SMTP password
    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 465;                                    // TCP port to connect to
    //Recipients
    // Activo condificacción utf-8
    $mail->CharSet = 'UTF-8';
    $mail->setFrom('websolicitudmuestras@flexol.es', "Solicitud de Muestras y Expositores");
    $mail->addAddress('clientes@flexol.es', 'Solicitud de Muestras y Expositores');
    if(isset($_SESSION['email_log'])):
        $mail->addAddress($_SESSION['email_log'], 'Solicitud de Muestras y Expositores');
    endif;
    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Solicitud de Muestras y Expositores';
    $email_body = 'Estimado cliente:<br>'
            .'Le informamos que hemos recibido la solicitud adjunta y procedemos a su gestión.<br>'
            .'Empresa: ENTRECORTINAS<br>'
            .'Cliente:'.$_SESSION['clproc'].$_SESSION['clcodi'].' - '.$_SESSION['name_log'].'<br><br>';
    include root . 'web/mods/mod_emails/footer.php';
    $mail->Body = $email_body . $email_footer;
    $mail->AltBody = 'Se he generado una solicitud de material';
    $mail->AddAttachment($ruta_pdf_pvp_email);
    $mail->send();
} catch (Exception $e) {
    echo '0.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
}
