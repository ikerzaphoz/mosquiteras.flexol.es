<?php

if(!isset($_SESSION))
{
    session_start();
}

require '../../../../config.php';
$familia = new Familia();

$option = "";
if(isset($_POST['option_sesion_ral'])) $option = $_POST['option_sesion_ral'];
$color_product = "";
if(isset($_POST['color_product'])) $color_product = $_POST['color_product'];
$unit_total = "";
if(isset($_POST['unit_total'])) $unit_total = $_POST['unit_total'];
$id_product = "";
if(isset($_POST['id_product'])) $id_product = $_POST['id_product'];

$is_lacado = $familia->getIsLacado($color_product, $id_product);

if($is_lacado == 1):
    if($option == "change_unit"):
        $color_html = $familia->getColorsRal($color_product, $id_product);
        set_array_ral($color_product, $unit_total);
    endif;
endif;

if($option == "change_color"):
    $color_product_old = "";
    if(isset($_POST['color_product_old'])) $color_product_old = $_POST['color_product_old'];
    if($familia->getIsLacado($color_product_old, $id_product) == 1):
        down_units_ral($color_product_old, $unit_total);
    endif;
    $color_product_new = "";
    if(isset($_POST['color_product_new'])) $color_product_new = $_POST['color_product_new'];
    if($familia->getIsLacado($color_product_new, $id_product) == 1):
        up_units_ral($color_product_new, $unit_total);
    endif;
endif;

foreach($_SESSION['array_colores_ral'] as $color_session => $item):?>
    <?php

    $precio = 0;
    $color_html = "";
    $color_html = $familia->getColorsRal($color_session, $id_product);

    if($item['unidades'] > 0):
        
        if($item['unidades'] == 1):
            $precio = "75.00";
        elseif($item['unidades'] > 1 AND $item['unidades'] <= 6):
            $precio = "125.00";
        elseif($item['unidades'] > 6 AND $item['unidades'] <= 15):
            $precio = "200.00";
        elseif($item['unidades'] > 15 AND $item['unidades'] <= 25):
            $precio = "275.00";
        elseif($item['unidades'] > 25 AND $item['unidades'] <= 35):
            $precio = "350.00";
        elseif($item['unidades'] > 35 AND $item['unidades'] <= 50):
            $precio = "475.00";
        elseif($item['unidades'] > 50):
        endif;

        ?>
        <div class="col-md-12 col-xs-12 line_lacado_ral">
            <div class="col-md-3 col-xs-3 title_line_product">
                <div class="preview_color_lacado" style="background-color: <?=$color_html['coralht']?>;"></div>
            </div>
            <div class="col-md-3 col-xs-3 color_lacado"><?=$color_session?></div>
            <div class="col-md-3 col-xs-3 unit_lacado"><?=$item['unidades']?></div>
            <div class="col-md-3 col-xs-3 product_price_lacado"><?=$precio?></div>
        </div>
    <?php endif; ?>
<?php endforeach;