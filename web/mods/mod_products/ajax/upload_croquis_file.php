<?php

require '../../../../config.php';
ini_set("display_errors", 1);

$fecha_hoy = date("Y/m/d");
if(!is_dir(path_uploads_files.$fecha_hoy)){
    mkdir(path_uploads_files.$fecha_hoy, 0755, true);
}

$nombre_fichero = date("Y_m_d_").date('H_i_s')."_".strtolower($_SESSION['name_log']);
$nombre_fichero = str_replace(" ", "", $nombre_fichero);

$titulo_respuesta = "Adjuntar archivo croquis";

$array_error = array('error' => '1', 'message' => 'Error al subir archivo', 'title' => $titulo_respuesta);

foreach ($_FILES["file_croquis"] as $key => $error) {
    if ($error == UPLOAD_ERR_OK) {
        $array_nombre_extension = explode('.', $_FILES["file_croquis"]["name"]);
        $extension = array_pop($array_nombre_extension);
        move_uploaded_file( $_FILES["file_croquis"]["tmp_name"], path_uploads_files . "/" . $fecha_hoy . "/" . $nombre_fichero."_.".$extension);
        $array_error = array('error' => '0', 'message' => 'Archivo subido correctamente', 'title' => $titulo_respuesta, 'file_title_hidden' => $nombre_fichero."_.".$extension, 'file_title' => $_FILES["file_croquis"]["name"]);
    }
}

echo json_encode($array_error);