<div id="content_product" class="div_all_product">
    <h4 class="text-center"><?=lang_menu_presupuestos_pedidos?></h4>
    <hr>
    <div class="row">
        <form class="form_product col-xs-12 col-md-12" name="form_product" method="post"
              action="<?= path_web ?>productos.php">
            <div class="panel-group">

                <div class="panel panel-default form-inline col-md-12 col-xs-12 nopadding">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <div class="title_panel_title"><?= lang_title_product ?></div>
                        </h4>
                    </div>
                    <div class="panel-collapse col-md-12 col-xs-12 nopadding">
                        <div id_product="2731" class="radio radio_product col-md-4 col-xs-6">
                                <img alt="2731" class="icon_product" src="<?= path_image ?>icons_products/2731.jpg">
                                <label><span class="title_label">Enrollables Ventana</span></label>
                        </div>
                        <div id_product="2732" class="radio radio_product col-md-4 col-xs-6">
                                <img alt="2732" class="icon_product" src="<?= path_image ?>icons_products/2732.jpg">
                                <label><span class="title_label">Enrollable Puerta</span></label>
                        </div>
                        <div id_product="plisadas" class="radio radio_product col-md-4 col-xs-6">
                                <img alt="2733" class="icon_product" src="<?= path_image ?>icons_products/2733.jpg">
                                <label><span class="title_label">Plisadas</span></label>
                        </div>
                        <div id_product="fijas_correderas" class="radio radio_product col-md-4 col-xs-6">
                                <img alt="2734" class="icon_product" src="<?= path_image ?>icons_products/2734.jpg">
                                <label><span class="title_label">Fijas y Correderas</span></label>
                        </div>
                        <div id_product="2736" class="radio radio_product col-md-4 col-xs-6">
                                <img alt="2736" class="icon_product" src="<?= path_image ?>icons_products/2736.jpg">
                                <label><span class="title_label">Abatibles</span></label>
                        </div>
                        <?php

                        $salto_linea = "";

                        if (dispositivo == "mobile")
                            $salto_linea = "<br>";

                        /*foreach ($array_familia as $item):
                            $id_pro = $item['fafami'];
                            $title_product = $item['fadesc'];

                            ?>

                            <div id_product="<?= $id_pro ?>" class="radio radio_product col-md-4 col-xs-6">
                                <img class="icon_product"
                                     src="<?= path_image ?>icons_products/<?= $id_pro ?>.jpg"><?= $salto_linea ?><label><span
                                        class="title_label"><?= $title_product ?></span></label>
                            </div>

                        <?php endforeach; */?>
                        
                            <div class="radio col-md-4 col-xs-6 radio_product_expositor">
                                <!--img class="hidden icon_product" src="<?= path_image ?>icons_products/<?= $id_pro ?>.jpg"-->
                                <label class="title_link_expositor"><a target="_blank"
                                   class="link_expositores" href="<?= path_web ?>productos.php?exp=exp">
                                <?= $salto_linea ?><input class="link_radio_expositor_text" type="radio"/>
                                <span
                                        class="title_label"><strong><?= lang_title_expositores ?></strong>
                                </span></a></label>
                            </div>
                        
                    </div>
                </div>
            </div>

            <div class="hidden type_product_group_fijas_correderas panel panel-default form-inline col-md-12 col-xs-12">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <div class="title_panel_title"><?=lang_title_version_producto?></div>
                    </h4>
                </div>
                <div class="panel-collapse text-center nopadding">
                    <div id_product="2734" class="radio radio_product col-md-4 col-xs-6">
                        <img alt="2734" class="icon_product" src="<?= path_image ?>icons_products/2734.jpg">
                        <label><span class="title_label">Fijas</span></label>
                    </div>
                    <div id_product="2735" class="radio radio_product col-md-4 col-xs-6">
                        <img alt="2735" class="icon_product" src="<?= path_image ?>icons_products/2735.jpg">
                        <label><span class="title_label">Correderas</span></label>
                    </div>
                </div>
            </div>
            <div class="hidden type_product_group_plisadas panel panel-default form-inline col-md-12 col-xs-12">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <div class="title_panel_title"><?=lang_title_version_producto?></div>
                    </h4>
                </div>
                <div class="panel-collapse text-center nopadding">
                    <div class="row col-md-12 col-xs-12">
                        <div id_product="2733" class="text-left radio radio_product col-md-6 col-xs-6">
                            <img alt="2733" class="icon_product" src="<?= path_image ?>icons_products/2733.jpg">
                            <label><span class="title_label">Plisada de 40</span><br><span class="title_label">Puerta</span></label>
                        </div>
                        <div class="text-left radio radio_product col-md-6 col-xs-6"></div>
                    </div>
                    <div class="row col-md-12 col-xs-12">
                        <div id_product="2737" class="text-left radio radio_product col-md-4 col-xs-4">
                            <img alt="2737" class="icon_product" src="<?= path_image ?>icons_products/2737.jpg">
                            <label><span class="title_label">Plisada de 22</span><br><span class="title_label">Puerta Lateral</span></label>
                        </div>
                        <div id_product="2737" class="text-left radio radio_product col-md-4 col-xs-4 no_show_version_2737" subfamilia_2737="2">
                            <img alt="2737" class="icon_product" src="<?= path_image ?>icons_products/2737.jpg">
                            <label><span class="title_label">Plisada de 22</span><br><span class="title_label">Puerta Reversible</span></label>
                        </div>
                        <div id_product="2737" class="text-left radio radio_product col-md-4 col-xs-4 no_show_version_2737" subfamilia_2737="3">
                            <img alt="2737" class="icon_product" src="<?= path_image ?>icons_products/2737.jpg">
                            <label><span class="title_label">Plisada de 22</span><br><span class="title_label">Ventana</span></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hidden type-product panel panel-default form-inline col-md-12 col-xs-12">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <div class="title_panel_title"><?= lang_title_tipo_producto ?></div>
                    </h4>
                </div>

                <div class="panel-collapse text-center nopadding">
                    <div class="col-md-4 col-xs-6 text-right content_radio_product align_left_mobile">
                        <input type="radio" name="change_components"
                               class="change_components change_components_acabado" value="2"><label
                            class="font-light check_product_component">
                            &nbsp;&nbsp;&nbsp;<?= lang_title_producto_acabado ?></label>
                                <div class="hidden img_perfil_acabado img_perfil_2734 row col-xs-12 col-md-12">
                                    <img alt="perfiles" src="<?= path_image ?>images_product/2734/perfiles.png">
                                    <div class="col-xs-3 col-md-1">
                                        <a class="image-popup-no-margins mt100 icono-lupa fa fa-search" href="<?=path_image?>images_product/2734/perfiles.png">
                                        <img alt="perfiles" class="hidden" src="<?=path_image?>images_product/2734/perfiles.png">
                                        </a>
                                        <span class="text_lupa"><?=lang_title_ver_imagen?></span>
                                    </div>
                                </div>
                                <div class="hidden img_perfil_acabado img_perfil_2735 row col-xs-12 col-md-12">
                                    <img alt="perfiles" src="<?= path_image ?>images_product/2735/perfiles.png">
                                    <div class="col-xs-3 col-md-1">
                                        <a class="image-popup-no-margins mt100 icono-lupa fa fa-search" href="<?=path_image?>images_product/2735/perfiles.png">
                                        <img alt="perfiles" class="hidden" src="<?=path_image?>images_product/2735/perfiles.png">
                                        </a>
                                        <span class="text_lupa"><?=lang_title_ver_imagen?></span>
                                    </div>
                                </div>
                    </div>
                    <div class="col-md-4 col-xs-6 text-center content_radio_product div_button_componente">
                        <input type="radio" name="change_components" class="change_components" value="1"><label
                            class="font-light check_product_component">
                            &nbsp;&nbsp;&nbsp;<?= lang_title_componente ?></label>
                    </div>
                    <div class="col-md-4 col-xs-6 text-left content_radio_product div_button_industrial">
                        <input type="radio" name="change_components" class="change_components" value="3"><label
                            class="font-light check_product_component">
                            &nbsp;&nbsp;&nbsp;<?= lang_title_venta_industrial ?></label>
                    </div>
                </div>
            </div>

            <div class="info_insert"></div>

            <div class="hidden panel panel-default form-inline col-md-12 col-xs-12 div_color_acabado nopadding">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <div class="title_panel_title"><?= lang_title_colores ?></div>
                    </h4>
                </div>
                <div class="panel-collapse col-md-12 col-xs-12 nopadding">
                    <div class="row">
                        <div class="div_colors col-md-8 col-xs-6">
                            <h5><?= lang_title_carta_colores ?></h5>
                            <h6>
                                <i class="fa fa-warning text-warning"></i><?= lang_title_seleccione_producto_colores ?>
                            </h6>
                        </div>
                        <div class="div_preview_img col-md-2 col-xs-6">
                            <h5><?= lang_title_vista_previa ?></h5>
                            <div class="text-center div_content_preview_img">
                                <div
                                    style="background-image: url(<?= path_image ?>images_product/2731/2.png)"
                                    class="row preview_img">
                                    <span class="text_no_image_preview"></span>
                                    <div class="content_lupa_vista_previa">
                                        <a class="image-popup-no-margins lupa_div_content_preview_img icono-lupa fa fa-search"
                                           href="<?= path_image ?>images_plupa_div_content_preview_imgroduct/2731/2.png">
                                            <img alt="2" class="hidden"
                                                 src="<?= path_image ?>images_product/2731/2.png">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <span class="message_color text-warning"><span class="fa fa-warning"></span>&nbsp;<?=lang_text_diferir_color?></span>
                </div>
            </div>

            <div class="form-inline col-md-12 col-xs-12 div_submit_product hidden padding8">
                <div class="panel-collapse text-center padding8">
                    <div class="form-group">
                        <button type="button" class="btn-sm btn-default btn_modal_ini_pro btn bg-border-info"
                                value="presupuesto"><?= lang_title_continuar_pres ?></button>
                    </div>
                    <?php if($_SESSION['pago_anticipado'] == "1"):?>
                        <div class="form-group">
                            <button type="button" class="btn-sm btn_modal_ini_pro btn bg-border-warning" name="type_button" value="pedido_valorado">Continuar valoración de pedido</button>
                        </div>
                        <?php else:?>
                        <div class="form-group">
                            <button type="button" class="btn-sm btn_modal_ini_pro btn bg-border-warning" name="type_button" value="pedido"><?=lang_title_continuar_ped?></button>
                        </div>
                    <?php endif; ?>
                </div>
                <?php if($_SESSION['pago_anticipado'] == "1"):?>
                <div class="col-md-6 col-xs-6"></div>
                <div class="col-md-6 col-xs-6 text-left"><p><i class="fa fa-2x fa-exclamation-circle text-danger"></i><strong>Su forma de pago es ANTICIPADO.</strong> Pulse <em>"Continuar valoración de pedido"</em> para obtener el importe y números de cuenta bancaria donde realizar el ingreso.</p></div> 
                <?php endif; ?> 
            </div>


    </div>

    <input value="<?= $id_pro ?>" span_value="<?= $id_pro ?>" class="hidden input_id_product"
           name="id_product"/>
    <input value="" class="hidden input_color_product" name="color_product"/>
    <input value="" class="hidden input_group_color" name="group_color"/>
    <input value="" class="hidden type_button" name="type_button"/>

    </form>
</div>
</div>