<div id="content_product" class="content_div_linea row">
    <div class="panel-group">
        <div class="panel panel-default form-inline col-md-12 pb-3">
            <div class="panel-heading selected">
                <h4 class="panel-title">
                    <div class="title_panel_title"><?=lang_text_detalle?></div>
                </h4>
            </div>
            <div class="panel-collapse text-left">
                <div class="row">
                    <label><?=lang_title_referencia?>:</label><input maxlength="10" class="referencia" name="referencia" type="text"
                                                     placeholder="<?=lang_title_referencia?>...">
                </div>
            </div>
            <div class="panel-content-lineas-pedido panel-collapse text-center">
                <div class="content_lineas_pedido row">

                    <?php

                    if (isset($_SESSION['products']) AND !empty($_SESSION['products'])):
                        include root . 'web/mods/mod_cart/product_cart.php';
                    endif;

                    ?>

                    <?php include 'linea_pedido_expositor.php' ?>
                </div>
            </div>
            <div class="col-md-12 col-xs-12 hidden table_product_0">
                <div class="col-md-12 col-xs-12 text-center"><i class="fa fa-exclamation-triangle"></i><span>&nbsp;<?=lang_text_no_productos?>.</span>
                </div>
            </div>
            <div class="segunda_linea_producto mt-10 text-right row col-md-12 col-xs-12">
                <button data-toggle="modal" data-target="#modal_add_product" title="Añadir nuevo producto"
                        class="btn btn_add_product btn_add_product_material btn-sm bg-border-warning">Añadir otro material
                </button>
            </div>
            <div class="segunda_linea_producto row total_line col-md-12 col-xs-12 text-right">
                <div class="col-md-10 col-xs-10"><b>Total PVP (€):</b></div>
                <div><input readonly type="text" class="col-md-2 col-xs-2 price_total"
                            value="<?= round2decimals($total_presupuesto) ?>"/></div>
                <div class="hidden col-md-10 col-xs-10"><b>Total <?= $type_button ?> condiciones cliente (€):</b></div>
                <div class="col-md-10 col-xs-10">(<?=lang_text_impuestos_no_incluidos?>)</div>
                <div class="hidden"><input readonly type="text" class="col-md-2 col-xs-2 price_cond_cliente"
                                           value="PENDIENTE"/>
                </div>
            </div>

            <input type="hidden" value="<?= $_SESSION['clproc'] ?>" class="clproc_cliente">
            <input type="hidden" value="<?= $total_lacado ?>" class="total_lacado">
            <input type="hidden" value="<?= $_SESSION['clcodi'] ?>" class="clcodi_cliente">
            <input type="hidden" value="fiscal" class="id_dire">
            <input type="hidden" value="<?= $type_button ?>" class="is_pedido" name="is_pedido">
            <input type="hidden" value="seleccionar_dire" class="agencia" name="agencia">

            <div class="row col-md-12 col-xs-12 mt-10 segunda_linea_producto">
                <?php /*MODIFICADO 100518*/?>
                <?php if (isset($_SESSION['ini_log']) == 1): ?>
                    <div class="row col-md-12 col-xs-12 text-center">
                        <button class="btn-sm btn-editar-dire btn bg-border-warning"
                                aria-hidden="true">Confirmar solicitud de material
                        </button>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

