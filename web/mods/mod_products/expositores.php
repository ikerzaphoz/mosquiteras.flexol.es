<form name="form_product" class="form_product" method="post" action="<?= path_web ?>productos.php">
    <div id="content_product" class="panel panel-default content_product_expositores">
        <div class="panel-heading selected">
            <h4 class="panel-title">
                <div class="title_panel_title"><?= lang_title_expositores_punto_venta ?></div>
            </h4>
        </div>
        <div class="panel-body text-center">
            <div class="col-md-12 col-xs-12">
                <div class="radio col-md-7 col-xs-7">
                    <div class="col-md-6 col-xs-6">
                        <div class="col-md-12 col-xs-12 div_title_expositores expositores_ventana contenido_expositor">
                            <div class="col-md-8 col-xs-8">
                                <strong><span class="text_expo">Enr. Ventana Cajón 35</span></strong>                
                            </div>
                            <div class="col-md-4 col-xs-4 div_unidades_expositores">
                                <input class="unidades_expositores" type="number" value="0" min="0">
                                <span class="text_uds">Uds.</span>
                                <input type="hidden" class="precio_expo hidden" value="75"/>
                                <input type="hidden" class="total_line">
                                <span class="hidden text">Muestra enr. cajón 35</span>
                                <input class="info_submit" type="hidden" name="enr_ventana_cajon_35" value="">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 div_title_expositores expositores_ventana contenido_expositor">
                            <div class="col-md-8 col-xs-8">
                                <strong><span class="text_expo">Enr. Ventana Cajón 42</span></strong>                
                            </div>
                            <div class="col-md-4 col-xs-4 div_unidades_expositores">
                                <input class="unidades_expositores" type="number" value="0" min="0">
                                <span class="text_uds">Uds.</span>
                                <input type="hidden" class="precio_expo hidden" value="75"/>
                                <input type="hidden" class="total_line">
                                <span class="hidden text">Muestra enr. cajón 42</span>
                                <input class="info_submit" type="hidden" name="enr_ventana_cajon_42" value="">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 contenido_expositor_border">
                            <div class="col-md-12 col-xs-12 div_img_expositores">
                                <img alt="img_expositor" class="img_expositor"
                                     src="<?= path_image ?>images_product/expositores/enrollable_ventana.png">
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <a class="image-popup-no-margins icono-lupa fa fa-search"
                                   href="<?= path_image ?>images_product/expositores/enrollable_ventana.png">
                                    <img alt="img_expositor" class="hidden"
                                         src="<?= path_image ?>images_product/expositores/enrollable_ventana.png">
                                </a>
                                <span class="text_lupa"><?= lang_title_ver_imagen ?></span>
                            </div>
                            <div class="col-md-12 price_expositor_green">
                                <span style='font-size: 1.2em; color: #37af04;font-weight: bolder;'>75€</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6 contenido_expositor">
                        <div class="col-md-12 col-xs-12 div_title_expositores">
                            <div class="col-md-8 col-xs-8">
                                <strong><span class="text_expo">Enrollable Puerta</span></strong>
                            </div>
                            <div class="col-md-4 col-xs-4 div_unidades_expositores">
                                <input class="unidades_expositores" type="number" value="0" min="0">
                                <span class="text_uds">Uds.</span>
                                <input type="hidden" class="precio_expo hidden" value="150"/>
                                <input type="hidden" class="total_line">
                                <span class="hidden text">Muestra enr. puerta</span>
                                <input class="info_submit" type="hidden" name="enr_puerta" value="">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 contenido_expositor_border">
                            <div class="col-md-12 col-xs-12 div_img_expositores">
                                <img alt="img_expositor" class="img_expositor"
                                     src="<?= path_image ?>images_product/expositores/enrollable_puerta.png">
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <a class="image-popup-no-margins icono-lupa fa fa-search"
                                   href="<?= path_image ?>images_product/expositores/enrollable_puerta.png">
                                    <img alt="img_expositor" class="hidden"
                                         src="<?= path_image ?>images_product/expositores/enrollable_puerta.png">
                                </a>
                                <span class="text_lupa"><?= lang_title_ver_imagen ?></span>
                            </div>
                            <div class="col-md-12 price_expositor_green">
                                <span style='font-size: 1.2em; color: #37af04;font-weight: bolder;'>150€</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6 mt-3">
                        <div class="col-md-12 col-xs-12 div_title_expositores contenido_expositor">
                            <div class="col-xs-8 col-md-8">
                                <strong><span class="text_expo text-left">Plisada de 40<br>Puerta</span></strong>
                            </div>
                            <div class="col-md-4 col-xs-4 div_unidades_expositores">
                                <input class="unidades_expositores" type="number" value="0" min="0">
                                <span class="text_uds">Uds.</span>
                                <input type="hidden" class="precio_expo hidden" value="150"/>
                                <input type="hidden" class="total_line">
                                <span class="hidden text">Muestra plisada de 40</span>
                                <input class="info_submit" type="hidden" name="plisada_40" value="">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 contenido_expositor_border">
                            <div class="col-md-12 col-xs-12 div_img_expositores">
                                <img alt="img_expositor" class="img_expositor"
                                     src="<?= path_image ?>images_product/expositores/plisada.png">
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <a class="image-popup-no-margins icono-lupa fa fa-search"
                                   href="<?= path_image ?>images_product/expositores/plisada.png">
                                    <img alt="img_expositor" class="hidden" src="<?= path_image ?>images_product/expositores/plisada.png">
                                </a>
                                <span class="text_lupa"><?= lang_title_ver_imagen ?></span>
                            </div>
                            <div class="col-md-12 price_expositor_green">
                                <span style='font-size: 1.2em; color: #37af04;font-weight: bolder;'>150€</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6 mt-3 contenido_expositor">
                        <div class="col-md-12 col-xs-12 div_title_expositores">
                            <div class="col-md-8 col-xs-8">
                                <strong><span class="text_expo">Abatibles</span></strong>
                            </div>
                            <div class="col-md-4 col-xs-4 div_unidades_expositores">
                                <input class="unidades_expositores" type="number" value="0" min="0">
                                <span class="text_uds">Uds.</span>
                                <input type="hidden" class="precio_expo hidden" value="150"/>
                                <input type="hidden" class="total_line">
                                <span class="hidden text">Muestra abatibles</span>
                                <input class="info_submit" type="hidden" name="exp_abatible" value="">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 contenido_expositor_border">

                            <div class="col-md-12 col-xs-12 div_img_expositores">
                                <img alt="img_expositor" class="img_expositor"
                                     src="<?= path_image ?>images_product/expositores/abatible.png">
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <a class="image-popup-no-margins icono-lupa fa fa-search"
                                   href="<?= path_image ?>images_product/expositores/abatible.png">
                                    <img alt="img_expositor" class="hidden" src="<?= path_image ?>images_product/expositores/abatible.png">
                                </a>
                                <span class="text_lupa"><?= lang_title_ver_imagen ?></span>
                                <div class="col-md-12 price_expositor_green">
                                    <span style='font-size: 1.2em; color: #37af04;font-weight: bolder;'>150€</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6 mt-3">
                        <div class="col-md-12 col-xs-12 div_title_expositores contenido_expositor">
                            <div class="col-xs-8 col-md-8">
                                <strong><span class="text_expo">Plisada de 22<br>Puerta Lateral</span></strong>
                            </div>
                            <div class="col-md-4 col-xs-4 div_unidades_expositores">
                                <input class="unidades_expositores" type="number" value="0" min="0">
                                <span class="text_uds">Uds.</span>
                                <input type="hidden" class="precio_expo hidden" value="150"/>
                                <input type="hidden" class="total_line">
                                <span class="hidden text">Muestra plisada de 22 puerta lateral</span>
                                <input class="info_submit" type="hidden" name="plisada_ventana" value="">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 contenido_expositor_border">
                            <div class="col-md-12 col-xs-12 div_img_expositores">
                                <img alt="img_expositor" class="img_expositor"
                                     src="<?= path_image ?>images_product/expositores/plisada_22_lateral.png">
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <a class="image-popup-no-margins icono-lupa fa fa-search"
                                   href="<?= path_image ?>images_product/expositores/plisada_22_lateral.png">
                                    <img alt="img_expositor" class="hidden" src="<?= path_image ?>images_product/expositores/plisada_22_lateral.png">
                                </a>
                                <span class="text_lupa"><?= lang_title_ver_imagen ?></span>
                            </div>
                            <div class="col-md-12 price_expositor_green">
                                <span style='font-size: 1.2em; color: #37af04;font-weight: bolder;'>150€</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6 mt-3">
                        <div class="col-md-12 col-xs-12 div_title_expositores contenido_expositor">
                            <div class="col-xs-8 col-md-8">
                                <strong><span class="text_expo">Plisada de 22<br>Ventana</span></strong>
                            </div>
                            <div class="col-md-4 col-xs-4 div_unidades_expositores">
                                <input class="unidades_expositores" type="number" value="0" min="0">
                                <span class="text_uds">Uds.</span>
                                <input type="hidden" class="precio_expo hidden" value="150"/>
                                <input type="hidden" class="total_line">
                                <span class="hidden text">Muestra plisada de 22 ventana</span>
                                <input class="info_submit" type="hidden" name="plisada_lateral" value="">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 contenido_expositor_border">
                            <div class="col-md-12 col-xs-12 div_img_expositores">
                                <img alt="img_expositor" class="img_expositor"
                                     src="<?= path_image ?>images_product/expositores/plisada_22_ventana.png">
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <a class="image-popup-no-margins icono-lupa fa fa-search"
                                   href="<?= path_image ?>images_product/expositores/plisada_22_ventana.png">
                                    <img alt="img_expositor" class="hidden" src="<?= path_image ?>images_product/expositores/plisada_22_ventana.png">
                                </a>
                                <span class="text_lupa"><?= lang_title_ver_imagen ?></span>
                            </div>
                            <div class="col-md-12 price_expositor_green">
                                <span style='font-size: 1.2em; color: #37af04;font-weight: bolder;'>150€</span>
                            </div>
                        </div>
                    </div>
                    <div class="hidden col-md-6 col-xs-6 mt-3">
                        <div class="col-md-12 col-xs-12 div_title_expositores contenido_expositor">
                            <div class="col-xs-8 col-md-8">
                                <strong><span class="text_expo">Plisada de 22<br>Puerta Reversible</span></strong>
                            </div>
                            <div class="col-md-4 col-xs-4 div_unidades_expositores">
                                <input class="unidades_expositores" type="number" value="0" min="0">
                                <span class="text_uds">Uds.</span>
                                <input type="hidden" class="precio_expo hidden" value="150"/>
                                <input type="hidden" class="total_line">
                                <span class="hidden text">Muestra plisada de 22 puerta reversible</span>
                                <input class="info_submit" type="hidden" name="plisada_reversible" value="">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 contenido_expositor_border">
                            <div class="col-md-12 col-xs-12 div_img_expositores">
                                <img alt="img_expositor" class="img_expositor"
                                     src="<?= path_image ?>images_product/expositores/plisada_22_lateral.png">
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <a class="image-popup-no-margins icono-lupa fa fa-search"
                                   href="<?= path_image ?>images_product/expositores/plisada_22_lateral.png">
                                    <img alt="img_expositor" class="hidden" src="<?= path_image ?>images_product/expositores/plisada_22_lateral.png">
                                </a>
                                <span class="text_lupa"><?= lang_title_ver_imagen ?></span>
                            </div>
                            <div class="col-md-12 price_expositor_green">
                                <span style='font-size: 1.2em; color: #37af04;font-weight: bolder;'>150€</span>
                            </div>
                        </div>
                    </div>
                    <div class="hidden col-md-6 col-xs-6 mt-3 contenido_expositor">
                        <div class="col-md-12 col-xs-12 div_title_expositores">
                            <div class="col-md-8 col-xs-8">
                                <strong><span class="text_expo">Fijas y Correderas</span></strong>
                            </div>
                            <div class="col-md-4 col-xs-4 div_unidades_expositores">
                                <input class="unidades_expositores" type="number" value="0" min="0">
                                <span class="text_uds">Uds.</span>
                                <input type="hidden" class="precio_expo hidden" value="75"/>
                                <input type="hidden" class="total_line">
                                <span class="hidden text">Muestra fijas y correderas</span>
                                <input class="info_submit" type="hidden" name="exp_fija_corredera" value="">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 contenido_expositor_border">

                            <div class="col-md-12 col-xs-12 div_img_expositores">
                                <img alt="img_expositor" class="img_expositor"
                                     src="<?= path_image ?>images_product/expositores/fija_corredera.png">
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <a class="image-popup-no-margins icono-lupa fa fa-search"
                                   href="<?= path_image ?>images_product/expositores/fija_corredera.png">
                                    <img alt="img_expositor" class="hidden"
                                         src="<?= path_image ?>images_product/expositores/fija_corredera.png">
                                </a>
                                <span class="text_lupa"><?= lang_title_ver_imagen ?></span>
                                <div class="col-md-12 price_expositor_green">
                                    <span style='font-size: 1.2em; color: #37af04;font-weight: bolder;'>150€</span>
                                </div>
                            </div>                    
                        </div>
                    </div>
                    <div class="hidden col-md-12 col-xs-12 mt-3 contenido_expositor">
                        <div class="col-md-12 col-xs-12 div_title_expositores">
                            <div class="col-md-8 col-xs-8">
                                <strong><span class="text_expo">Selector de Color</span></strong>
                            </div>
                            <div class="col-md-4 col-xs-4 div_unidades_expositores">
                                <input class="unidades_expositores" type="number" value="0" min="0">
                                <span class="text_uds">Uds.</span>
                                <input type="hidden" class="precio_expo hidden" value="10"/>
                                <input type="hidden" class="total_line">
                                <span class="hidden text">Selector de color</span>
                                <input class="info_submit" type="hidden" name="exp_selector_color" value="">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 contenido_expositor_border">

                            <div class="col-md-12 col-xs-12 div_img_expositores">
                                <img alt="img_expositor" class="img_expositor"
                                     src="<?= path_image ?>images_product/expositores/selector.png">
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <a class="image-popup-no-margins icono-lupa fa fa-search"
                                   href="<?= path_image ?>images_product/expositores/selector.png">
                                    <img alt="img_expositor" class="hidden"
                                         src="<?= path_image ?>images_product/expositores/selector.png">
                                </a>
                                <span class="text_lupa"><?= lang_title_ver_imagen ?></span>
                                <div class="col-md-12 price_expositor_green">
                                    <span style='font-size: 1.2em; color: #37af04;font-weight: bolder;'>10€</span>
                                </div>
                            </div>                    
                        </div>
                    </div>
                </div>
                <div class="radio col-md-5 col-xs-5 div_imagen_expositor contenido_expositor">
                    <div class="col-md-12 col-xs-12 div_title_expositores">
                        <div class="col-xs-8 col-md-8">
                            <strong><span class="text_expo">Expositor Personalizable</span></strong>
                        </div>
                        <div class="col-md-4 col-xs-4 div_unidades_expositores">
                            <input class="unidades_expositores" type="number" value="0" min="0">
                            <span class="text_uds">Uds.</span>
                            <input type="hidden" class="precio_expo hidden" value="590"/>
                            <input type="hidden" class="total_line">
                            <span class="hidden text">Expositor personalizable</span>
                            <input class="info_submit" type="hidden" name="exp_fija_expositor" value="">
                        </div>

                    </div>
                    <div class="col-md-12 col-xs-12 contenido_expositor_border">

                        <div class="col-md-12 col-xs-12 div_img_expositores">
                            <img alt="img_expositor" style="height: 667px;" src="<?= path_image ?>images_product/expositores/expositor.png">
                        </div>
                        <div>
                            <a class="image-popup-no-margins icono-lupa fa fa-search"
                               href="<?= path_image ?>images_product/expositores/expositor.png">
                                <img alt="img_expositor" class="hidden" src="<?= path_image ?>images_product/expositores/expositor.png">
                            </a>
                            <span class="text_lupa"><?= lang_title_ver_imagen ?></span>
                        </div>
                        <div class="col-md-12 price_expositor_green">
                            <span style='font-size: 1.2em; color: #37af04;font-weight: bolder;'>590€</span>
                        </div>
                    </div>
                    <div style="padding: 0; margin-top: 18px;" class="col-md-6 col-xs-6 mt-3 contenido_expositor">
                        <div class="col-md-12 col-xs-12 div_title_expositores">
                            <div class="col-md-7 col-xs-7">
                                <strong><span class="text_expo">Selector de Color</span></strong>
                            </div>
                            <div class="col-md-5 col-xs-5 div_unidades_expositores">
                                <input class="unidades_expositores" type="number" value="0" min="0">
                                <span class="text_uds">Uds.</span>
                                <input type="hidden" class="precio_expo hidden" value="10"/>
                                <input type="hidden" class="total_line">
                                <span class="hidden text">Selector de color</span>
                                <input class="info_submit" type="hidden" name="exp_selector_color" value="">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 contenido_expositor_border" style="padding-top: 45px">

                            <div class="col-md-12 col-xs-12 div_img_expositores" style="padding: 0 !important;">
                                <img alt="img_expositor" class="img_expositor" style="height: 200px"
                                     src="<?= path_image ?>images_product/expositores/selector.png">
                            </div>
                            <div class="col-md-12 col-xs-12" style="padding-top: 18px">
                                <a class="image-popup-no-margins icono-lupa fa fa-search"
                                   href="<?= path_image ?>images_product/expositores/selector.png">
                                    <img alt="img_expositor" class="hidden"
                                         src="<?= path_image ?>images_product/expositores/selector.png">
                                </a>
                                <span class="text_lupa"><?= lang_title_ver_imagen ?></span>
                                <div class="col-md-12 price_expositor_green">
                                    <span style='font-size: 1.2em; color: #37af04;font-weight: bolder;'>10€</span>
                                </div>
                            </div>                    
                        </div>
                    </div>
                    <div style="padding: 0; margin-top: 18px;" class="col-md-1 col-xs-1 mt-3 contenido_expositor"></div>
                    <div style="padding: 0; margin-top: 18px;" class="col-md-5 col-xs-5 mt-3 contenido_expositor">
                        <div class="col-md-12 col-xs-12 div_title_expositores">
                            <div class="col-md-7 col-xs-7">
                                <strong><span class="text_expo">Busca tornillos</span></strong>
                            </div>
                            <div class="col-md-5 col-xs-5 div_unidades_expositores">
                                <input class="unidades_expositores" type="number" value="0" min="0">
                                <span class="text_uds">Uds.</span>
                                <input type="hidden" class="precio_expo hidden" value="1"/>
                                <input type="hidden" class="total_line">
                                <span class="hidden text">Busca tornillos</span>
                                <input class="info_submit" type="hidden" name="exp_buscatornillos" value="">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 contenido_expositor_border">

                            <div class="col-md-12 col-xs-12 div_img_expositores" style="padding-top:90px;">
                                <img alt="img_expositor" class="img_expositor" style="height: 130px;"
                                     src="<?= path_image ?>images_product/expositores/buscatornillos.png">
                            </div>
                            <div class="col-md-12 col-xs-12" style="padding-top: 35px;">
                                <a class="image-popup-no-margins icono-lupa fa fa-search"
                                   href="<?= path_image ?>images_product/expositores/buscatornillos.png">
                                    <img alt="img_expositor" class="hidden"
                                         src="<?= path_image ?>images_product/expositores/buscatornillos.png">
                                </a>
                                <span class="text_lupa"><?= lang_title_ver_imagen ?></span>
                                <div class="col-md-12 price_expositor_green">
                                    <span style='font-size: 1.2em; color: #37af04;font-weight: bolder;'>1€</span>
                                </div>
                            </div>                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xs-6"></div>
    <div class="col-md-6 col-xs-6 text-right div_resumen_expositores hidden">
        <div class="col-xs-12 col-md-12 div_content_resumen_expositores">
            <div class="col-md-12 text-left col-xs-12 title_resumen_expositores"><div class='col-md-2 font-bold'><strong>Ud.</strong></div><div class='font-bold col-md-8'><strong>Artículo.</strong></div><div class=''><strong>PVP (€)</strong></div></div>
            <div class="col-md-12 text-left col-xs-12 content_resumen_expositores"></div>
            <div class="col-md-12 text-right col-xs-12 line_total_expositores"><strong>TOTAL PVP (Impuestos no incluidos):</strong> <span class="total_expo">0.00</span>&nbsp;€
            </div>
        </div>
    </div>
    <div class="form-inline col-md-12 col-xs-12 div_buttons_expo invisible">
        <div class="panel-collapse text-center padding8">
            <?php /*MODIFICADO 100518*/?>
            <div class="form-group">
                <button type="button" class="btn-sm btn_modal_ini_pro btn bg-border-warning" name="type_button" value="solicitud_material">Continuar solicitud de material</button>
            </div>
        </div>
    </div>
    <input type="hidden" class="form_expo" name="form_expo" value="1">
    <input value="" class="hidden type_button" name="type_button"/>
</form>