<div id="content_product" class="row panel panel-default">
    <div class="panel-heading selected">
        <h4 class="panel-title">
            <div class="title_panel_title">Expositores Punto de Venta</div>
        </h4>
    </div>
    <div class="panel-body text-center">
        <div class="col-md-12 col-xs-12">
            <div class="radio col-md-4 col-xs-4">
                <div class="col-md-12 col-xs-12">
                    Enrollable ventana
                </div>
                <div class="col-md-12 col-xs-12">
                    <img class="img_expositor" src="<?= path_image ?>images_product/expositores/enrollable_ventana.png">
                </div>
                <div class="col-md-12 col-xs-12">
                    <a class="image-popup-no-margins icono-lupa fa fa-search" href="<?= path_image ?>images_product/expositores/enrollable_ventana.png">
                        <img class="hidden" src="<?= path_image ?>images_product/expositores/enrollable_ventana.png">
                    </a>
                    <span class="text_lupa">Ver imagen</span>
                </div>
            </div>
            <div class="radio col-md-4 col-xs-4">
                <div class="col-md-12 col-xs-12">
                    Enrollables puerta
                </div>
                <div class="col-md-12 col-xs-12">
                    <img class="img_expositor" src="<?= path_image ?>images_product/expositores/enrollable_puerta.png">
                </div>
                <div class="col-md-12 col-xs-12">
                    <a class="image-popup-no-margins icono-lupa fa fa-search" href="<?= path_image ?>images_product/expositores/enrollable_puerta.png">
                        <img class="hidden" src="<?= path_image ?>images_product/expositores/enrollable_puerta.png">
                    </a>
                    <span class="text_lupa">Ver imagen</span>
                </div>
            </div>
            <div class="radio col-md-4 col-xs-4">
                <div class="col-md-12 col-xs-12">
                    Plisadas
                </div>
                <div class="col-md-12 col-xs-12">
                    <img class="img_expositor" src="<?= path_image ?>images_product/expositores/plisada.png">
                </div>
                <div class="col-md-12 col-xs-12">
                    <a class="image-popup-no-margins icono-lupa fa fa-search" href="<?= path_image ?>images_product/expositores/plisada.png">
                        <img class="hidden" src="<?= path_image ?>images_product/expositores/plisada.png">
                    </a>
                    <span class="text_lupa">Ver imagen</span>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-xs-12">
            <div class="radio col-md-4 col-xs-4">
                <div class="col-md-12 col-xs-12">
                    Abatibles
                </div>
                <div class="col-md-12 col-xs-12">
                    <img class="img_expositor" src="<?= path_image ?>images_product/expositores/abatible.png">
                </div>
                <div class="col-md-12 col-xs-12">
                    <a class="image-popup-no-margins icono-lupa fa fa-search" href="<?= path_image ?>images_product/expositores/abatible.png">
                        <img class="hidden" src="<?= path_image ?>images_product/expositores/abatible.png">
                    </a>
                    <span class="text_lupa">Ver imagen</span>
                </div>
            </div>
            <div class="radio col-md-4 col-xs-4">
                <div class="col-md-12 col-xs-12">
                    Fijas y correderas
                </div>
                <div class="col-md-12 col-xs-12">
                    <img class="img_expositor" src="<?= path_image ?>images_product/expositores/fija_corredera.png">
                </div>
                <div class="col-md-12 col-xs-12">
                    <a class="image-popup-no-margins icono-lupa fa fa-search" href="<?= path_image ?>images_product/expositores/fija_corredera.png">
                        <img class="hidden" src="<?= path_image ?>images_product/expositores/fija_corredera.png">
                    </a>
                    <span class="text_lupa">Ver imagen</span>
                </div>
            </div>
            <div class="radio col-md-4 col-xs-4">
                <div class="col-md-12 col-xs-12">
                    Expositor personalizable
                </div>
                <div class="col-md-12 col-xs-12">
                    <img class="img_expositor" src="<?= path_image ?>images_product/expositores/expositor.png">
                </div>
                <div class="col-md-12 col-xs-12">
                    <a class="image-popup-no-margins icono-lupa fa fa-search" href="<?= path_image ?>images_product/expositores/expositor.png">
                        <img class="hidden" src="<?= path_image ?>images_product/expositores/expositor.png">
                    </a>
                    <span class="text_lupa">Ver imagen</span>
                </div>
            </div>
        </div>
    </div>
</div>