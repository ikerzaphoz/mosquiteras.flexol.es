<table class="table table-condensed table_linea_pedido table-responsive">
    <thead class="table_product">
    <tr>
        <th>Producto</th>
        <th>Color</th>
        <th>Versión</th>
        <th>Unidades</th>
        <th>Ancho</th>
        <th>Alto</th>
        <th>Opciones</th>
        <th>Observaciones</th>
        <th>Precio</th>
        <th></th>
    </tr>
    </thead>
    <tbody class="content_table_linea_pedidos">
    <?php if(empty($_SESSION['products'])): ?>
        <tr class="line_product">
            <?php ?>
            <td class="title_product"><?=$array_familia[$id_product]['fadesc']?></td>
            <td>
                <div style="background-color: <?=$array_color['html']?>" class="color_product_line"></div>
            </td>
            <td>
                <?php
                $version = "";
                if($id_product == "2731"): ?>
                    <select class="form-control change-subfamily input_subfamily_product">
                        <option <?php if(empty($subfamily_product)): echo "selected"; endif;?> value="">C.33</option>
                        <option <?php if($subfamily_product == 1): echo "selected"; endif;?> value="1">C.35</option>
                        <option <?php if($subfamily_product == 2): echo "selected"; endif;?> value="2">C.42</option>
                    </select>
                <?php endif; ?>
            </td>
            <td>
                <input required placeholder="1" title="" value="1" min="1" type="number" name="input_unit_product" class="input_unit_product form-control w75"/>
            </td>
            <td>
                <input pattern="[0-9]+([\.,][0-9]+)" title="Valor mínimo: <?=$ancho_minimo?> y máximo: <?=$ancho_maximo?>. Ejemplo formato: 1.010" required placeholder="<?=$ancho_minimo?>" type="text" name="input_width_product" class="input_width_product form-control check_size border-warning w100"/>
            </td>
            <td>
                <input pattern="[0-9]+([\.,][0-9]+)" required placeholder="<?=$alto_minimo?>" title="Valor mínimo: <?=$alto_minimo?> y máximo: <?=$alto_maximo?>. Ejemplo formato: 1.010" type="text" name="input_height_product" class="input_height_product form-control check_size border-warning w100"/>
            </td>
            <td>
                <button data-toggle="modal" data-target="#modal_option_product" class="btn btn-sm btn-info btn_option_product">Configurar</button>
            </td>
            <td>
                <textarea class="border_gris noresize observaciones" placeholder="Observaciones"></textarea>
            </td>
            <td>
                <span class="product_price">Revisar medidas</span>
            </td>
            <td>
                <span class="glyphicon glyphicon-duplicate text-warning btn_copy_line"></span>
                <span class="glyphicon glyphicon-remove-circle text-danger btn_delete_line"></span>
                <button data-toggle="modal" data-target="#modal_add_product" class="btn btn-sm btn-success btn_add_product">Añadir</button>
            </td>
            <td class="hidden info_product_line">
                <span class="hidden input_id_product" span_value="<?=$id_product?>" span_name="id_product"></span>
                <span class="hidden input_color_product" span_value="<?=$color_product?>" span_name="color_product"></span>
                <span class="hidden input_group_color" span_value="<?=$group_color?>" span_name="group_color"></span>
                <span class="hidden input_danch" span_value="<?=str_pad($ancho_minimo,5,"0")?>" span_name="input_danch"></span>
                <span class="hidden input_hanch" span_value="<?=str_pad($ancho_maximo,5,"0")?>" span_name="input_hanch"></span>
                <span class="hidden input_dalto" span_value="<?=str_pad($alto_minimo,5,"0")?>" span_name="input_dalto"></span>
                <span class="hidden input_halto" span_value="<?=str_pad($alto_maximo,5,"0")?>" span_name="input_halto"></span>
            </td>
        </tr>
        <?php if(!isset($id_product)):?>
            <tr class="table_product_0 ">
                <td class="text-center" colspan="7">Sin productos añadidos</td>
            </tr>
        <?php endif; ?>
    <?php else:?>
        <?php

        include root.'web/mods/mod_cart/product_cart.php';

        ?>
    <?php endif; ?>
    </tbody>
</table>
<table>
    <tr class="total_line">
        <td class="text-right"><b>Total:</b>    </td>
        <td><input readonly type="text" class="price_total" value="0.00"/></td>
    </tr>
</table>